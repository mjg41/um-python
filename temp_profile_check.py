#!usr/bin/python3

import matplotlib
matplotlib.use('AGG')
import numpy as np
import iris
import iris.plot as iplt
import matplotlib.pyplot as plt
from matplotlib.ticker import IndexFormatter, FuncFormatter

# Initialise cube using pre-existing cube as template

cubes = iris.load('/projects/ukca-ex/mgriffit/output_files/19880901T0000Z/L88_100km/ap618_last_ts.nc')
temp = cubes[1]

# Extract height coordinate and extend to 'top'

temp.coord('level_height').standard_name = 'height'
heights = temp.coord('height').points[:]
top = 300001.
heights = np.append(heights , np.arange(100000.,top,5000))
n = len(heights)

# Create new iris coordinate from extended heights

height = iris.coords.DimCoord(heights , standard_name = 'height' , units = 'm')

# Construct cube containing temperature profile

T_NUDGE = iris.cube.Cube(np.zeros(n , np.float32) , standard_name = 'air_temperature' , units = 'K' , dim_coords_and_dims = [(height , 0)])

# Define necessary constants

planet_radius = 6371229.0

ndg_thermo_Tzero = 219.6
ndg_thermo_z_USSA_bottom=70000.0
ndg_thermo_z_spause = 50000.0 
ndg_thermo_z_USSA_top= 86000.0
ndg_thermo_z_mpause = 95000.0
ndg_thermo_lapse_meso= -2.0/1000.

ndg_thermo_Tmin=179.799
ndg_thermo_zmin_CIRA=93300.
ndg_thermo_lapse_CIRA=-0.000972739

ndg_thermo_z_CIRA_top=119700.
ndg_thermo_T_CIRA_top=369.827

ndg_thermo_T_exobase=1000
ndg_thermo_lambda=0.01875/1000.

# Set temperature profile based on US standard atmosphere, CIRA, and exobase temperature
# Loop over levels setting temperature values
for k in range(n):
    z = heights[k]

    #from 70-86 km, follow USSA
    if (z >=  ndg_thermo_z_USSA_bottom and z <= ndg_thermo_z_USSA_top):
        
        T_NUDGE.data[k] = ndg_thermo_Tzero + ndg_thermo_lapse_meso*(z-ndg_thermo_z_USSA_bottom)
        
    #from 86 km to T minimum (93.3 km) use weaker lapse rate consistent with CIRA
    if (z >  ndg_thermo_z_USSA_top and z < ndg_thermo_zmin_CIRA):
        
        T_NUDGE.data[k] = ndg_thermo_Tzero+ndg_thermo_lapse_meso*(ndg_thermo_z_USSA_top-ndg_thermo_z_USSA_bottom)+ndg_thermo_lapse_CIRA*(z-ndg_thermo_z_USSA_top)
        
    # from 93.3 km to top of CIRA (119.7 km) follow CIRA tempreature rise based on square of height difference
    if (z >=  ndg_thermo_zmin_CIRA):
        
        T_NUDGE.data[k] = ndg_thermo_Tzero+ndg_thermo_lapse_meso*(ndg_thermo_z_USSA_top-ndg_thermo_z_USSA_bottom)+ndg_thermo_lapse_CIRA*(z-ndg_thermo_z_USSA_top)+(0.3/1000000.)*(z-ndg_thermo_zmin_CIRA)**2
        
    # above 119.7 km asymptote to selected exobase temperature
    if (z >= ndg_thermo_z_CIRA_top):
        
        Sigma = (z - ndg_thermo_z_CIRA_top)*(planet_radius+ndg_thermo_z_CIRA_top) / (planet_radius+z)
        
        T_NUDGE.data[k] = ndg_thermo_T_exobase - (ndg_thermo_T_exobase - ndg_thermo_T_CIRA_top)*np.exp(-ndg_thermo_lambda*Sigma)

# Save cube

iris.save(T_NUDGE,'/projects/ukca-ex/mgriffit/output_files/t_nudge.nc')

# Compare to 85km temperatures to see jump between standard model and new nudging 

cubes_85km = iris.load('/projects/ukca-ex/mgriffit/output_files/19880901T0000Z/L85_85km/be882_jun.nc')
temp_85km = cubes_85km[6]
temp_85km = temp_85km.collapsed(['longitude'],iris.analysis.MEAN)
temp_85km_75N = temp_85km[:,132]
temp_85km_EQ = temp_85km[:,72]
temp_85km_75S = temp_85km[:,11]
height_85km = temp_85km.coord('level_height')
# Plot the non-zero part of the temperature profile and save to file

nonzero = T_NUDGE[T_NUDGE.data > 0.]
fig, ax = plt.subplots(figsize=[12,6])
nonzeroheight = nonzero.coord('height')
ax.plot(nonzero.data, nonzeroheight.points[:], lw=3.0, label='Forcing Profile')
ax.fill_between([160., 220.], [90000., 90000.], [105000., 105000.], hatch='/',facecolor='none', alpha=0.4, linestyle='--')
iplt.plot(temp_85km_75N, height_85km, label='75N')
iplt.plot(temp_85km_EQ, height_85km, label='EQ')
iplt.plot(temp_85km_75S, height_85km, label = '75S')
iplt.plt.legend(loc = 'upper left')
iplt.plt.xlabel('Air Temperature / K', fontsize=16)
#ax.set_xlim(165.,215.)
iplt.plt.ylabel('Height / km', fontsize=16)
#ax.set_ylim(79000.,101000.)
#ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
ax.tick_params(axis='both', labelsize=14)
iplt.plt.ylim([0.,top])
iplt.plt.title('Temperature Forcing Profile in Comparison to 85km Model Data', fontsize = 12)
iplt.plt.savefig('/home/d03/mgriffit/Documents/Plots/Model/Temp_Profile.png', format='png', dpi=200)
iplt.plt.close()

# Calculate scale heights with variable g and plot

g = 9.80665/((1 + nonzeroheight.points[:]/6371000)**2)
H = 287.05 * nonzero / g

# Need grid spacing between H/2 and H/4

H_quarter = 0.25*H
H_half = 0.5*H
print(np.min(H.data))
print(np.min(H_half.data))
print(np.min(H_quarter.data))
iplt.plot(H, nonzeroheight, label = '$H$')
iplt.plot(H_quarter, nonzeroheight, 'r', label = '$H/4$')
iplt.plot(H_half, nonzeroheight, 'g', label = '$H/2$')
fig = iplt.plt.gcf()
DPI = fig.get_dpi()
fig.set_size_inches(1200.0/float(DPI),600.0/float(DPI))
iplt.plt.legend(loc = 'upper left')
iplt.plt.xlabel('Scale Height / m')
xticks = range(0,30001,2000)
iplt.plt.xticks(xticks)
iplt.plt.xlim(0,30000)
iplt.plt.ylabel('Height / m')
yticks = range(50000, 300001, 50000)
iplt.plt.yticks(yticks)
iplt.plt.ylim(50000,300000)
iplt.plt.title('Plot showing the atmospheric scale height $H$ with $H/2$ and $H/4$ for comparison')
iplt.plt.savefig('/home/d03/mgriffit/Documents/Plots/Model/scale_heights.png')


