def badplot(cube,title='',iswind=False):
    import numpy as np
    from numpy import unravel_index
    import iris
    import iris.plot as iplt
    
    # Extract coordinates from cube
    
    x = cube.coord('longitude').points[:]
    y = cube.coord('level_height').points[:]
    z = cube.data
    
    # Calculate cube maximum and minimum and print to screen
    
    cubemin, cubemax = [np.nanmin(z),np.nanmax(z)]
    limit = np.max((np.abs(cubemin),np.abs(cubemax)))
    print "Maximum value: " + str(cubemax) + "    " + "Index: " , unravel_index(z.argmax(), z.shape)
    print "Minimum value: " + str(cubemin) + "    " + "Index: " , unravel_index(z.argmin(),z.shape)
    
    # If plotting wind values use red-blue colour scheme, otherwise use jet
    
    if iswind:
        
        cmap = iplt.plt.cm.get_cmap('brewer_RdBu_11')
        N=12
    else:
        cmap = iplt.plt.cm.jet
        N=100
    
    # Change the plot so that longitude goes from [-180,180] instead of [0,360]    
    
    newx = x - 180
    newz = np.hstack([z[:,x>=180], z[:,x<180]])
    
    # Produce the plot using the set colourmap
    
    fig,ax = iplt.plt.subplots(1,1)    
    cs = ax.contourf(newx,
                     y,
                     newz,
                     N, 
                     cmap=cmap,
                     vmin=-limit,
                     vmax=limit)
    
    # Label up axes and add a colourbar
    
    cs.set_clim(vmin = -limit, vmax = limit)
    xticks = range(-180,181,30)
    cs.ax.set_xticks(xticks)
    cs.ax.set_xlabel('Longitude / degrees')
    cs.ax.set_ylabel('Height / m')
    cb = fig.colorbar(cs, orientation='horizontal',format = '%3.0f')
    cb.set_clim(vmin = -limit, vmax = limit)
    cb.set_label("LW Heating / K d-1")
    cb.draw_all()
    
    # Save and display plot
    
    fig.savefig('/home/d03/mgriffit/Documents/Plots/Model/19880901T0000Z/USSP_ON/' + title + '.eps')
    fig.show()
    
