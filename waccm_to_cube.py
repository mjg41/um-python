# Function to convert WACCMX output netcdf to temperature cube on
# geopotential height levels

# General WACCM-X 2.1 output downloaded from:
# https://www.earthsystemgrid.org/dataset/ucar.cgd.ccsm4.SD-WACCM-X_v2.1.atm.hist.monthly_ave/file.html

# Solar min/max output downloaded from:
# https://dashrepo.ucar.edu/dataset/WACC_Dependence_on_Solar_Activity/file.html

def waccm_to_cube(cubes_path):
    '''Function to convert waccm output cube to cube of temperature on
    height levels.
    
    Positional arguments:
    cubes_path is the path to the cube
    '''
    
    import warnings
    import numpy as np
    import iris
    
    warnings.filterwarnings("ignore")
    
    # Load cubes
    
    cubes = iris.load(cubes_path)
    
    # Get temperature data
    
    temp_cube = cubes.extract(iris.Constraint('Temperature'))[0]
    temp_data = temp_cube.data
    
    # Get heights data
    
    geopot_height_cube = cubes.extract(iris.Constraint('Geopotential Height (above sea level)'))[0]
    geopot_height_data = geopot_height_cube.data
    
    # Get latitudes
    
    lats = temp_cube.coord('latitude').points[:]
    n_lats = len(lats)
    
    # Squeeze out dimensions of length 1
    
    temp_data = np.squeeze(temp_data)
    geopot_height_data = np.squeeze(geopot_height_data)
    
    # Mean in longitude and latitude to get approximate height coord
    
    heights = np.mean(np.mean(geopot_height_data, axis=-1), axis=-1)
    
    # Flip to get in ascending order
    
    heights = np.flip(heights)
    n_heights = len(heights)
    
    # Mean in longitude to get temp as funcn of height and lat
    
    temps = np.mean(temp_data, axis = -1)
    
    # Flip on height axis to get in ascending order
    
    temps = np.flip(temps, axis = 0)
    
    # Construct cube containing temperature profile
    
    height_coord = iris.coords.DimCoord(heights, standard_name = 'height', units = 'm')
    
    lat_coord = iris.coords.DimCoord(lats, standard_name = 'latitude', units = 'degrees')

    T_WACCM = iris.cube.Cube(temps, standard_name = 'air_temperature' , units = 'K' , dim_coords_and_dims = [(height_coord, 0), (lat_coord, 1)])
    
    return T_WACCM
    
def waccm_to_full_cube(cubes_path):
    '''Function to convert waccm output cube to cube of temperature on
    height levels, longitude and latitude.
    
    Positional arguments:
    cubes_path is the path to the cube
    '''
    
    import warnings
    import numpy as np
    import iris
    
    warnings.filterwarnings("ignore")
    
    # Load cubes
    
    cubes = iris.load(cubes_path)
    
    # Get temperature data
    
    temp_cube = cubes.extract(iris.Constraint('Temperature'))[0]
    temp_data = temp_cube.data
    
    # Get heights data
    
    geopot_height_cube = cubes.extract(iris.Constraint('Geopotential Height (above sea level)'))[0]
    geopot_height_data = geopot_height_cube.data
    
    # Get latitudes
    
    lats = temp_cube.coord('latitude').points[:]
    n_lats = len(lats)
    
    # Get longitudes
    
    longs = temp_cube.coord('longitude').points[:]
    n_longs = len(longs)
    
    # Squeeze out dimensions of length 1
    
    temp_data = np.squeeze(temp_data)
    geopot_height_data = np.squeeze(geopot_height_data)
    
    # Mean in longitude and latitude to get approximate height coord
    
    heights = np.mean(np.mean(geopot_height_data, axis=-1), axis=-1)
    
    # Flip to get in ascending order
    
    heights = np.flip(heights)
    n_heights = len(heights)
    
    # Flip on height axis to get in ascending order
    
    temps = np.flip(temp_data, axis = 0)
    
    # Construct cube containing temperature profile
    
    height_coord = iris.coords.DimCoord(heights, standard_name = 'height', units = 'm')
    
    lat_coord = iris.coords.DimCoord(lats, standard_name = 'latitude', units = 'degrees')
    
    long_coord = iris.coords.DimCoord(longs, standard_name = 'longitude', units = 'degrees')

    T_WACCM = iris.cube.Cube(temps, standard_name = 'air_temperature' , units = 'K' , dim_coords_and_dims = [(height_coord, 0), (lat_coord, 1), (long_coord, 2)])
    
    return T_WACCM    