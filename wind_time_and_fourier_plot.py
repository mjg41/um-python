# Module to do diagnostic plots for wind time series
#
# INPUT: path to netcdf - relative to 'radar_comparison' and month
# OUTPUT: figures saved to file
import matplotlib
matplotlib.use('AGG')
import os
import numpy as np
import iris
import sys
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter


## Add symmetric colorbar about 0 for wind cases

def add_colorbar(plot, z, cb = None, **params):
    '''add_colorbar adds a symmetric colorbar about 0 for wind plots
    
    Postional arguments:
    plot is the handle to the plot (e.g. line/contour) object
    z    is the data to be plotted
    '''
    # Make mapable
    mymap = plt.cm.ScalarMappable(cmap=plot.get_cmap())
    mymap.set_array(z)
    
    # Get limits and set new limits
    top = plot.get_clim()[1]
    # top = 118.0
    mymap.set_clim(-top, top)
    
    # Add colorbar
    cb = plt.colorbar(mymap, **params)
    # And remove white lines between
    cb.solids.set_edgecolor("face")
    
    return cb   
    
# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
    run = input('Please enter path to file relative to "radar_comparison/":    ')
    month = input('Please enter three letter month code:    ')
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    # Make the output directory if it doesn't exist
    
    if not os.path.exists(os.path.dirname(outputpath + run + month + '/')):
        try:
            os.makedirs(os.path.dirname(outputpath + run + month + '/'))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    # Load in pre-existing interpolation
        
    u_asc = iris.load(inputpath + run + 'u_asc_' + month + '.nc')[0]
    u_rot = iris.load(inputpath + run + 'u_rot_' + month + '.nc')[0]
    v_asc = iris.load(inputpath + run + 'v_asc_' + month + '.nc')[0]
    v_rot = iris.load(inputpath + run + 'v_rot_' + month + '.nc')[0]
    	
    # Get year info for titles
    
    year = str(u_asc.coord('time').cell(0)[0])[:4]
    
    # Set up coordinates

    t = np.arange(0.,30.,1/24)
    z = u_asc.coord('level_height').points[:]
    T,Z = np.meshgrid(t,z)
    T = T.T
    Z = Z.T
        
    # Set up fourier scales
    
    sampling_t = 24 # Sampling frequency per day
    l_t = len(t)
    f_t = sampling_t*np.arange(int(0.5*l_t) + 1)/l_t

    sampling_z = 1/3 # Sampling frequency per km
    l_z = len(z)
    f_z = sampling_z*np.arange(int(0.5*l_z) + 1)/l_z
    
    F_T, F_Z = np.meshgrid(f_t,f_z)
    F_T = F_T.T
    F_Z = F_Z.T
    
    # Calculate single sided fourier spectrum and scale appropriately
    
    def fft_ss_spectrum(wind):
        
        f_wind = np.fft.fft2(wind.data)
        ds_spectrum = np.abs(f_wind/l_t/l_z)
        ss_spectrum = ds_spectrum[:(int(0.5*l_t) + 1),:(int(0.5*l_z) + 1)]
        ss_spectrum[1:-1,1:-1] = 2*ss_spectrum[1:-1,1:-1]
        
        return ss_spectrum
    
    f_u_asc = fft_ss_spectrum(u_asc)
    f_u_rot = fft_ss_spectrum(u_rot)
    f_v_asc = fft_ss_spectrum(v_asc)
    f_v_rot = fft_ss_spectrum(v_rot)
    
    ### Plot winds ###
    
    # colormap and figsize
    
    cmap = plt.cm.get_cmap('RdBu_r', 31)
    figsize = [16,5]
    
    # Property setup
    
    winds = [u_asc.data, u_rot.data, v_asc.data, v_rot.data]
    titles = [r'$\bf{ExUM}$ zonal wind at Ascension Island in ',
              r'$\bf{ExUM}$ zonal wind at Rothera in ',
              r'$\bf{ExUM}$ meridional wind at Ascension Island in ',
              r'$\bf{ExUM}$ meridional wind at Rothera in ']
    cbar_labels = [r'Zonal Wind / ms$^{-1}$', 'Zonal Wind / ms$^{-1}$',
                   r'Meridional Wind / ms$^{-1}$', 'Meridional Wind / ms$^{-1}$']
    filenames = ['uwind_asc', 'uwind_rot', 'vwind_asc', 'vwind_rot']
    
    # Loop to plot
    
    for (wind, title, cbar_label, filename) in zip(winds, titles, cbar_labels, filenames):
    
        # Plot setup
    
        fig, ax = plt.subplots(figsize=figsize)
    
        # Calculate top value on colorbar
        
        top = np.amax((np.abs(np.nanmin(wind)), np.abs(np.nanmax(wind))))
        # top = 118.0
    
        # Do contour plot
    
        contour = ax.contourf(T, Z, wind, 31, cmap=cmap, vmax=top, vmin=-top)
        
        # Fix edge colours
        
        for c in ax.collections:
            c.set_edgecolor("face")
            
        # x axis
        
        ax.set_xticks(np.arange(0,31,2))
        ax.set_xlabel('Time / days', fontsize=26)
        ax.tick_params(axis='x', labelsize=24)
        
        # y axis
        
        ax.set_ylabel('Height / km', fontsize=26)
        ax.set_ylim(79000.,101000.)
        ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        ax.tick_params(axis='y', labelsize=24)
        
        # title
        
        ax.set_title(title + month.capitalize() + ' ' + year + '. Hourly sampling.', fontsize=19)
        
        # colorbar
        
        cb = add_colorbar(contour, u_asc.data, label=cbar_label)
        cb.set_label(cbar_label, size=24)
        cb.ax.tick_params(labelsize=20)
        
        # save fig
        
        fig.savefig(outputpath + run + month + '/Model_' + filename + '_' + month + '.png', dpi = 200, bbox_inches = 'tight', pad_inches = 0.2)
    
    #~ # Plot sanity checks
    
    #~ if interp_flag == 'y':
    
        #~ # u wind at asc
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(T, Z, cubes[0][:,:,2,2].data, 50, cmap=cmap)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,31,5))
        #~ ax.set_xlabel('Time / days')
        #~ ax.set_ylabel('Height / m')
        #~ ax.set_title('Zonal wind over Ascension Island in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = add_colorbar(contour, cubes[0][:,:,2,2].data)
        #~ cb.set_label('Zonal Wind / ms-1')
        #~ fig.savefig(outputpath + run + month + '/Model_uwind_asc_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
        #~ # v wind at asc
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(T, Z, cubes[2][:,:,2,2].data, 50, cmap=cmap)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,31,5))
        #~ ax.set_xlabel('Time / days')
        #~ ax.set_ylabel('Height / m')
        #~ ax.set_title('Meridional wind over Ascension Island in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = add_colorbar(contour, cubes[2][:,:,2,2].data)
        #~ cb.set_label('Meridional Wind / ms-1')
        #~ fig.savefig(outputpath + run + month + '/Model_vwind_asc_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
        #~ # u wind at roth
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(T, Z, cubes[1][:,:,2,2].data, 50, cmap=cmap)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,31,5))
        #~ ax.set_xlabel('Time / days')
        #~ ax.set_ylabel('Height / m')
        #~ ax.set_title('Zonal wind over Rothera in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = add_colorbar(contour, cubes[1][:,:,2,2].data)
        #~ cb.set_label('Zonal Wind / ms-1')
        #~ fig.savefig(outputpath + run + month + '/Model_uwind_rot_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
        #~ # v wind at roth
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(T, Z, cubes[3][:,:,2,2].data, 50, cmap=cmap)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,31,5))
        #~ ax.set_xlabel('Time / days')
        #~ ax.set_ylabel('Height / m')
        #~ ax.set_title('Meridional wind over Rothera in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = add_colorbar(contour, cubes[3][:,:,2,2].data)
        #~ cb.set_label('Meridional Wind / ms-1')
        #~ fig.savefig(outputpath + run + month + '/Model_vwind_rot_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
    
    ### Plot fourier components ###
    
    # figsize
    
    figsize = [16,8]

    # u wind at asc
    
    fig, ax = plt.subplots(figsize=figsize)
    fig.set_tight_layout(True)
    contour = ax.contourf(F_T, F_Z, f_u_asc, 39)
    for c in ax.collections:
        c.set_edgecolor("face")
    ax.set_xticks(np.arange(0,12.001))
    ax.set_xlabel('Temporal frequency / day-1')
    ax.set_ylabel('Spatial frequency / km-1')
    ax.set_title('Spectral components of zonal wind over Ascension Island in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
    cb = plt.colorbar(contour)
    cb.set_label('Spectral Power per day per km')
    fig.savefig(outputpath + run + month + '/Model_spec_uwind_asc_' + month + '.png', dpi = 200, bbox_inches='tight', pad_inches=0)
    
    # v wind at asc
    
    fig, ax = plt.subplots(figsize=figsize)
    fig.set_tight_layout(True)
    contour = ax.contourf(F_T, F_Z, f_v_asc, 39)
    for c in ax.collections:
        c.set_edgecolor("face")
    ax.set_xticks(np.arange(0,12.001))
    ax.set_xlabel('Temporal frequency / day-1')
    ax.set_ylabel('Spatial frequency / km-1')
    ax.set_title('Spectral components of meridional wind over Ascension Island in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
    cb = plt.colorbar(contour)
    cb.set_label('Spectral Power per day per km')
    fig.savefig(outputpath + run + month + '/Model_spec_vwind_asc_' + month + '.png', dpi = 200, bbox_inches='tight', pad_inches=0)
    
    # u wind at rot
    
    fig, ax = plt.subplots(figsize=figsize)
    fig.set_tight_layout(True)
    contour = ax.contourf(F_T, F_Z, f_u_rot, 39)
    for c in ax.collections:
        c.set_edgecolor("face")
    ax.set_xticks(np.arange(0,12.001))
    ax.set_xlabel('Temporal frequency / day-1')
    ax.set_ylabel('Spatial frequency / km-1')
    ax.set_title('Spectral components of zonal wind over Rothera in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
    cb = plt.colorbar(contour)
    cb.set_label('Spectral Power per day per km')
    fig.savefig(outputpath + run + month + '/Model_spec_uwind_rot_' + month + '.png', dpi = 200, bbox_inches='tight', pad_inches=0)
    
    # v wind at rot
    
    fig, ax = plt.subplots(figsize=figsize)
    fig.set_tight_layout(True)
    contour = ax.contourf(F_T, F_Z, f_v_rot, 39)
    for c in ax.collections:
        c.set_edgecolor("face")
    ax.set_xticks(np.arange(0,12.001))
    ax.set_xlabel('Temporal frequency / day-1')
    ax.set_ylabel('Spatial frequency / km-1')
    ax.set_title('Spectral components of meridional wind over Rothera in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
    cb = plt.colorbar(contour)
    cb.set_label('Spectral Power per day per km')
    fig.savefig(outputpath + run + month + '/Model_spec_vwind_rot_' + month + '.png', dpi = 200, bbox_inches='tight', pad_inches=0)
    
    #~ # Plot sanity checks
    
    #~ if interp_flag == 'y':
        
        #~ f_u_asc_check = fft_ss_spectrum(cubes[0][:,:,2,2])
        #~ f_u_rot_check = fft_ss_spectrum(cubes[1][:,:,2,2])
        #~ f_v_asc_check = fft_ss_spectrum(cubes[2][:,:,2,2])
        #~ f_v_rot_check = fft_ss_spectrum(cubes[3][:,:,2,2])
        
        #~ # u wind at asc
    
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(F_T, F_Z, f_u_asc_check, 39)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,12.001))
        #~ ax.set_xlabel('Temporal frequency / day-1')
        #~ ax.set_ylabel('Spatial frequency / km-1')
        #~ ax.set_title('Spectral components of zonal wind over Ascension Island in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = plt.colorbar(contour)
        #~ cb.set_label('Spectral Power per day per km')
        #~ fig.savefig(outputpath + run + month + '/Model_spec_uwind_asc_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
        #~ # v wind at asc
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(F_T, F_Z, f_v_asc_check, 39)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,12.001))
        #~ ax.set_xlabel('Temporal frequency / day-1')
        #~ ax.set_ylabel('Spatial frequency / km-1')
        #~ ax.set_title('Spectral components of meridional wind over Ascension Island in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = plt.colorbar(contour)
        #~ cb.set_label('Spectral Power per day per km')
        #~ fig.savefig(outputpath + run + month + '/Model_spec_vwind_asc_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
        #~ # u wind at rot
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(F_T, F_Z, f_u_rot_check, 39)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,12.001))
        #~ ax.set_xlabel('Temporal frequency / day-1')
        #~ ax.set_ylabel('Spatial frequency / km-1')
        #~ ax.set_title('Spectral components of zonal wind over Rothera in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = plt.colorbar(contour)
        #~ cb.set_label('Spectral Power per day per km')
        #~ fig.savefig(outputpath + run + month + '/Model_spec_uwind_rot_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
        #~ # v wind at rot
        
        #~ fig, ax = plt.subplots(figsize=figsize)
        #~ fig.set_tight_layout(True)
        #~ contour = ax.contourf(F_T, F_Z, f_v_rot_check, 39)
        #~ for c in ax.collections:
            #~ c.set_edgecolor("face")
        #~ ax.set_xticks(np.arange(0,12.001))
        #~ ax.set_xlabel('Temporal frequency / day-1')
        #~ ax.set_ylabel('Spatial frequency / km-1')
        #~ ax.set_title('Spectral components of meridional wind over Rothera in ' + month.capitalize() + ' ' + year + '. Hourly sampling.')
        #~ cb = plt.colorbar(contour)
        #~ cb.set_label('Spectral Power per day per km')
        #~ fig.savefig(outputpath + run + month + '/Model_spec_vwind_rot_' + month + '_sanitycheck.png', dpi = 200, bbox_inches='tight', pad_inches=0)
        
