import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import matplotlib.colors as col
from matplotlib.ticker import FixedFormatter, FixedLocator

# Initialise list and import Rothera data for 2005 - 2011

mat_files = [None for ii in range(7)]

mat_files[0] = sio.loadmat('data/RADAR/Rothera/2005_rothera_HourlyWind.mat')
mat_files[1] = sio.loadmat('data/RADAR/Rothera/2006_rothera_HourlyWind.mat')
mat_files[2] = sio.loadmat('data/RADAR/Rothera/2007_rothera_HourlyWind.mat')
mat_files[3] = sio.loadmat('data/RADAR/Rothera/2008_rothera_HourlyWind.mat')
mat_files[4] = sio.loadmat('data/RADAR/Rothera/2009_rothera_HourlyWind.mat')
mat_files[5] = sio.loadmat('data/RADAR/Rothera/2010_rothera_HourlyWind.mat')
mat_files[6] = sio.loadmat('data/RADAR/Rothera/2011_rothera_HourlyWind.mat')

# Initialise list and get winds from mat_files with janky MATLAB struct

winds = [None for ii in range(7)]

for ii, mat_file in enumerate(mat_files):
    
    winds[ii] = mat_file['HourlyWind']['Data'][0,0][0,0]['u']
    
# Initialise mean wind vec and compute daily averages
# Initialise number of nans vec and compute number of nans in day

wind_means = [np.zeros((30,365)) for ii in range(7)]
wind_nans = np.zeros((7, 365))

# Loop over winds

for ii, wind in enumerate(winds):
    
    # Loop over days
    
    for jj in np.arange(365):
        
        # For ii'th wind, compute mean over 24 hours for jj'th day. Wind indexed in hours appropriately
        
        wind_means[ii][:,jj] = np.mean(wind[:,jj*24:(jj+1)*24],axis=1)
        
        # From mean compute number of NaNs over height range for each day.
        
        wind_nans[ii,jj] = np.isnan(wind_means[ii][:,jj]).sum()

# Plot where there are fewer than 16 NaNs

nan_bound = 16

# Initialise figure and axis

fig, ax = plt.subplots(figsize=[11,8])

# False/True cmap

cmap = col.ListedColormap(['w', 'darkblue'])

# Do main plot

ax.imshow(wind_nans<nan_bound, aspect='auto', cmap=cmap)

# Do lines between years

ax.plot([0., 365.], [-3.5, -3.5], 'darkblue')
ax.plot([0., 365.], [-2.5, -2.5], 'darkblue')
ax.plot([0., 365.], [-1.5, -1.5], 'darkblue')
ax.plot([0., 365.], [-0.5, -0.5], 'darkblue')
ax.plot([0., 365.], [0.5, 0.5], 'darkblue')
ax.plot([0., 365.], [1.5, 1.5], 'darkblue')
ax.plot([0., 365.], [2.5, 2.5], 'darkblue')
ax.plot([0., 365.], [3.5, 3.5], 'darkblue')
ax.plot([0., 365.], [4.5, 4.5], 'darkblue')
ax.plot([0., 365.], [5.5, 5.5], 'darkblue')

# y-axis. Set up and change y ticks. Change limits and labelsize

ax.set_yticks([-4, -3, -2, -1, 0,1,2,3,4,5,6])
ax.set_yticklabels(['2001', '2002', '2003', '2004', '2005', '2006',
                    '2007', '2008', '2009', '2010', '2011'])
ax.set_ylim([6.5,-4.5])
ax.tick_params(axis='y', labelsize=26)

# x-axis. Set up and change x ticks. Major ticks shown, minor ticks have
# labels. Also change limits and labelsize.

ax.set_xlim([0,365])
ax.set_xticks([0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365])
ax.xaxis.set_minor_locator(FixedLocator([15.5, 45., 74.5, 105., 135.5, 166., 196.5, 227.5, 258., 288.5, 319, 349.5]))
ax.xaxis.set_major_formatter(plt.NullFormatter())

ax.xaxis.set_minor_formatter(FixedFormatter(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']))
ax.tick_params(which='minor', color='white', labelsize=21)


plt.savefig('/Users/mjg41/Documents/PhD/Plots/rothera_online.png', dpi=250, bbox_inches = 'tight', pad_inches = 0.2)