import numpy as np
# import matplotlib.pyplot as plt
# from vertlev_plot import vertlevplot

def heights_to_levels(um_level_file):
    
    # Function which takes a path (string) to a text file of heights and
    # outputs the eta_theta, eta_rho and top_height values for the given
    # heights
    
    # Open the heights file, read in the heights and close the file
    
    f = open(um_level_file, 'r')
    data = f.read()  # Heights read in
    f.close()
    
    # Split the file to get each height value
    
    data = data.replace('\n','')
    data = data.split(',')
    
    # Initialise new height vector
    
    heights = np.zeros(len(data))
    
    # Convert the strings from the file to floats and put them in the
    # new height vector
    
    for i in np.arange(len(data)):
        heights[i] = np.float(data[i])
    
    # Define top height
    
    top_height = heights[-1]
    
    # Compute eta_theta
    eta_theta = np.zeros(heights.size + 1)
    eta_theta[0] = 0.0
    eta_theta[1:] = heights/top_height
    
    # Compute eta_rho using eta_theta
    
    eta_rho = 0.5*(eta_theta[:-1] + eta_theta[1:])
    
    # Output
    return eta_theta, eta_rho, top_height
    

def levels_to_string(eta_theta, eta_rho, top_height):
    # Function which converts eta_theta and eta_rho arrays for a given 
    # top boundary height into a string in the correct format for
    # vertlevs files.
    
    # Initialise counting variable and specify file header.
    # Initialise output string.
    
    count = 0
    header = '&VERTLEVS\n  z_top_of_model = ' + str(top_height) + \
         ', \n  first_constant_r_rho_level = 51, \n  eta_theta = \n    '
    string = header
    
    # Begin loop to print eta_theta to file
    
    for i in eta_theta:
        count+=1
        string += '{:.6e},   '.format(i)
        if count%5 == 0:
            string = string + '\n    '
    
    string += '\n  eta_rho = \n    '
    
    # Begin loop to print eta_rho to file and reinitialise count
    
    count = 0
    for i in eta_rho:
        count+=1
        string = string + '{:.6e},   '.format(i)
        if count%5 == 0:
            string = string + '\n    '
    
    # Add extra new lines
    
    string = string + '\n /\n'
    
    # Output
    return string


#------------------------------- Main ---------------------------------#


# Choose load location. On Mac or at MO?

Mac = True

if Mac:
    um_level_file = '/Users/mjg41/Documents/PhD/Ancillary_Creation/' \
                    'ozone/CMIP5/raw/O3_z_theta_L100_120km.txt'
else:
    um_level_file = '/projects/ukca-ex/mgriffit/ozone/CMIP5/raw/' \
                    'O3_z_theta_L88_100km.txt'

# Convert height file to eta_theta, eta_rho values

eta_theta, eta_rho, top_height = heights_to_levels(um_level_file)

# Make into a string in correct format for vertlevs file

eta_string = levels_to_string(eta_theta, eta_rho, top_height)

# Choose save location, on Mac or at MO?

if Mac:
    output_path = '/Users/mjg41/Documents/PhD/Ancillary_Creation/' \
                  'vertlevs/vertlevs_L' + str(eta_rho.size) + '_' + \
                  str(np.int(top_height/1000)) + 'km'
else:
    output_path = '/projects/ukca-ex/mgriffit/vertlevs/vertlevs_L' + \
                  str(itop) + '_' + str(np.int(new_top_max/1000)) + \
                  'km'

# Write file

text_file = open(output_path, 'w')
text_file.write(eta_string)
text_file.close()

##----------------------------------------------------------------------
##   ------------------------        OLD         -----------------------
##   Script which creates new vertlevs files for a chosen new_top_max
##   based on the 85km original levels and a roughly exponentially
##   increasing level size thereafter (5% increase per level)
##----------------------------------------------------------------------

#top=85000.00
##new_top_max=90000.00
##new_top_max=95000.00
#new_top_max=100000.00
##new_top_max=105000.00
##new_top_max=110000.00
##new_top_max=115000.00

#print 'Extrapolating from ', top/1000,' km to ', new_top_max/1000,' km.'

#eta_theta_levels=np.array([
  #0.0000000E+00,   0.2352941E-03,   0.6274510E-03,   0.1176471E-02,   0.1882353E-02, 
  #0.2745098E-02,   0.3764706E-02,   0.4941176E-02,   0.6274510E-02,   0.7764705E-02, 
  #0.9411764E-02,   0.1121569E-01,   0.1317647E-01,   0.1529412E-01,   0.1756863E-01, 
  #0.2000000E-01,   0.2258823E-01,   0.2533333E-01,   0.2823529E-01,   0.3129411E-01, 
  #0.3450980E-01,   0.3788235E-01,   0.4141176E-01,   0.4509804E-01,   0.4894118E-01, 
  #0.5294117E-01,   0.5709804E-01,   0.6141176E-01,   0.6588235E-01,   0.7050980E-01, 
  #0.7529411E-01,   0.8023529E-01,   0.8533333E-01,   0.9058823E-01,   0.9600001E-01, 
  #0.1015687E+00,   0.1072942E+00,   0.1131767E+00,   0.1192161E+00,   0.1254127E+00, 
  #0.1317666E+00,   0.1382781E+00,   0.1449476E+00,   0.1517757E+00,   0.1587633E+00, 
  #0.1659115E+00,   0.1732221E+00,   0.1806969E+00,   0.1883390E+00,   0.1961518E+00, 
  #0.2041400E+00,   0.2123093E+00,   0.2206671E+00,   0.2292222E+00,   0.2379856E+00, 
  #0.2469709E+00,   0.2561942E+00,   0.2656752E+00,   0.2754372E+00,   0.2855080E+00, 
  #0.2959203E+00,   0.3067128E+00,   0.3179307E+00,   0.3296266E+00,   0.3418615E+00, 
  #0.3547061E+00,   0.3682416E+00,   0.3825613E+00,   0.3977717E+00,   0.4139944E+00, 
  #0.4313675E+00,   0.4500474E+00,   0.4702109E+00,   0.4920571E+00,   0.5158098E+00, 
  #0.5417201E+00,   0.5700686E+00,   0.6011688E+00,   0.6353697E+00,   0.6730590E+00, 
  #0.7146671E+00,   0.7606701E+00,   0.8115944E+00,   0.8680208E+00,   0.9305884E+00, 
  #0.1000000E+01])
  
#eta_rho_levels=np.array([ 
  #0.1176471E-03,   0.4313726E-03,   0.9019608E-03,   0.1529412E-02,   0.2313725E-02, 
  #0.3254902E-02,   0.4352941E-02,   0.5607843E-02,   0.7019607E-02,   0.8588235E-02, 
  #0.1031373E-01,   0.1219608E-01,   0.1423529E-01,   0.1643137E-01,   0.1878431E-01, 
  #0.2129412E-01,   0.2396078E-01,   0.2678431E-01,   0.2976470E-01,   0.3290196E-01, 
  #0.3619608E-01,   0.3964706E-01,   0.4325490E-01,   0.4701960E-01,   0.5094118E-01, 
  #0.5501961E-01,   0.5925490E-01,   0.6364705E-01,   0.6819607E-01,   0.7290196E-01, 
  #0.7776470E-01,   0.8278431E-01,   0.8796078E-01,   0.9329412E-01,   0.9878433E-01, 
  #0.1044314E+00,   0.1102354E+00,   0.1161964E+00,   0.1223144E+00,   0.1285897E+00, 
  #0.1350224E+00,   0.1416128E+00,   0.1483616E+00,   0.1552695E+00,   0.1623374E+00, 
  #0.1695668E+00,   0.1769595E+00,   0.1845180E+00,   0.1922454E+00,   0.2001459E+00, 
  #0.2082247E+00,   0.2164882E+00,   0.2249446E+00,   0.2336039E+00,   0.2424783E+00, 
  #0.2515826E+00,   0.2609347E+00,   0.2705562E+00,   0.2804726E+00,   0.2907141E+00, 
  #0.3013166E+00,   0.3123218E+00,   0.3237787E+00,   0.3357441E+00,   0.3482838E+00, 
  #0.3614739E+00,   0.3754014E+00,   0.3901665E+00,   0.4058831E+00,   0.4226810E+00, 
  #0.4407075E+00,   0.4601292E+00,   0.4811340E+00,   0.5039334E+00,   0.5287649E+00, 
  #0.5558944E+00,   0.5856187E+00,   0.6182693E+00,   0.6542144E+00,   0.6938630E+00, 
  #0.7376686E+00,   0.7861323E+00,   0.8398075E+00,   0.8993046E+00,   0.9652942E+00])

## Make vector of original heights

#heightall = top*eta_theta_levels

##print 'original height = ', heightall

##create new higher levels
##calculate resolution then extrapolate

#diff = np.diff(heightall)[-5:]

##print 'Differences', diff
##percent=diff[3]/diff[2]
## Level diff increases by 5% per level
#percent=1.05
#difftop=diff[3]
#toplev=heightall[85]
#newhts=np.zeros(121)
#newhts[0:86]=heightall[0:86]
#for i in range(86,121):
    #newhts[i]=newhts[i-1]+difftop*percent
    #difftop=difftop*percent
    #if newhts[i] > new_top_max and newhts[i-1] <= new_top_max:
        #itop=i

#print 'Numer of levels = ',itop

#eta_theta_new = np.zeros(itop+1)
#eta_theta_new[0:86] = eta_theta_levels*top/new_top_max
#eta_theta_new[86:itop+1] = newhts[86:itop+1]/new_top_max

### Normalise so that the values are evenly distributed and end at 1.0
## Can't do this as this adjusts the lower levels of the atmosphere!!
##eta_theta_new[:] = eta_theta_new[:]/eta_theta_new[-1]


##print 'new heights = ',new_top_max*eta_theta_new[:]
##print 'new eta_theta = ',eta_theta_new

#eta_rho_new=np.zeros(itop)

#for i in range(0,itop):
    #eta_rho_new[i]=0.5*(eta_theta_new[i]+eta_theta_new[i+1])

##print 'new eta_rho', eta_rho_new

##plot old and new vertical levels

#f1,ax1,ax2 = vertlevplot(eta_theta_levels,top)
#f2,ax3,ax4 = vertlevplot(eta_theta_new,new_top_max)
#ax2.set_ylim([0,new_top_max*1.05])
#ax4.set_ylim([0,new_top_max*1.05])
##f1.savefig('/home/d03/mgriffit/Documents/Plots/Model/vertlevs_85km.eps')
##f2.savefig('/home/d03/mgriffit/Documents/Plots/Model/vertlevs_100km.eps')
#plt.show()

## Save new eta_theta and eta_rho values to a vertlevs file for use in UM

#eta_string = levels_to_string(eta_theta_new, eta_rho_new, new_top_max)
#output_path = '/projects/ukca-ex/mgriffit/vertlevs/vertlevs_L' + \
              #str(itop) + '_' + str(np.int(new_top_max/1000)) + 'km-edt'
#text_file = open(output_path, 'w')
#text_file.write(eta_string)
#text_file.close()
