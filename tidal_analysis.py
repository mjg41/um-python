import iris
import numpy as np
import os
import sys
import matplotlib
matplotlib.use('AGG')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from iris.coord_categorisation import add_day_of_month

# Create class to do dynamic printing on one line

class Printer():
    """Print things to stdout on one line dynamically"""
    def __init__(self,data):
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/tidal_analysis/'
    run = input('Please enter path to file relative to "tidal_analysis/":    ')
    months = [item for item in input('Please enter three letter month code(s): ').split()]
    variable = input('Please enter model variable (temp, u, v, w): ')

    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/tidal_analysis/'
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath + run)):
        try:
            os.makedirs(os.path.dirname(outputpath + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    for month in months:

        Printer('Month: ' + month)

        # Find corresponding UM file for month chosen above in month directory

        um_file = None
        tidal_var_file = None

        if os.path.exists(inputpath + run + month):

            files = os.listdir(inputpath + run + month)

            for fname in files:

                # Check to see if tidal_vars have already been calculated

                if (variable + '_tidal_var_' + month) in fname:

                    tidal_var_file = fname
                    break

        # Otherwise just load corresponding UM file and make directory for tidal files to be stored in
    
        if not tidal_var_file:

            files = os.listdir(inputpath + run)

            # Make directory for tidal files

            if not os.path.exists(os.path.dirname(inputpath + run + month + '/')):
                try:
                    os.makedirs(os.path.dirname(inputpath + run + month + '/'))
                except OSError as exc: # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            for fname in files:

                if month in fname:

                    if variable == 'temp':

                        if 'a.pa' in fname:

                            um_file = fname
                            break

                    else:

                        if 'a.pb' in fname:

                            um_file = fname
                            break

        if tidal_var_file:

            Printer('Month: ' + month +'. Tidal Variations already calculated. Loading tidal_var cube')

            tidal_var = iris.load(inputpath + run + month + '/' + tidal_var_file)[0]

            # Frequency of model values (one time value minus another) and corresponding values per day

            model_freq = int(tidal_var.coord('time').points[1] - tidal_var.coord('time').points[0])
            values_per_day = 24//model_freq

        if um_file:

            Printer('Month: ' + month +'. Tidal Variations not calculated. Loading full model variable')

            # Load in variable
        
            if variable == 'temp':

                model_var = iris.load(inputpath + run + um_file, 'air_temperature')[0]

            if variable == 'u':

                model_var = iris.load(inputpath + run + um_file, 'x_wind')[0]

            if variable == 'v':

                model_var = iris.load(inputpath + run + um_file, 'y_wind')[0]

            if variable == 'w':

                model_var = iris.load(inputpath + run + um_file, 'upward_air_velocity')[0]

            # Frequency of model values (one time value minus another) and corresponding values per day

            model_freq = int(model_var.coord('time').points[1] - model_var.coord('time').points[0])
            values_per_day = 24//model_freq

            ## Compute tidal variation ##

            # Initialise
            tidal_var = model_var.copy()

            # Loop over 30 days in month
            for ii in np.arange(30):

                # Compute daily mean
                daily_mean = np.mean(model_var.data[values_per_day*ii:values_per_day*(ii+1),:,:,:],axis=0)

                # Loop over values in day
                for jj in np.arange(values_per_day*ii,values_per_day*(ii+1)):

                    # Subtract daily mean from temperature data
                    tidal_var.data[jj,:,:,:] -= daily_mean

            # Save tidal_var to netcdf
            iris.save(tidal_var, inputpath + run + month + '/' + variable + '_tidal_var_' + month + '.nc')

            Printer('Month: ' + month +'. Tidal Variations calculated.')

        ## Compute migrating tides ##

        # Initialise
        mig_comp = tidal_var.copy()

        # Longitude step
        lon_step = mig_comp.coord('longitude').points[:].size//values_per_day
    
        # Roll data round so the Local Times line up
        for ii in np.arange(30*values_per_day):
            mig_comp.data[ii,:,:,:] = np.roll(tidal_var.data[ii,:,:,:], lon_step*(ii+1),axis=-1)

        # Group by day
        add_day_of_month(mig_comp,'time',name='day')

        # Take mean for each day of month
        mig_comp = mig_comp.aggregated_by(['day'],iris.analysis.MEAN)

        # Save migrating component
        iris.save(mig_comp, inputpath + run + month + '/' + variable + '_mig_comp_' + month + '.nc')

        Printer('Month: ' + month +'. Migrating component calculated.')

        ## Compute non-migrating tides ##
    
        # Initialise
        non_mig_comp = tidal_var.copy()

        # Loop over 30 days in month
        for ii in np.arange(30):

            # Get migrating tides for day
            mig_day = mig_comp.data[ii,:,:,:]

            # Loop over values in day
            for jj in np.arange(values_per_day*ii,values_per_day*(ii+1)):

                # Subtract migrating tide from tidal var
                non_mig_comp.data[jj,:,:,:] -= mig_day

        # Save non-migrating component
        iris.save(non_mig_comp, inputpath + run + month + '/' + variable + '_non_mig_comp_' + month + '.nc')

        Printer('Month: ' + month +'. Non-Migrating component calculated.')
