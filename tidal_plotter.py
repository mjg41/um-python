import iris
import numpy as np
import os
import matplotlib
matplotlib.use('AGG')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import cartopy.crs as ccrs

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y)) 

# Routine to plot some tidal properties once migrating and non-migrating components have been calculated

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/tidal_analysis/'
    run = input('Please enter path to file relative to "tidal_analysis/":    ')
    month = input('Please enter three letter month code: ')
    variable = input('Please enter model variable (temp, u, v, w): ')

    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/tidal_analysis/'
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath + run + month + '/')):
        try:
            os.makedirs(os.path.dirname(outputpath + run + month + '/'))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    # Load in all tidal components

    files = os.listdir(inputpath + run + month)

    # Load in tidal vars

    for fname in files:

        if (variable + '_tidal_var_' + month) in fname:

            tidal_var_file = fname
            break
    
    tidal_var = iris.load(inputpath + run + month + '/' + tidal_var_file)[0]

    # Load in migrating component

    for fname in files:

        if (variable + '_mig_comp_' + month) in fname:

            mig_comp_file = fname
            break

    mig_comp = iris.load(inputpath + run + month + '/' + mig_comp_file)[0]

    # Load in non-migrating component

    for fname in files:

        if (variable + '_non_mig_comp_' + month) in fname:

            non_mig_comp_file = fname
            break

    non_mig_comp = iris.load(inputpath + run + month + '/' + non_mig_comp_file)[0]

    # Extract coordinates

    lats = tidal_var.coord('latitude').points[:]

    longs = tidal_var.coord('longitude').points[:]

    heights = tidal_var.coord('level_height').points[:]

    times = tidal_var.coord('time').points[:]

    # Meshgrid coords
    X,Y = np.meshgrid(longs - 180., heights[:-2])
    X = X.T
    Y = Y.T

    ## Plot tidal perturbations and corresponding migrating and non-migrating components based on variable perturbations from the mean at a couple of latitudes##

    lat_indices = [60, 17]

    lat_labels = ['8.1S', '68.1S']

    for lat_ind, lat_label in zip(lat_indices, lat_labels):

        fig, axes = plt.subplots(1,3, figsize=[12,4])

        # Plot title

        fig.suptitle('Tidal perturbations and corresponding migrating and non-migrating components\n at ' + lat_label + ' based on ' + variable + ' perturbations from the mean')

        newz = np.concatenate([tidal_var.data[:,:,:,longs>=180.], tidal_var.data[:,:,:,longs<180.]], axis=-1)
        #plot = axes[0].pcolormesh(X[:,:85], Y[:,:85], newz[4,:85,lat_ind,:].T, norm=MidpointNormalize(midpoint=0.), cmap='bwr')
        plot = axes[0].pcolormesh(X, Y, newz[0,:-2,lat_ind,:].T, norm=MidpointNormalize(midpoint=0.), cmap='bwr')
    
        # Sort out axes
        axes[0].set_xlabel('Longitude / deg')
        axes[0].set_xticks(np.arange(-180.,181.,60.))
        axes[0].set_ylabel('Height / km')
        axes[0].set_ylim(top=110000)
        axes[0].yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        axes[0].set_title('Tidal Perturbations at ' + lat_label)
        cb = fig.colorbar(plot,ax=axes[0])

        if variable == 'temp':

            cb.set_label('Perturbation / K')

        else:

            cb.set_label(r'Perturbation / ms$^{-1}$')

        # Migrating Tide Plot

        newz = np.concatenate([mig_comp.data[:,:,:,longs>=180.], mig_comp.data[:,:,:,longs<180.]], axis=-1)
        #plot = axes[1].pcolormesh(X[:,:85], Y[:,:85], newz[0,:85,lat_ind,:].T, norm=MidpointNormalize(midpoint=0.), cmap='bwr')
        plot = axes[1].pcolormesh(X, Y, newz[0,:-2,lat_ind,:].T, norm=MidpointNormalize(midpoint=0.), cmap='bwr')
    
        # Sort out axes
        axes[1].set_xlabel('Longitude / deg')
        axes[1].set_xticks(np.arange(-180.,181.,60.))
        axes[1].set_ylabel('Height / km')
        axes[1].set_ylim(top=110000)
        axes[1].yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        axes[1].set_title('Migrating component at ' + lat_label)
        cb = fig.colorbar(plot,ax=axes[1])

        if variable == 'temp':

            cb.set_label('Perturbation / K')

        else:

            cb.set_label(r'Perturbation / ms$^{-1}$')

        # Non Migrating Tide Plot

        newz = np.concatenate([non_mig_comp.data[:,:,:,longs>=180.], non_mig_comp.data[:,:,:,longs<180.]], axis=-1)
        #plot = axes[2].pcolormesh(X[:,:85], Y[:,:85], newz[4,:85,lat_ind,:].T, norm=MidpointNormalize(midpoint=0.), cmap='bwr')
        plot = axes[2].pcolormesh(X, Y, newz[0,:-2,lat_ind,:].T, norm=MidpointNormalize(midpoint=0.), cmap='bwr')
    
        # Sort out axes
        axes[2].set_xlabel('Longitude / deg')
        axes[2].set_xticks(np.arange(-180.,181.,60.))
        axes[2].set_ylabel('Height / km')
        axes[2].set_ylim(top=110000)
        axes[2].yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        axes[2].set_title('Non-Migrating Component at ' + lat_label)
        cb = fig.colorbar(plot,ax=axes[2])

        if variable == 'temp':

            cb.set_label('Perturbation / K')

        else:

            cb.set_label(r'Perturbation / ms$^{-1}$')

        # Stop overlaps

        plt.tight_layout(rect=[0, 0.03, 1, 0.9])

        # Save figure

        fig.savefig(outputpath + run + month + '/' + variable + '_' + lat_label + '_tidal_component_check.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

    ## Fourier analysis ##

    # Set up fourier scales
    
    sampling_t = 24 # Sampling frequency per day
    l_t = len(times)
    f_t = sampling_t*np.arange(int(0.5*l_t) + 1)/l_t

    # Define function to calculate single sided fourier spectrum and scale appropriately
    
    def fft_ss_spectrum(model_var):
        
        f_wind = np.fft.fft(model_var)
        ds_spectrum = np.abs(f_wind/l_t)
        ss_spectrum = ds_spectrum[:(int(0.5*l_t) + 1)]
        ss_spectrum[1:-1] = 2*ss_spectrum[1:-1]
        
        return ss_spectrum

    # Do fourier transform for 95 km, at two latitudes

    lat_indices = [60, 17]

    lat_labels = ['8.1S', '68.1S']

    height_indices = [15, 17, 19]

    height_labels = ['90 km', '95 km', '100 km']

    for lat_ind, lat_label in zip(lat_indices, lat_labels):

        fig, axes = plt.subplots(1,3, figsize=[12,4])

        fig.suptitle('Fourier decomposition of ' + variable + ' perturbations at ' + lat_label + ', {:.1f}E and various heights'.format(longs[0]))

        f_mig_comps = [fft_ss_spectrum(tidal_var[:,height_indices[0],lat_ind,0].data), fft_ss_spectrum(tidal_var[:,height_indices[1],lat_ind,0].data), fft_ss_spectrum(tidal_var[:,height_indices[2],lat_ind,0].data)]

        mig_comp_ylim = np.amax(f_mig_comps) + 1.

        for ax, f_mig_comp, height_ind, height_label in zip(axes.ravel(), f_mig_comps, height_indices, height_labels):
    
            f_mig_comp = fft_ss_spectrum(tidal_var[:,height_ind,lat_ind,0].data)

            ax.plot(f_t,f_mig_comp,label=height_label)

            ax.set_xlabel('Temporal frequency / day-1')

            if variable == 'temp':

                ax.set_ylabel('Amplitude / K')

            else:

                ax.set_ylabel(r'Amplitude / ms$^{-1}$')

            ax.set_ylim(0, mig_comp_ylim)

            ax.set_title('Height: ' + height_label)

        # Stop overlaps

        plt.tight_layout(rect=[0, 0.03, 1, 0.9])

        fig.savefig(outputpath + run + month + '/' + variable + '_' + lat_label + '_tidal_var_fourier_var_heights.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

    ## lat-long plot at 95km ##

    # Set up coord

    X,Y = np.meshgrid(longs - 180., lats)
    X = X.T
    Y = Y.T

    # Initialise plot

    fig, axes = plt.subplots(3,1, figsize=[4,8], subplot_kw={'projection': ccrs.PlateCarree()})

    fig.suptitle('Tidal perturbations and corresponding migrating\n and non-migrating components at 95 km based\n on ' + variable + ' perturbations from the mean')

    # Full tidal component

    newz = np.concatenate([tidal_var.data[:,:,:,longs>=180.], tidal_var.data[:,:,:,longs<180.]], axis=-1)
    plot = axes[0].contourf(X, Y, newz[0,17,:,:].T, 24, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    axes[0].coastlines(alpha=0.25)

    # Sort out axes
    axes[0].set_xlabel('Longitude / deg')
    axes[0].set_xticks(np.arange(-180.,181.,60.))
    axes[0].set_ylabel('Latitude / deg')
    axes[0].set_yticks(np.arange(-90., 91.,30.))
    axes[0].set_title('Tidal Perturbations')
    cb = fig.colorbar(plot,ax=axes[0])

    if variable == 'temp':

        cb.set_label('Perturbation / K', size=12)

    else:

        cb.set_label(r'Perturbation / ms$^{-1}$', size=12)

    # Migrating component

    newz = np.concatenate([mig_comp.data[:,:,:,longs>=180.], mig_comp.data[:,:,:,longs<180.]], axis=-1)
    plot = axes[1].contourf(X, Y, newz[0,17,:,:].T, 24, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    axes[1].coastlines(alpha=0.25)

    # Sort out axes
    axes[1].set_xlabel('Longitude / deg')
    axes[1].set_xticks(np.arange(-180.,181.,60.))
    axes[1].set_ylabel('Latitude / deg')
    axes[1].set_yticks(np.arange(-90., 91.,30.))
    axes[1].set_title('Migrating Component')
    cb = fig.colorbar(plot,ax=axes[1])

    if variable == 'temp':

        cb.set_label('Perturbation / K', size=12)

    else:

        cb.set_label(r'Perturbation / ms$^{-1}$', size=12)

    # Non-Migrating component

    newz = np.concatenate([non_mig_comp.data[:,:,:,longs>=180.], non_mig_comp.data[:,:,:,longs<180.]], axis=-1)
    plot = axes[2].contourf(X, Y, newz[0,17,:,:].T, 24, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    axes[2].coastlines(alpha=0.25)

    # Sort out axes
    axes[2].set_xlabel('Longitude / deg')
    axes[2].set_xticks(np.arange(-180.,181.,60.))
    axes[2].set_ylabel('Latitude / deg')
    axes[2].set_yticks(np.arange(-90., 91.,30.))
    axes[2].set_title('Non-Migrating Component')
    cb = fig.colorbar(plot,ax=axes[2])

    if variable == 'temp':

        cb.set_label('Perturbation / K', size=12)

    else:

        cb.set_label(r'Perturbation / ms$^{-1}$', size=12)

    # Stop overlaps

    #plt.tight_layout()

    fig.savefig(outputpath + run + month + '/' + variable + '_tidal_component_check_lat_long_95km.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.25)

    ## Comparison to Forbes (lat-long plot) at 103km

    #fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})

    #X,Y = np.meshgrid(longs - 180., lats)
    #X = X.T
    #Y = Y.T

    #newz = np.concatenate([mig_comp.data[:,:,:,longs>=180.], mig_comp.data[:,:,:,longs<180.]], axis=-1)
    #plot = ax.contourf(X, Y, newz[0,20,:,:].T, 20, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    #ax.coastlines(alpha=0.25)

    # Sort out axes
    #ax.set_xlabel('Longitude / deg')
    #ax.set_xticks(np.arange(-180.,181.,60.))
    #ax.set_ylabel('Latitude / deg')
    #ax.set_yticks(np.arange(-90., 91.,30.))
    #ax.set_title('Migrating component of ' + variable + ' at {:.2f} km'.format(heights[20]/1000.))
    #cb = fig.colorbar(plot,ax=ax, orientation='horizontal')
    #cb.set_label('Amplitude')

    #fig.savefig(outputpath + run + month + '/' + variable + '_mig_comp_lat_long_103km.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

    ## Comparison to Forbes (lat-long plot) at 98km

    #fig, axes = plt.subplots(2,1, figsize=[4,8], subplot_kw={'projection': ccrs.PlateCarree()})

    #fig.suptitle('Migrating tide compared with combined\n response for ' + variable + ' at {:.2f} km'.format(heights[18]/1000.))

    #newz = np.concatenate([mig_comp.data[:,:,:,longs>=180.], mig_comp.data[:,:,:,longs<180.]], axis=-1)
    #plot = axes[0].contourf(X, Y, newz[0,18,:,:].T, 20, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    #axes[0].coastlines(alpha=0.25)

    # Sort out axes
    #axes[0].set_xlabel('Longitude / deg')
    #axes[0].set_xticks(np.arange(-180.,181.,60.))
    #axes[0].set_ylabel('Latitude / deg')
    #axes[0].set_yticks(np.arange(-90., 91.,30.))
    #axes[0].set_title('Migrating component')
    #cb = fig.colorbar(plot,ax=axes[0], orientation='horizontal')
    #cb.set_label('Amplitude')

    #newz = np.concatenate([tidal_var.data[:,:,:,longs>=180.], tidal_var.data[:,:,:,longs<180.]], axis=-1)
    #plot = axes[1].contourf(X, Y, newz[1,18,:,:].T, 20, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    #axes[1].coastlines(alpha=0.25)

    # Sort out axes
    #axes[1].set_xlabel('Longitude / deg')
    #axes[1].set_xticks(np.arange(-180.,181.,60.))
    #axes[1].set_ylabel('Latitude / deg')
    #axes[1].set_yticks(np.arange(-90., 91.,30.))
    #axes[1].set_title('Combined response')
    #cb = fig.colorbar(plot,ax=axes[1], orientation='horizontal')
    #cb.set_label('Amplitude')

    # Stop overlaps

    #plt.tight_layout()

    #fig.savefig(outputpath + run + month + '/' + variable + '_mig_v_combined_lat_long_98km.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

    ## Comparison to Forbes (lat-long plot) at 115km

    #fig, axes = plt.subplots(2,1, figsize=[4,8], subplot_kw={'projection': ccrs.PlateCarree()})

    #fig.suptitle('Migrating tide compared with combined\n response for ' + variable + ' at {:.2f} km'.format(heights[23]/1000.))

    #newz = np.concatenate([mig_comp.data[:,:,:,longs>=180.], mig_comp.data[:,:,:,longs<180.]], axis=-1)
    #plot = axes[0].contourf(X, Y, newz[0,23,:,:].T, 20, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    #axes[0].coastlines(alpha=0.25)

    # Sort out axes
    #axes[0].set_xlabel('Longitude / deg')
    #axes[0].set_xticks(np.arange(-180.,181.,60.))
    #axes[0].set_ylabel('Latitude / deg')
    #axes[0].set_yticks(np.arange(-90., 91.,30.))
    #axes[0].set_title('Migrating component')
    #cb = fig.colorbar(plot,ax=axes[0], orientation='horizontal')
    #cb.set_label('Amplitude')

    #newz = np.concatenate([tidal_var.data[:,:,:,longs>=180.], tidal_var.data[:,:,:,longs<180.]], axis=-1)
    #plot = axes[1].contourf(X, Y, newz[1,23,:,:].T, 20, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    #axes[1].coastlines(alpha=0.25)

    # Sort out axes
    #axes[1].set_xlabel('Longitude / deg')
    #axes[1].set_xticks(np.arange(-180.,181.,60.))
    #axes[1].set_ylabel('Latitude / deg')
    #axes[1].set_yticks(np.arange(-90., 91.,30.))
    #axes[1].set_title('Combined response')
    #cb = fig.colorbar(plot,ax=axes[1], orientation='horizontal')
    #cb.set_label('Amplitude')

    # Stop overlaps

    #plt.tight_layout()

    #fig.savefig(outputpath + run + month + '/' + variable + '_mig_v_combined_lat_long_115km.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

    ## Comparison to Forbes (spectral analysis plot)

    #fig, ax = plt.subplots()

    # Take 90km and 0 deg latitude tidal vars and plot longitude against time converted to units of days since start of month

    #X,Y = np.meshgrid(longs - 180., ((times - times[0])/24.))
    #X = X.T
    #Y = Y.T

    #newz = np.concatenate([tidal_var.data[:,:,:,longs>=180.], tidal_var.data[:,:,:,longs<180.]], axis=-1)
    #plot = ax.contourf(X, Y, newz[:,ind_90km,eq_ind,:].T, 20, norm=MidpointNormalize(midpoint=0.), cmap='bwr')

    # Sort out axes
    #ax.set_xlabel('Longitude / deg')
    #ax.set_xticks(np.arange(-180.,181.,60.))
    #ax.set_ylabel('Time / days')
    #ax.set_title('Tidal variation at the equator at {:.1f} km'.format(heights[ind_90km]/1000.))
    #cb = fig.colorbar(plot,ax=ax, orientation='horizontal')
    #cb.set_label('Amplitude')

    # Stop overlaps

    #plt.tight_layout()

    #fig.savefig(outputpath + run + month + '/' + variable + '_tidal_var_long_time_90km.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

