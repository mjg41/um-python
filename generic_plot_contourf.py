def cubeplot(cube,filename='test',iswind=False):
    import numpy as np
    from numpy import unravel_index
    import iris
    import iris.plot as iplt
    
    ##############################################################################################################
    # Takes in a cube with 2 dimensional coordinates (e.g. x_wind with coords latitude,longitude)  as well as a
    # string as the filename (with 'test' as default) and creates and saves a plot. The colour bar can be chosen
    # to be red-white-blue with iswind=True, or left as standard, which is the default. The user can choose to add
    # coastlines on a lat-long plot, but this must be commented out otherwise. The axes and colour bars must be
    # labelled manually as well as the save location of the plot.
    ##############################################################################################################
    
    # Calculate cube maximum and minimum - gives an idea of what coordinate points the maximal and minimal data values occur (i.e. where problems are)
    
    z = cube.data
    cubemin, cubemax = [np.nanmin(z),np.nanmax(z)]
    print "Maximum value: " + str(cubemax) + "    " + "Index: " , unravel_index(z.argmax(), z.shape)
    print "Minimum value: " + str(cubemin) + "    " + "Index: " , unravel_index(z.argmin(),z.shape)
    
    # Plot cube - iswind=True gives red-blue colour bar, iswind=False gives standard colour bar and is the default setting
    
    N=100
    
    if iswind:
        
        cmap = iplt.plt.cm.get_cmap('brewer_RdBu_11')
        #cmap = iplt.plt.cm.get_cmap('jet')
        N = cmap.N
    else:
        cmap = None
    
    myplot = iplt.contourf(cube, 
                       N, 
                       cmap = cmap, 
                       origin = 'lower', 
                       hold = 'on',
                       vmin=-100,
                       vmax=100)
    
    # This is the fix for the white lines between contour levels
        
    for c in myplot.collections:
        c.set_edgecolor("face")
    
    # If you're plotting a quantity on a lat-long axes, the below option allows adding of coastlines
                           
    #plt.gca().coastlines() ### This must be commented out if doing any other plot, e.g. height vs latitude etc. ###
    
    # Add axes labels and colourbar - these must be manually changed for the specific cube to be plotted
    
    myplot.ax.set_xlabel('Latitude / degrees')
    xticks = range(-90,91,30)
    #xticks = range(-180,181,30)
    myplot.ax.set_xticks(xticks)
    myplot.ax.set_ylabel('Height / m')
    yticks = range(0,100001,20000)
    #yticks = range(-90,91,30)
    myplot.ax.set_yticks(yticks)
    cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = '%3.0f')
    cb.set_label("Westerly Wind / m s-1")
    
    # Save and display plot - this must be manually changed for the specific model run to be plotted
    
    iplt.plt.savefig('/home/d03/mgriffit/Documents/Plots/URAP/' + filename + '.eps')
    iplt.plt.show()


