import iris
from iris.coord_categorisation import add_hour
import numpy as np
import numpy.ma as ma
import os
import sys
import matplotlib
matplotlib.use('AGG')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from matplotlib.ticker import IndexFormatter, AutoMinorLocator, NullFormatter, FuncFormatter
import cartopy.crs as ccrs

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y)) 

# Create class to do dynamic printing on one line

class Printer():
    """Print things to stdout on one line dynamically"""
    def __init__(self,data):
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/tidal_analysis/'
    run = input('Please enter path to file relative to "tidal_analysis/":    ')
    variable = input('Please enter model variable (temp, u, v, w): ')

    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/tidal_analysis/'

    ### Do plots of fourier fit at Ascension and Rothera latitude at 95km ###

    lat_labels = ['8.1S', '68.1S']

    height_label = '95km'

    # Set up figure

    fig, axes = plt.subplots(2, 2, sharex=True, figsize=[12,6])

    fig.suptitle('Short term variability in modes of ' + variable + ' perturbations at equatorial and polar latitudes at ' + height_label + ' for year', fontsize=16)

    days = np.arange(360)

    # Set up month labelling function

    months_label = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']

    def f_month_label(x, pos):

        # Label 15th day of month with month label

        if x%30 == 15:

            label = months_label[pos]

        return label

    ## Start in upper left ##

    lat_label = lat_labels[0]

    ax = axes.ravel()[0]

    # Load in amps from 30-day sliding window

    amps_diurnal_year_30day = np.load(inputpath + run + variable + '_amps_diurnal_year_30day_' + height_label + '_' + lat_label + '.npy')

    amps_semi_year_30day = np.load(inputpath + run + variable + '_amps_semi_year_30day_' + height_label + '_' + lat_label + '.npy')

    # Load in amps from shorter sliding window

    amps_diurnal_year_short = np.load(inputpath + run + variable + '_amps_diurnal_year_1day_' + height_label + '_' + lat_label + '.npy')

    amps_semi_year_short = np.load(inputpath + run + variable + '_amps_semi_year_1day_' + height_label + '_' + lat_label + '.npy')

    ylim_mig_asc = np.amax((amps_diurnal_year_30day[:,7], amps_semi_year_30day[:,8], amps_diurnal_year_short[:,7], amps_semi_year_short[:,8]))

    ylim_non_mig_asc = np.amax(([amps_diurnal_year_30day[:,ii] for ii in [3,8,9]],
                                [amps_semi_year_30day[:,ii] for ii in [6,7,9]],
                                [amps_diurnal_year_short[:,ii] for ii in [3,8,9]],
                                [amps_semi_year_short[:,ii] for ii in [6,7,9]]))

    # Do plots

    ax.set_title(lat_label + ' migrating modes', fontsize=16)

    ax.plot(days, amps_diurnal_year_30day[:,7], 'C0', label='DW1')

    ax.plot(days, amps_diurnal_year_short[:,7], 'C0', alpha=0.5)

    ax.plot(days, amps_semi_year_30day[:,8], 'C1', label='SW2')

    ax.plot(days, amps_semi_year_short[:,8], 'C1', alpha=0.5)

    ax.legend()

    ax.xaxis.set_major_formatter(plt.NullFormatter())
    ax.xaxis.set_tick_params(which='minor', color='white', labelsize=13)

    if variable == 'temp':

        ax.set_ylabel('Amplitude / K', fontsize=16)

    else:

        ax.set_ylabel(r'Amplitude / ms$^{-1}$', fontsize=16)

    ax.tick_params(axis='y', labelsize=13)

    ## Now lower left ##

    ax = axes.ravel()[2]

    ax.set_title(lat_label + ' non-migrating modes', fontsize=16)

    for ii, mode_label, col in zip([3, 8, 9], ['DE3', 'DW2', 'DW3'], ['C0', 'C1', 'C2']):

        ax.plot(days, amps_diurnal_year_30day[:,ii], col, label=mode_label)

        ax.plot(days, amps_diurnal_year_short[:,ii], col, alpha=0.3)

    ax.legend()

    ax.set_xlabel('Month', fontsize=15)
    ax.set_xticks(np.arange(0, 361, 30))
    ax.set_xlim(0,360)
    ax.xaxis.set_major_formatter(plt.NullFormatter())
    ax.xaxis.set_minor_locator(AutoMinorLocator(2))
    ax.xaxis.set_minor_formatter(FuncFormatter(f_month_label))
    ax.xaxis.set_tick_params(which='minor', color='white', labelsize=13)

    if variable == 'temp':

        ax.set_ylabel('Amplitude / K', fontsize=16)

    else:

        ax.set_ylabel(r'Amplitude / ms$^{-1}$', fontsize=16)

    ax.tick_params(axis='y', labelsize=13)

    ## Now upper right ##

    lat_label = lat_labels[1]

    ax = axes.ravel()[1]

    # Load in amps from 30-day sliding window

    amps_diurnal_year_30day = np.load(inputpath + run + variable + '_amps_diurnal_year_30day_' + height_label + '_' + lat_label + '.npy')

    amps_semi_year_30day = np.load(inputpath + run + variable + '_amps_semi_year_30day_' + height_label + '_' + lat_label + '.npy')

    # Load in amps from shorter sliding window

    amps_diurnal_year_short = np.load(inputpath + run + variable + '_amps_diurnal_year_4day_' + height_label + '_' + lat_label + '.npy')

    amps_semi_year_short = np.load(inputpath + run + variable + '_amps_semi_year_4day_' + height_label + '_' + lat_label + '.npy')

    # Calculate plot ylims

    #import pdb; pdb.set_trace()

    ylim_mig = np.amax((ylim_mig_asc, amps_diurnal_year_30day[:,7].max(), amps_semi_year_30day[:,8].max(), amps_diurnal_year_short[:,7].max(), amps_semi_year_short[:,8].max())) + 1.

    ylim_non_mig = np.amax((ylim_non_mig_asc,
                            np.array([amps_diurnal_year_30day[:,ii] for ii in [3,8,9]]).max(),
                            np.array([amps_semi_year_30day[:,ii] for ii in [6,7,9]]).max(),
                            np.array([amps_diurnal_year_short[:,ii] for ii in [3,8,9]]).max(),
                            np.array([amps_semi_year_short[:,ii] for ii in [6,7,9]]).max())) + 1.

    # Change left upper plot based on this ylim

    axes.ravel()[0].set_ylim(0, ylim_mig)

    # Do plots

    ax.set_title(lat_label + ' migrating modes', fontsize=16)

    ax.plot(days, amps_diurnal_year_30day[:,7], 'C0', label='DW1')

    ax.plot(days, amps_diurnal_year_short[:,7], 'C0', alpha=0.5)

    ax.plot(days, amps_semi_year_30day[:,8], 'C1', label='SW2')

    ax.plot(days, amps_semi_year_short[:,8], 'C1', alpha=0.5)

    ax.legend()

    ax.xaxis.set_major_formatter(plt.NullFormatter())
    ax.xaxis.set_tick_params(which='minor', color='white', labelsize=13)

    ax.set_yticks(axes.ravel()[0].get_yticks())
    ax.set_ylim(0, ylim_mig)
    ax.tick_params(axis='y', labelsize=13)
    ax.yaxis.set_major_formatter(plt.NullFormatter())

    ## Now lower right ##

    ax = axes.ravel()[3]

    ax.set_title(lat_label + ' non-migrating modes', fontsize=16)

    # Change left lower plot based on this ylim

    axes.ravel()[2].set_ylim(0, ylim_non_mig)

    for ii, mode_label, col in zip([6, 7, 9], ['S0', 'SW1', 'SW3'], ['C0', 'C1', 'C2']):

        ax.plot(days, amps_semi_year_30day[:,ii], col, label=mode_label)

        ax.plot(days, amps_semi_year_short[:,ii], col, alpha=0.3)

    ax.legend()

    ax.set_xlabel('Month', fontsize=15)
    ax.set_xticks(np.arange(0, 361, 30))
    ax.set_xlim(0,360)
    ax.xaxis.set_major_formatter(plt.NullFormatter())
    ax.xaxis.set_minor_locator(AutoMinorLocator(2))
    ax.xaxis.set_minor_formatter(FuncFormatter(f_month_label))
    ax.xaxis.set_tick_params(which='minor', color='white', labelsize=13)

    ax.set_yticks(axes.ravel()[2].get_yticks())
    ax.set_ylim(0, ylim_non_mig)
    ax.tick_params(axis='y', labelsize=13)
    ax.yaxis.set_major_formatter(plt.NullFormatter())

    # Save completed fig

    fig.savefig(outputpath + run + variable + '_tidal_var_fourier_2d_' + height_label + '_year_variability.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

