import numpy as np
import iris
import copy
from netCDF4 import Dataset

# Initialise cube using pre-existing cube as template

cubes = iris.load('/projects/ukca-ex/mgriffit/output_files/19880901T0000Z/USSP_ON/ap618_last_ts.nc')
temp = cubes[1]
cubes2 = iris.load('/projects/ukca-ex/mgriffit/ukca_chris/ukca_emiss_NO_aircrft.nc')
NOx = cubes2[0]

# Extract height coordinate and extend to 120km

time = NOx.coord('time')
t = time.shape[0]
model_level_number = temp.coord('model_level_number')
z = model_level_number.shape[0]
latitude = temp.coord('latitude')
y = latitude.shape[0]
longitude = temp.coord('longitude')
x = longitude.shape[0]
forecast_reference_time = NOx.coord('forecast_reference_time')
level_height = temp.coord('level_height')
sigma = temp.coord('sigma')
forecast_period = NOx.coord('forecast_period')
fp = forecast_period.shape[0]
emission_type = '2'
update_freq_in_hours = '120'
update_type = '2'
STASH = NOx.attributes.get('STASH')
tracer_name = NOx.attributes.get('tracer_name')
vertical_scaling = NOx.attributes.get('vertical_scaling')

# Construct cube which will contain aircraft data

NO_aircrft_L88 = iris.cube.Cube(np.zeros((t, z, y, x, fp), np.float64) , long_name = NOx.long_name , units = 'kg m-2 s-1' , dim_coords_and_dims = [(time, 0), (model_level_number , 1), (latitude, 2), (longitude, 3), (forecast_period, 4)], aux_coords_and_dims = [(forecast_reference_time, 0), (level_height, 1), (sigma, 1)], attributes = dict(emission_type = emission_type, update_freq_in_hours = update_freq_in_hours, update_type = update_type, STASH = STASH))

# Choose scalar coordinate for forecast_period

NO_aircrft_L88 = NO_aircrft_L88[:,:,:,:,0]

# Set data

NO_aircrft_L88.data[:,0:85,:,:] = copy.deepcopy(NOx.data[:,:,:,:])

# Save cube

iris.save(NO_aircrft_L88, '/projects/ukca-ex/mgriffit/ukca_chris/output/ukca_emiss_NO_aircrft_l88.nc')

# Use netCDF4 package to add missing attributes

ncfile = Dataset('/projects/ukca-ex/mgriffit/ukca_chris/output/ukca_emiss_NO_aircrft_l88.nc','r+')
setattr(ncfile.variables['nox_aircraft_emissions'],'tracer_name',tracer_name)
setattr(ncfile.variables['nox_aircraft_emissions'],'vertical_scaling',vertical_scaling)
ncfile.close()

