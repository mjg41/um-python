#!/usr/bin/python3

import matplotlib
matplotlib.use('AGG')
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import os
import numpy as np
import iris
import iris.plot as iplt
import cartopy.crs as ccrs
import datetime
import pickle

class plot_type(object):
    def __init__(self, filename_var, cube, cblabel, plot_specifier='', iswind=False, cbformat = '%3.0f', slicetype=''):
        self.filename_var = filename_var
        self.cube = cube
        self.cblabel = cblabel
        self.plot_specifier = plot_specifier
        self.iswind = iswind
        self.cbformat = cbformat
        self.slicetype = slicetype


# PLOTTING FUNCTIONS
def cube_bounds(cube):
    
    # Calculate and print cube maximum and minimum
    
    cubemin, cubemax = [np.nanmin(cube.data), np.nanmax(cube.data)]
    
    return cubemin,cubemax

def index_extrema(cube):
    
    # Finds extrema indices from cube
    # Cube minimum indices
    min_lev  = np.unravel_index(cube.data.argmin(), cube.data.shape)[0]
    min_lat  = np.unravel_index(cube.data.argmin(), cube.data.shape)[1] 
    min_long = np.unravel_index(cube.data.argmin(), cube.data.shape)[2]
    
    # Cube maximum indices
    max_lev  = np.unravel_index(cube.data.argmax(), cube.data.shape)[0]
    max_lat  = np.unravel_index(cube.data.argmax(), cube.data.shape)[1] 
    max_long = np.unravel_index(cube.data.argmax(), cube.data.shape)[2] 
    
    return min_lev, min_lat, min_long, max_lev, max_lat, max_long  

def extract_from_cube(cube, xlabel, ylabel):
    
    # Extract coordinates from cube
    
    x = cube.coord(xlabel).points[:]
    y = cube.coord(ylabel).points[:]
    z = cube.data
    
    return x, y, z

# Main plotting routine
def cubeplot(cube, N=50, iswind=False, mesh=False, stereo=False, **params):

    # Plot cube, red/blue for winds, normal for everything else
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        # Use perceptually uniform version of jet from colorcet
        rainbow = ['#0034f8', '#0037f6', '#003af3', '#003df0', '#003fed', '#0041ea', '#0044e7', '#0046e4', '#0048e1', '#004ade',
                   '#004cdb', '#004fd8', '#0051d5', '#0053d2', '#0054d0', '#0056cd', '#0058ca', '#005ac7', '#005cc4', '#005ec1',
                   '#0060be', '#0061bb', '#0063b8', '#0065b6', '#0066b3', '#0068b0', '#006aad', '#006baa', '#006da7', '#006ea5',
                   '#006fa2', '#00719f', '#00729d', '#00739a', '#007598', '#007695', '#077793', '#0d7890', '#13798e', '#187a8b',
                   '#1c7b89', '#1f7c87', '#237d84', '#267e82', '#287f7f', '#2b807d', '#2d817b', '#2f8278', '#318376', '#328473',
                   '#348571', '#35866f', '#36876c', '#37886a', '#388967', '#398a65', '#3a8b62', '#3b8c60', '#3c8e5d', '#3c8f5b',
                   '#3d9058', '#3d9155', '#3e9253', '#3e9350', '#3e944d', '#3e954a', '#3e9647', '#3f9745', '#3f9842', '#3e993e',
                   '#3e9a3b', '#3e9b38', '#3e9c35', '#3e9d32', '#3e9e2e', '#3e9f2b', '#3fa027', '#3fa124', '#40a221', '#41a31d',
                   '#42a41a', '#44a517', '#45a615', '#47a713', '#4aa711', '#4ca80f', '#4fa90e', '#51a90d', '#54aa0d', '#57ab0d',
                   '#5aab0d', '#5dac0d', '#5fad0d', '#62ad0e', '#65ae0e', '#67ae0e', '#6aaf0f', '#6db00f', '#6fb00f', '#72b110',
                   '#74b110', '#77b211', '#79b211', '#7cb311', '#7eb412', '#80b412', '#83b512', '#85b513', '#88b613', '#8ab613',
                   '#8cb714', '#8fb814', '#91b815', '#93b915', '#95b915', '#98ba16', '#9aba16', '#9cbb16', '#9fbb17', '#a1bc17',
                   '#a3bc18', '#a5bd18', '#a7be18', '#aabe19', '#acbf19', '#aebf19', '#b0c01a', '#b2c01a', '#b5c11b', '#b7c11b',
                   '#b9c21b', '#bbc21c', '#bdc31c', '#c0c31c', '#c2c41d', '#c4c41d', '#c6c51d', '#c8c51e', '#cac61e', '#cdc61f',
                   '#cfc71f', '#d1c71f', '#d3c820', '#d5c820', '#d7c920', '#d9c921', '#dcca21', '#deca22', '#e0ca22', '#e2cb22',
                   '#e4cb23', '#e6cc23', '#e8cc23', '#eacc24', '#eccd24', '#eecd24', '#f0cd24', '#f2cd24', '#f3cd24', '#f5cc24',
                   '#f6cc24', '#f8cb24', '#f9ca24', '#f9c923', '#fac823', '#fbc722', '#fbc622', '#fcc521', '#fcc421', '#fcc220',
                   '#fdc120', '#fdc01f', '#fdbe1f', '#fdbd1e', '#febb1d', '#feba1d', '#feb91c', '#feb71b', '#feb61b', '#feb51a',
                   '#ffb31a', '#ffb219', '#ffb018', '#ffaf18', '#ffae17', '#ffac16', '#ffab16', '#ffa915', '#ffa815', '#ffa714',
                   '#ffa513', '#ffa413', '#ffa212', '#ffa111', '#ff9f10', '#ff9e10', '#ff9c0f', '#ff9b0e', '#ff9a0e', '#ff980d',
                   '#ff970c', '#ff950b', '#ff940b', '#ff920a', '#ff9109', '#ff8f08', '#ff8e08', '#ff8c07', '#ff8b06', '#ff8905',
                   '#ff8805', '#ff8604', '#ff8404', '#ff8303', '#ff8102', '#ff8002', '#ff7e01', '#ff7c01', '#ff7b00', '#ff7900',
                   '#ff7800', '#ff7600', '#ff7400', '#ff7200', '#ff7100', '#ff6f00', '#ff6d00', '#ff6c00', '#ff6a00', '#ff6800',
                   '#ff6600', '#ff6400', '#ff6200', '#ff6100', '#ff5f00', '#ff5d00', '#ff5b00', '#ff5900', '#ff5700', '#ff5500',
                   '#ff5300', '#ff5000', '#ff4e00', '#ff4c00', '#ff4a00', '#ff4700', '#ff4500', '#ff4200', '#ff4000', '#ff3d00',
                   '#ff3a00', '#ff3700', '#ff3400', '#ff3100', '#ff2d00', '#ff2a00']
        cmap = ListedColormap(rainbow)
        #cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.pcolormesh( cube,
                              cmap = cmap,
                              **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace when not a stereo plot
        if not stereo:
            plot.axes.axis('tight')
    
    else:
        plot = iplt.contourf( cube,
                            N,
                            cmap = cmap, 
                            origin = 'lower',
                            **params)
        
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Alternative plotting routine for odd cases
def badplot(cube, xextract, yextract, N=100, iswind=False, mesh=False, **params):
    
    x,y,z = extract_from_cube(cube, xextract, yextract)
    # Change the plot so that longitude goes from [-180,180] instead of [0,360]    
    newx = x - 180
    newz = np.hstack([z[:,x>=180], z[:,x<180]])
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.plt.pcolormesh( newx, y, newz,
                                  cmap = cmap,
                                  **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace
        
        plot.axes.axis('tight')
    
    else:
        plot = iplt.plt.contourf( newx, y, newz,
                                N, 
                                cmap = cmap, 
                                origin = 'lower',
                                **params)        
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Add labels
def labelling(plot, title='', xlabel='', xticks=None, ylabel='', yticks=None, mesh = False):
    
    from matplotlib.ticker import FuncFormatter
    
    if mesh:
        plot.axes.set_title(title, fontsize = 12)
        
        plot.axes.set_xlabel(xlabel, fontsize=15)
        if not xticks is None:
            plot.axes.set_xticks(xticks)
            plot.axes.tick_params(axis='x', labelsize=14)
    
        plot.axes.set_ylabel(ylabel, fontsize=15)
        if not yticks is None:
            plot.axes.set_yticks(yticks)
            plot.axes.tick_params(axis='y', labelsize=14)
            plot.axes.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))

    else:    
        plot.ax.set_title(title, fontsize = 12)
        
        plot.ax.set_xlabel(xlabel, fontsize=15)
        if not xticks is None:
            plot.ax.set_xticks(xticks)
            plot.ax.tick_params(axis='x', labelsize=14)
    
        plot.ax.set_ylabel(ylabel, fontsize=15)
        if not yticks is None:
            plot.ax.set_yticks(yticks)
            plot.ax.tick_params(axis='y', labelsize=14)
            plot.ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
    
    return plot
    
## Add colorbar, taking care to scale colorbar correctly based on vmin, vmax
def add_colorbar(plot, z, cb = None, iswind=False, **params):
    
    if iswind:
    
        # Make mapable
        mymap = iplt.plt.cm.ScalarMappable(cmap=plot.get_cmap())
        mymap.set_array(z)
    
        # Get limits and set new limits
        top = plot.get_clim()[1]
        #top = 90.0 # Hard-coding cbar limits
        mymap.set_clim(-top, top)
    
        # Add colorbar
        cb = iplt.plt.colorbar(mymap, **params)
        # And remove white lines between
        cb.solids.set_edgecolor("face")

    else:
        
        # Make mapable
        mymap = iplt.plt.cm.ScalarMappable(cmap=plot.get_cmap())
        mymap.set_array(z)
    
        # Get limits and set new limits
        bottom, top = plot.get_clim()
        # print(bottom,top)
        # bottom,top = [156., 432.] # Hard-coding cbar limits
        mymap.set_clim(bottom, top)
    
        # Add colorbar
        cb = iplt.plt.colorbar(mymap, **params)
        # And remove white lines between
        cb.solids.set_edgecolor("face")
    
    return cb

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Choose input filepath

    inputpath = '/projects/ukca-ex/mgriffit/output_files/'
    run = input('Please enter path to file relative to "output_files/": ')
    month = input('Please enter three letter month code: ')

    # Find corresponding nc file
    
    files = os.listdir(inputpath + run)

    for fname in files:

        # Check for month + '.nc' (this avoids finding pp files)
    
        if month + '.nc' in fname:

            ncfile = fname
    
    # Choose whether to use mesh plots or contour plots
    
    method = input('Mesh or Contour? (m or c)')
    if method == 'm': 
        mesh_on = True
    elif method == 'c':
        mesh_on = False
        
    # Choose output filepath
    
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/'
    
    # Load in cube
    
    cubes = iris.load(inputpath + run + ncfile)
    
    # Initialise plotting lists

    lath = []
    
    # Split cube into variables and convert units 
    
    temp_constraint = iris.Constraint('air_temperature',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    temp = cubes.extract(temp_constraint)[0]
    
    u_mean_constraint = iris.Constraint('x_wind',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    u_mean = cubes.extract(u_mean_constraint)[0]
    
    v_mean_constraint = iris.Constraint('y_wind',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    v_mean = cubes.extract(v_mean_constraint)[0]
    
    w_mean_constraint = iris.Constraint('upward_air_velocity',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    w_mean = cubes.extract(w_mean_constraint)[0]
    
    northgw_mean_constraint = iris.Constraint('tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    northgw_mean = cubes.extract(northgw_mean_constraint)[0]
    northgw_mean.convert_units('m s-1 d-1')
    
    eastgw_mean_constraint = iris.Constraint('tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    eastgw_mean = cubes.extract(eastgw_mean_constraint)[0]
    eastgw_mean.convert_units('m s-1 d-1')
        
    # Work out cube end time and correctly format it
    
    endtime = str(temp.coord('time').cell(0)[0])
    endtime = endtime.replace(' ','_')

    # Change outputpath based on endtime and choice of mesh or contour plot

    run += endtime + '/' # added endtime to outputpath
    if method == 'm':
        run += 'mesh/' # add mesh to outputpath
    elif method == 'c':
        run += 'contour/'
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath + run)):
        try:
            os.makedirs(os.path.dirname(outputpath + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    #Take zonal means (lat v height)   
    
    lath.append(plot_type('Model_tempmean',
                          temp.collapsed(['longitude'],iris.analysis.MEAN),
                          'Air Temperature / K',
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_uwindmean',
                          u_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Zonal Wind / m s-1',
                          iswind = True,
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_vwindmean',
                          v_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Meridional Wind / m s-1',
                          iswind = True,
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_wwindmean',
                          w_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Vertical Wind / m s-1',
                          iswind = True,
                          cbformat = '%.2f',
                          plot_specifier = 'ZonalMean'))
                          
    lath.append(plot_type('Model_MeridGWTendmean',
                          northgw_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Meridional Wind Tendency / m s-1 d-1',
                          iswind = True,
                          cbformat = '%2.1f',
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_ZonalGWTendmean',
                          eastgw_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Zonal Wind Tendency / m s-1 d-1',
                          iswind = True,
                          cbformat = '%2.1f',
                          plot_specifier = 'ZonalMean'))
    
    ########################################    Plot figures    ########################################
    
    ### Upper atmosphere altitude slice plots ###
    
    ### Zonal Mean plots ###
   
    for job in lath:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        #top = 90.0 # Hard-coding cbar limits
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, coords = ['latitude','level_height'],
                              vmin=-top, vmax=top, mesh = mesh_on, iswind = job.iswind)
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat, iswind = job.iswind)

            cb.set_label(job.cblabel, size=15)
            cb.ax.tick_params(labelsize=14)
            
        else:
        
            # Plot data on axis
            
            bottom,top = [None, None]
            # bottom,top = [156., 342.] # Hard-coding cbar limits
            
            myplot = cubeplot(job.cube, coords = ['latitude','level_height'], mesh = mesh_on, vmin=bottom, vmax=top)

            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat, iswind = job.iswind)
            
            cb.set_label(job.cblabel, size=15)
            cb.ax.tick_params(labelsize=14)
                    
        # Title and label axes
        title = job.filename_var + '_' + job.plot_specifier + '_at_' + endtime
        
        model_top = job.cube.coord('level_height').points[-1]
        
        labelling(myplot, title = title, xlabel='Latitude / degrees', xticks=np.arange(-90,91,30),
                  ylabel='Height / km', yticks=np.arange(0., model_top + 1.0, 20000.), mesh = mesh_on)
        
        # Save figure
        iplt.plt.savefig(outputpath + run + job.filename_var + '_' + job.plot_specifier + '.png', format='png', dpi=400, bbox_inches='tight')
        #iplt.plt.show()
        #xx = input('Enter to close figure\n')
        print(job.filename_var + '_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
              
    # Blocking:
    # xx = input('Enter to quit\n')


