# Module to interpolate wind fields onto radar lat-longs
#
# INPUT: path to netcdf - relative to 'radar_comparison'
# OUTPUT: -

import matplotlib
matplotlib.use('AGG')
import os
import numpy as np
import iris
import sys
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import cf_units

def radar_interp(run, month):
    '''radar_interp interpolates data on model grid to Ascension Island
    and Rothera radar lat-longs.
    
    Positional arguments:
    run   is the file path relative to 'radar_comparison/'
    month is the three letter month code
    '''
    
    inputpath = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
    files = os.listdir(inputpath + run)
    
    for fname in files:
        # Check for 'a.p' to only get fields files and then check for month
        if ('a.p' in fname) and (month in fname):
            umfile = fname
            
            # Get year from filename by splitting off first part, taking last part and removing month

            year = fname.split('.pl')[-1].split(month)[0]
    
    # Choose output filepath
    
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    # Make the directory if it doesn't exist
    
    if not os.path.exists(os.path.dirname(outputpath + run + month + '/')):
        try:
            os.makedirs(os.path.dirname(outputpath + run + month + '/'))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    # Required lats/longs
    
    asc_coord = [('latitude', -7.9), ('longitude', 345.6)]
    
    rot_coord = [('latitude', -67.6), ('longitude', 291.9)]
 
    # Load in cube

    cubes = iris.load(inputpath + run + umfile)
    
    # Add names and units for GW drag variables if they exist
    
    if cubes.extract(iris.AttributeConstraint(STASH='m01s06i105')):
        for cube in cubes.extract(iris.AttributeConstraint(STASH='m01s06i105')):
            cube.long_name = 'tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag'
            cube.units = cf_units.Unit('m s-2')

    if cubes.extract(iris.AttributeConstraint(STASH='m01s06i106')):
        for cube in cubes.extract(iris.AttributeConstraint(STASH='m01s06i106')):
            cube.long_name = 'tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag'
            cube.units = cf_units.Unit('m s-2')

    # Split cube into variables and convert units

    u_asc_constraint = iris.Constraint('x_wind',cube_func = lambda cube: (cube.coord('latitude').points[:]>-50).all()) # check for greater than 50S lat (ASC)
    u_asc = cubes.extract(u_asc_constraint)[0]

    u_rot_constraint = iris.Constraint('x_wind',cube_func = lambda cube: (cube.coord('latitude').points[:]<-50).all()) # check for less than 50S lat (ROT)
    u_rot = cubes.extract(u_rot_constraint)[0]

    v_asc_constraint = iris.Constraint('y_wind',cube_func = lambda cube: (cube.coord('latitude').points[:]>-50).all()) # check for greater than 50S lat (ASC)
    v_asc = cubes.extract(v_asc_constraint)[0]

    v_rot_constraint = iris.Constraint('y_wind',cube_func = lambda cube: (cube.coord('latitude').points[:]<-50).all()) # check for less than 50S lat (ROT)
    v_rot = cubes.extract(v_rot_constraint)[0]
    
    eastgw_asc_constraint = iris.Constraint('tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag', 
                                            cube_func = lambda cube: (cube.coord('latitude').points[:]>-50).all()) # check for greater than 50S lat (ASC)
    if cubes.extract(eastgw_asc_constraint):
        eastgw_asc = cubes.extract(eastgw_asc_constraint)[0]
        eastgw_asc.convert_units('m s-1 d-1')

    eastgw_rot_constraint = iris.Constraint('tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag', 
                                            cube_func = lambda cube: (cube.coord('latitude').points[:]<-50).all()) # check for less than 50S lat (ROT)
    if cubes.extract(eastgw_rot_constraint):
        eastgw_rot = cubes.extract(eastgw_rot_constraint)[0]
        eastgw_rot.convert_units('m s-1 d-1')

    northgw_asc_constraint = iris.Constraint('tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag', 
                                            cube_func = lambda cube: (cube.coord('latitude').points[:]>-50).all()) # check for greater than 50S lat (ASC)
    if cubes.extract(northgw_asc_constraint):
        northgw_asc = cubes.extract(northgw_asc_constraint)[0]
        northgw_asc.convert_units('m s-1 d-1')

    northgw_rot_constraint = iris.Constraint('tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag', 
                                            cube_func = lambda cube: (cube.coord('latitude').points[:]<-50).all()) # check for less than 50S lat (ROT)
    if cubes.extract(northgw_rot_constraint):
        northgw_rot = cubes.extract(northgw_rot_constraint)[0]
        northgw_rot.convert_units('m s-1 d-1')


    # Do interpolation
    
    u_asc = u_asc.interpolate(asc_coord, iris.analysis.Linear())
    u_rot = u_rot.interpolate(rot_coord, iris.analysis.Linear())
    v_asc = v_asc.interpolate(asc_coord, iris.analysis.Linear())
    v_rot = v_rot.interpolate(rot_coord, iris.analysis.Linear())
    if cubes.extract(eastgw_asc_constraint):
        eastgw_asc = eastgw_asc.interpolate(asc_coord, iris.analysis.Linear())
    if cubes.extract(eastgw_rot_constraint):
        eastgw_rot = eastgw_rot.interpolate(rot_coord, iris.analysis.Linear())
    if cubes.extract(northgw_asc_constraint):
        northgw_asc = northgw_asc.interpolate(asc_coord, iris.analysis.Linear())
    if cubes.extract(northgw_rot_constraint):
        northgw_rot = northgw_rot.interpolate(rot_coord, iris.analysis.Linear())

    # Save for future use
    
    iris.save(u_asc, inputpath + run + 'u_asc_' + month + '.nc')
    iris.save(u_rot, inputpath + run + 'u_rot_' + month + '.nc')
    iris.save(v_asc, inputpath + run + 'v_asc_' + month + '.nc')
    iris.save(v_rot, inputpath + run + 'v_rot_' + month + '.nc')
    if cubes.extract(eastgw_asc_constraint):
        iris.save(eastgw_asc, inputpath + run + 'eastgw_asc_' + month + '.nc')
    if cubes.extract(eastgw_rot_constraint):
        iris.save(eastgw_rot, inputpath + run + 'eastgw_rot_' + month + '.nc')
    if cubes.extract(northgw_asc_constraint):
        iris.save(northgw_asc, inputpath + run + 'northgw_asc_' + month + '.nc')
    if cubes.extract(northgw_rot_constraint):
        iris.save(northgw_rot, inputpath + run + 'northgw_rot_' + month + '.nc')

    return year, u_asc, u_rot, v_asc, v_rot
    
# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
    run = input('Please enter path to file relative to "radar_comparison/":    ')
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    # Setup list of months
    
    months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
    
    # Do interpolation for all months
    
    for month in months:
        
        _,_,_,_,_ = radar_interp(run, month)
        
        print(month + ' completed.')
    
