# Module to take temperature data and use to make appropriate vertlev
# files which vary according to scale height

import numpy as np
from netCDF4 import Dataset
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import iris

# Create function which takes temp and gives vertlevs

def temp_to_vertlevs(temp, heights):
    
    #Calculate scale heights with variable g

    g = 9.80665/((1 + heights/6371000)**2)
    H = 287.05 * temp / g
    
    # Get top of data

    top = np.amax(heights)
    
    # Find vertical height corresponding to minimum scale height - and
    # hence mesopause (temp minimum)
    
    mesopause_height = heights[np.argmin(H)]
    exobase = 700000
    
    # Need grid spacing at H/2

    H_quarter = 0.5*H

    # Resolution needs to be at min(H) up to mesopause, then become coarser
    # according to increase in scale height

    H_min = np.amin(H_quarter)

    # Build on original L85 configuration

    vertlevs_orig = np.array([
        20.00,    53.33,   100.00,   160.00,   233.33,
        320.00,   420.00,   533.33,   660.00,   800.00,
        953.33,  1120.00,  1300.00,  1493.33,  1700.00,
       1920.00,  2153.33,  2400.00,  2660.00,  2933.33,
       3220.00,  3520.00,  3833.33,  4160.00,  4500.00,
       4853.33,  5220.00,  5600.00,  5993.33,  6400.00,
       6820.00,  7253.33,  7700.00,  8160.00,  8633.34,
       9120.01,  9620.02, 10133.37, 10660.08, 11200.16,
      11753.64, 12320.55, 12900.93, 13494.88, 14102.48,
      14723.88, 15359.24, 16008.82, 16672.90, 17351.90,
      18046.29, 18756.70, 19483.89, 20228.78, 20992.53,
      21776.51, 22582.39, 23412.16, 24268.18, 25153.23,
      26070.59, 27024.11, 28018.26, 29058.23, 30150.02,
      31300.54, 32517.71, 33810.59, 35189.52, 36666.24,
      38254.03, 39967.93, 41824.85, 43843.83, 46046.21,
      48455.83, 51099.35, 54006.42, 57210.01, 60746.70,
      64656.96, 68985.52, 73781.77, 79100.01, 85000.00])
  
    # Cut off levels larger than H_min
    
    spacing = np.diff(vertlevs_orig) # Calculate level spacings
    last_lev_index = spacing[spacing<H_min].size + 1 # Index of level
                            # will be last level spacing less than H_min
                            # plus 1 to get right hand element of diff
    
    lower_levs = vertlevs_orig[:last_lev_index] # cut off upper levels
    
    # Add levels of new width (H_min) up to mesopause
    
    new_levs = lower_levs
    
    while new_levs[-1] < mesopause_height:
        
        # Append new level height
        
        new_levs = np.append(new_levs, (new_levs[-1] + H_min))
    
    # Now increase depth according to scale height
    
    f_interp_H_quarter = interp1d(heights, H_quarter, kind='cubic')
    
    while new_levs[-1] < top:
        
        new_levs = np.append(new_levs, (new_levs[-1] + f_interp_H_quarter(new_levs[-1])))
    
    # Level spacing is quadratic at top due to ~constant temp and
    # therefore quadratic dependence of g on height is dominating term.
    # Can therefore extrapolate quadratically to exobase
    
    f_extrap_levs = interp1d(np.arange(new_levs.size),new_levs,fill_value='extrapolate', kind='quadratic')
    
    while new_levs[-1] < exobase:
        
        new_levs = np.append(new_levs, f_extrap_levs(new_levs.size))
    
    return new_levs, H_min

if __name__ == '__main__':
    
    # Import temp cube

    temp_cube = iris.load('data/WACCM/T_WACCM_SMIN_200506.nc')[0]

    # Extract heights and temp data from cube
    
    temp_data = temp_cube.data
    
    heights = temp_cube.coord('height').points[:]

    # Get minimum temps, max temps and mean temps

    temp_min = np.amin(temp_data, axis=1)

    temp_max = np.amax(temp_data, axis=1)

    temp_mean = np.mean(temp_data, axis=1)
    
    # Create new vertical level set using function
    
    temps = [temp_min, temp_mean, temp_max]
    temp_spec = ['Min','Mean','Max']
    
    for temp, spec in zip(temps,temp_spec):
        
        print(spec)
    
        new_levs, H_min = temp_to_vertlevs(temp, heights)

        # Plot new vertical level set

        # Set up figures and axes

        f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, gridspec_kw = {'width_ratios':[1, 9]}, figsize = [9,4])
        f.suptitle('New Vertical Levels')
        ax1.set_xticklabels([])
        ax1.set_xticks([])
        ax2.set_xticks(range(0,new_levs.size,10))
        plt.subplots_adjust(wspace=0.05)
        plt.xlabel("Model Level Number")
        ax1.set_ylabel("Height / m")
        
        # Do plots and save
        
        ax1.plot(np.zeros(np.size(new_levs)),new_levs,'ko',fillstyle='none')
        ax2.plot(new_levs,'k-o',fillstyle='none')
        print('Levels to 100km = ', new_levs[new_levs<100000].size,
              '\nLevels to 120km = ', new_levs[new_levs<120000].size,
              '\nLevels to 400km = ', new_levs[new_levs<400000].size, 
              '\nNarrowest level added = ', H_min)
        # plt.show()
        #plt.savefig('/home/d03/mgriffit/Documents/Plots/Model/new_vert_lev.png')
        
        

