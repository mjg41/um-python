#!/usr/bin/python3

import matplotlib
matplotlib.use('AGG')
import os
import numpy as np
import iris
import iris.plot as iplt
import cartopy.crs as ccrs
import datetime
import pickle

import sys
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")


class plot_type(object):
    def __init__(self, filename_var, cube, cblabel, plot_specifier='', iswind=False, cbformat = '%3.0f', slicetype=''):
        self.filename_var = filename_var
        self.cube = cube
        self.cblabel = cblabel
        self.plot_specifier = plot_specifier
        self.iswind = iswind
        self.cbformat = cbformat
        self.slicetype = slicetype


# PLOTTING FUNCTIONS
def cube_bounds(cube):
    
    # Calculate and print cube maximum and minimum
    
    cubemin, cubemax = [np.nanmin(cube.data), np.nanmax(cube.data)]
    
    return cubemin,cubemax

def index_extrema(cube):
    
    # Finds extrema indices from cube
    # Cube minimum indices
    min_lev  = np.unravel_index(cube.data.argmin(), cube.data.shape)[0]
    min_lat  = np.unravel_index(cube.data.argmin(), cube.data.shape)[1] 
    min_long = np.unravel_index(cube.data.argmin(), cube.data.shape)[2]
    
    # Cube maximum indices
    max_lev  = np.unravel_index(cube.data.argmax(), cube.data.shape)[0]
    max_lat  = np.unravel_index(cube.data.argmax(), cube.data.shape)[1] 
    max_long = np.unravel_index(cube.data.argmax(), cube.data.shape)[2] 
    
    return min_lev, min_lat, min_long, max_lev, max_lat, max_long  

def extract_from_cube(cube, xlabel, ylabel):
    
    # Extract coordinates from cube
    
    x = cube.coord(xlabel).points[:]
    y = cube.coord(ylabel).points[:]
    z = cube.data
    
    return x, y, z

# Main plotting routine
def cubeplot(cube, N=100, iswind=False, mesh=False, stereo=False, **params):

    # Plot cube, red/blue for winds, normal for everything else
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.pcolormesh( cube,
                              cmap = cmap,
                              **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace when not a stereo plot
        if not stereo:
            plot.axes.axis('tight')
    
    else:
        plot = iplt.contourf( cube,
                            N,
                            cmap = cmap, 
                            origin = 'lower', 
                            hold = 'on',
                            **params)
        
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Alternative plotting routine for odd cases
def badplot(cube, xextract, yextract, N=100, iswind=False, mesh=False, **params):
    
    x,y,z = extract_from_cube(cube, xextract, yextract)
    # Change the plot so that longitude goes from [-180,180] instead of [0,360]    
    newx = x - 180
    newz = np.hstack([z[:,x>=180], z[:,x<180]])
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.plt.pcolormesh( newx, y, newz,
                                  cmap = cmap,
                                  **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace
        
        plot.axes.axis('tight')
    
    else:
        plot = iplt.plt.contourf( newx, y, newz,
                                N, 
                                cmap = cmap, 
                                origin = 'lower', 
                                hold = 'on',
                                **params)        
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Add labels
def labelling(plot, title='', xlabel='', xticks=None, ylabel='', yticks=None, mesh = False):
    if mesh:
        plot.axes.set_title(title, fontsize = 12)
        
        plot.axes.set_xlabel(xlabel)
        if not xticks is None:
            plot.axes.set_xticks(xticks)
    
        plot.axes.set_ylabel(ylabel)
        yticks = range(0, 100001, 20000)
        if not yticks is None:
            plot.axes.set_yticks(yticks)
    else:    
        plot.ax.set_title(title, fontsize = 12)
        
        plot.ax.set_xlabel(xlabel)
        if not xticks is None:
            plot.ax.set_xticks(xticks)
    
        plot.ax.set_ylabel(ylabel)
        if not yticks is None:
            plot.ax.set_yticks(yticks)
    
    return plot
    
## Add symmetric colorbar about 0 for wind cases
def add_colorbar(plot, z, cb = None, **params):
    # Make mapable
    mymap = iplt.plt.cm.ScalarMappable(cmap=plot.get_cmap())
    mymap.set_array(z)
    
    # Get limits and set new limits
    top = plot.get_clim()[1]
    mymap.set_clim(-top, top)
    
    # Add colorbar
    cb = iplt.plt.colorbar(mymap, **params)
    # And remove white lines between
    cb.solids.set_edgecolor("face")
    
    return cb

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Choose input filepath

    inputpath = '/projects/ukca-ex/mgriffit/output_files/'
    run = input('Please enter path to file relative to "output_files/": ')
    ncfile = input('Please enter NetCDF file name: ')
    
    # Choose whether to use mesh plots or contour plots
    
    mesh_on = True
        
    # Choose output filepath
    
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/Paper/'
    
    # Load in cube
    
    cubes = iris.load(inputpath + run + ncfile)
    
    # Initialise plotting lists
    
    lath = []
    
    # Split cube into variables and convert units 
    
    temp = cubes.extract(iris.Constraint('air_temperature'))[0]
    
    u_mean_constraint = iris.Constraint('x_wind',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    u_mean = cubes.extract(u_mean_constraint)[0]
    
    w_mean_constraint = iris.Constraint('upward_air_velocity',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
    w_mean = cubes.extract(w_mean_constraint)[0]
        
    # Work out cube end time and correctly format it
    
    endtime = str(temp.coord('time').cell(0)[0])
    endtime = endtime.replace(' ','_')

    # Change outputpath based on endtime and choice of mesh or contour plot

    run += endtime + '/' # added endtime to outputpath
    run += 'mesh/' # add mesh to outputpath
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath + run)):
        try:
            os.makedirs(os.path.dirname(outputpath + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    #Take zonal means (lat v height)   
    
    lath.append(plot_type('Model_tempmean',
                          temp.collapsed(['longitude'],iris.analysis.MEAN),
                          'Air Temperature / K',
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_uwindmean',
                          u_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Zonal Wind / m s-1',
                          iswind = True,
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_wwindmean',
                          w_mean.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Vertical Wind / m s-1',
                          iswind = True,
                          cbformat = '%.3f',
                          plot_specifier = 'ZonalMean'))
    
    ########################################    Plot figures    ########################################
    
    ### Upper atmosphere altitude slice plots ###
    
    ### Zonal Mean plots ###
    
    print(run, "\n")
    
    for job in lath:

        # Find symmetric limits
        #cubemin , cubemax = cube_bounds(job.cube)
        #print(job.filename_var, " - Max:", cubemax, "Min:", cubemin)
        #top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.filename_var == 'Model_tempmean':
            cubemin = 172.0
            cubemax = 301.0
        elif job.filename_var == 'Model_uwindmean':
            top = 112.0
        elif job.filename_var == 'Model_wwindmean':
            top = 0.05
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, coords = ['latitude','level_height'],
                              vmin=-top, vmax=top, mesh = mesh_on, iswind = job.iswind)
            # Add funky colorbar
            #cb = add_colorbar(myplot, job.cube,
            #                  orientation='vertical',format = job.cbformat,label = job.cblabel)
            
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube, coords = ['latitude','level_height'], mesh = mesh_on, vmin = cubemin, vmax = cubemax)
        
            # Add funky colorbar
            #cb = iplt.plt.colorbar(myplot, orientation='vertical',format = job.cbformat,label = job.cblabel)
            # Remove white lines between sections
            #cb.solids.set_edgecolor("face")
                    
        # Title and label axes
        # title = job.filename_var + '_' + job.plot_specifier + '_at_' + endtime
        
        labelling(myplot, xlabel='Latitude / degrees', xticks=np.arange(-90,91,30),
                  ylabel='Height / m', mesh = mesh_on)
        
        # Save figure
        iplt.plt.savefig(outputpath + run + job.filename_var + '_' + job.plot_specifier + '.png', format='png', dpi=900, bbox_inches='tight')
        #iplt.plt.show()
        #xx = input('Enter to close figure\n')
        #print(job.filename_var + '_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
              
    # Blocking:
    # xx = input('Enter to quit\n')


