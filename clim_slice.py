#!/usr/bin/python3

import iris
import cf_units
import os

# Initialise input path

inputpath = '/projects/ukca-ex/mgriffit/output_files/'

# Get input directory and month

run = input('Please enter path to file relative to "output_files/": ')

month  = input('Please enter three letter month code: ')

# Find corresponding pp file
    
files = os.listdir(inputpath + run)

for fname in files:

    # Check for month and avoid netcdf files
    
    if (month in fname) and ('.nc' not in fname):

        ppfile = fname

print('Loading cube')

cubes = iris.load(inputpath + run + ppfile)

# Create output nc file

ncfile = ppfile[:5] + '_' + month + '.nc'

# Add names and units for GW drag variables

for cube in cubes.extract(iris.AttributeConstraint(STASH='m01s06i105')):
    cube.long_name = 'tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag'
    cube.units = cf_units.Unit('m s-2')
for cube in cubes.extract(iris.AttributeConstraint(STASH='m01s06i106')):
    cube.long_name = 'tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag'
    cube.units = cf_units.Unit('m s-2')

# Save the new file

print('Saving file')

iris.save(cubes, inputpath + run + ncfile)

