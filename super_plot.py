#!/usr/bin/python3

import matplotlib
matplotlib.use('AGG')
import os
import numpy as np
import iris
import iris.plot as iplt
import cartopy.crs as ccrs
import datetime
import pickle

class plot_type(object):
    def __init__(self, filename_var, cube, cblabel, plot_specifier='', iswind=False, cbformat = '%3.0f', slicetype=''):
        self.filename_var = filename_var
        self.cube = cube
        self.cblabel = cblabel
        self.plot_specifier = plot_specifier
        self.iswind = iswind
        self.cbformat = cbformat
        self.slicetype = slicetype


# PLOTTING FUNCTIONS
def cube_bounds(cube):
    
    # Calculate and print cube maximum and minimum
    
    cubemin, cubemax = [np.nanmin(cube.data), np.nanmax(cube.data)]
    
    return cubemin,cubemax

def index_extrema(cube):
    
    # Finds extrema indices from cube
    # Cube minimum indices
    min_lev  = np.unravel_index(cube.data.argmin(), cube.data.shape)[0]
    min_lat  = np.unravel_index(cube.data.argmin(), cube.data.shape)[1] 
    min_long = np.unravel_index(cube.data.argmin(), cube.data.shape)[2]
    
    # Cube maximum indices
    max_lev  = np.unravel_index(cube.data.argmax(), cube.data.shape)[0]
    max_lat  = np.unravel_index(cube.data.argmax(), cube.data.shape)[1] 
    max_long = np.unravel_index(cube.data.argmax(), cube.data.shape)[2] 
    
    return min_lev, min_lat, min_long, max_lev, max_lat, max_long  

def extract_from_cube(cube, xlabel, ylabel):
    
    # Extract coordinates from cube
    
    x = cube.coord(xlabel).points[:]
    y = cube.coord(ylabel).points[:]
    z = cube.data
    
    return x, y, z

# Main plotting routine
def cubeplot(cube, N=100, iswind=False, mesh=False, stereo=False, **params):

    # Plot cube, red/blue for winds, normal for everything else
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.pcolormesh( cube,
                              cmap = cmap,
                              **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace when not a stereo plot
        if not stereo:
            plot.axes.axis('tight')
    
    else:
        plot = iplt.contourf( cube,
                            N,
                            cmap = cmap, 
                            origin = 'lower', 
                            hold = 'on',
                            **params)
        
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Alternative plotting routine for odd cases
def badplot(cube, xextract, yextract, N=100, iswind=False, mesh=False, **params):
    
    x,y,z = extract_from_cube(cube, xextract, yextract)
    # Change the plot so that longitude goes from [-180,180] instead of [0,360]    
    newx = x - 180
    newz = np.hstack([z[:,x>=180], z[:,x<180]])
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.plt.pcolormesh( newx, y, newz,
                                  cmap = cmap,
                                  **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace
        
        plot.axes.axis('tight')
    
    else:
        plot = iplt.plt.contourf( newx, y, newz,
                                N, 
                                cmap = cmap, 
                                origin = 'lower', 
                                hold = 'on',
                                **params)        
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Add labels
def labelling(plot, title='', xlabel='', xticks=None, ylabel='', yticks=None, mesh = False):
    if mesh:
        plot.axes.set_title(title, fontsize = 12)
        
        plot.axes.set_xlabel(xlabel)
        if not xticks is None:
            plot.axes.set_xticks(xticks)
    
        plot.axes.set_ylabel(ylabel)
        if not yticks is None:
            plot.axes.set_yticks(yticks)
    else:    
        plot.ax.set_title(title, fontsize = 12)
        
        plot.ax.set_xlabel(xlabel)
        if not xticks is None:
            plot.ax.set_xticks(xticks)
    
        plot.ax.set_ylabel(ylabel)
        if not yticks is None:
            plot.ax.set_yticks(yticks)
    
    return plot
    
## Add symmetric colorbar about 0 for wind cases
def add_colorbar(plot, z, cb = None, **params):
    # Make mapable
    mymap = iplt.plt.cm.ScalarMappable(cmap=plot.get_cmap())
    mymap.set_array(z)
    
    # Get limits and set new limits
    top = plot.get_clim()[1]
    mymap.set_clim(-top, top)
    
    # Add colorbar
    cb = iplt.plt.colorbar(mymap, **params)
    # And remove white lines between
    cb.solids.set_edgecolor("face")
    
    return cb

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Choose input filepath

    inputpath = '/projects/ukca-ex/mgriffit/output_files/'
    run = raw_input('Please enter path to file relative to "output_files/":    ')
    ncfile = raw_input('Please enter NetCDF file name:    ')
    
    # Choose whether to use mesh plots or contour plots
    
    method = raw_input('Mesh or Contour? (m or c)    ')
    if method == 'm': 
        mesh_on = True
    elif method == 'c':
        mesh_on = False
        
    # Choose output filepath
    
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/'
    
    # Load in cube
    
    cubes = iris.load(inputpath + run + ncfile)
    
    # Initialise plotting lists
    
    longlat = []
    longh = []
    lath = []
    stereo = []
    
    # Split cube into variables and convert units 
    
    pressure = cubes.extract(iris.Constraint('air_pressure'))[0]
    pressure.convert_units('hPa')
    
    temp = cubes.extract(iris.Constraint('air_temperature'))[0]
    
    LW = cubes.extract(iris.Constraint('tendency_of_air_temperature_due_to_longwave_heating'))[0]
    LW.convert_units('K d-1')
    
    SW = cubes.extract(iris.Constraint('tendency_of_air_temperature_due_to_shortwave_heating'))[0]
    SW.convert_units('K d-1')
    
    u = cubes.extract(iris.Constraint('x_wind'))[0]
        
    v = cubes.extract(iris.Constraint('y_wind'))[0]
    
    wcube = cubes.extract(iris.Constraint('upward_air_velocity')) # <-- without [0] gives array of cubes
    
    if wcube: #If there is a vertical wind cube
        
        # Cubes containing vertical wind
        print('Cube with vertical wind')
        
        w = cubes.extract(iris.Constraint('upward_air_velocity'))[0]
        
    else:
    
        # Cubes not containing vertical wind
        print('Cube without vertical wind')
        
        w=[]
    
    northgwcube = cubes.extract(iris.Constraint('tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag'))
    
    if northgwcube:
        
        northgw = cubes.extract(iris.Constraint('tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag'))[0]
        northgw.convert_units('m s-1 d-1')
        
    else:
        
        northgw = []
    
    eastgwcube = cubes.extract(iris.Constraint('tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag'))
    
    if eastgwcube:
        
        eastgw = cubes.extract(iris.Constraint('tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag'))[0]
        eastgw.convert_units('m s-1 d-1')
        
    else:
        
        eastgw = []
    
    # Work out cube end time and correctly format it
    
    endtime = str(temp.coord('time').cell(0)[0])
    endtime = endtime.replace(' ','_')

    # Change outputpath based on endtime and choice of mesh or contour plot

    run += endtime + '/' # added endtime to outputpath
    if method == 'm':
        run += 'mesh/' # add mesh to outputpath
    elif method == 'c':
        run += 'contour/'
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath + run)):
        try:
            os.makedirs(os.path.dirname(outputpath + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
           
    # Decide where to slice cube
    
    where_slice = raw_input('Slice for Current data or based on Previous data? (c or p)    ')
    
    # Take slice at current maximal/minimal values and save this for future use
    # Save performed in top level file so that it is accessible by any run type at specific date.
    # Split identified by 'Z' at end of date and then added back on.
    
    if where_slice == 'c':
        umin_lev, umin_lat, umin_long, umax_lev, umax_lat, umax_long = index_extrema(u)
        vmin_lev, vmin_lat, vmin_long, vmax_lev, vmax_lat, vmax_long = index_extrema(v)
        if w:
            wmin_lev, wmin_lat, wmin_long, wmax_lev, wmax_lat, wmax_long = index_extrema(w)
        slice_file = open(inputpath + run.split('Z')[0] + 'Z/' + 'slice_points.txt','wb')
        if w:
            pickle.dump([umin_lev, umin_lat, umin_long, umax_lev, umax_lat, umax_long,
                         vmin_lev, vmin_lat, vmin_long, vmax_lev, vmax_lat, vmax_long,
                         wmin_lev, wmin_lat, wmin_long, wmax_lev, wmax_lat, wmax_long], slice_file)
        else:
            pickle.dump([umin_lev, umin_lat, umin_long, umax_lev, umax_lat, umax_long,
                         vmin_lev, vmin_lat, vmin_long, vmax_lev, vmax_lat, vmax_long], slice_file)
        slice_file.close()
    
    # Take slice at previous maximal/minimal values for comparisons    
    elif where_slice == 'p':
        slice_file = open(inputpath + run.split('Z')[0] + 'Z/' + 'slice_points.txt','rb')
        if w:
            [umin_lev, umin_lat, umin_long, umax_lev, umax_lat, umax_long,
             vmin_lev, vmin_lat, vmin_long, vmax_lev, vmax_lat, vmax_long,
             wmin_lev, wmin_lat, wmin_long, wmax_lev, wmax_lat, wmax_long] = pickle.load(slice_file)
        else:
            [umin_lev, umin_lat, umin_long, umax_lev, umax_lat, umax_long,
             vmin_lev, vmin_lat, vmin_long, vmax_lev, vmax_lat, vmax_long] = pickle.load(slice_file)
        slice_file.close()
        
    # Work out height in km and latitude labels from index extrema for u
    
    umax_height_label = str(int(np.rint(u.coord('level_height').points[umax_lev]/1000.))) + 'km'
    
    umin_height_label = str(int(np.rint(u.coord('level_height').points[umin_lev]/1000.))) + 'km'
    
    umax_lat_label = int(np.rint(u.coord('latitude').points[umax_lat]))
    
    if (umax_lat_label < 0): # When we're in the southern hemisphere
        
        umax_lat_label = str(abs(umax_lat_label)) + 'S'
        
    else: # When we're in the northern hemisphere
        
        umax_lat_label = str(umax_lat_label) + 'N'
    
    umin_lat_label = int(np.rint(u.coord('latitude').points[umin_lat]))
    
    if (umin_lat_label < 0): # When we're in the southern hemisphere
        
        umin_lat_label = str(abs(umin_lat_label)) + 'S'
        
    else: # When we're in the northern hemisphere
        
        umin_lat_label = str(umin_lat_label) + 'N'
    
    # Work out height in km and latitude labels from index extrema for v
    
    vmax_height_label = str(int(np.rint(v.coord('level_height').points[vmax_lev]/1000.))) + 'km'
    
    vmin_height_label = str(int(np.rint(v.coord('level_height').points[vmin_lev]/1000.))) + 'km'
    
    vmax_lat_label = int(np.rint(v.coord('latitude').points[vmax_lat]))
    
    if (vmax_lat_label < 0): # When we're in the southern hemisphere
        
        vmax_lat_label = str(abs(vmax_lat_label)) + 'S'
        
    else: # When we're in the northern hemisphere
        
        vmax_lat_label = str(vmax_lat_label) + 'N'
    
    vmin_lat_label = int(np.rint(v.coord('latitude').points[vmin_lat]))
    
    if (vmin_lat_label < 0): # When we're in the southern hemisphere
        
        vmin_lat_label = str(abs(vmin_lat_label)) + 'S'
        
    else: # When we're in the northern hemisphere
        
        vmin_lat_label = str(vmin_lat_label) + 'N'
    
    # Work out height in km and latitude labels from index extrema for w
    
    if w:
        wmax_height_label = str(int(np.rint(w.coord('level_height').points[wmax_lev]/1000.))) + 'km'
        
        wmin_height_label = str(int(np.rint(w.coord('level_height').points[wmin_lev]/1000.))) + 'km'
        
        wmax_lat_label = int(np.rint(w.coord('latitude').points[wmax_lat]))
        
        if (wmax_lat_label < 0): # When we're in the southern hemisphere
            
            wmax_lat_label = str(abs(wmax_lat_label)) + 'S'
            
        else: # When we're in the northern hemisphere
            
            wmax_lat_label = str(wmax_lat_label) + 'N'
        
        wmin_lat_label = int(np.rint(w.coord('latitude').points[wmin_lat]))
        
        if (wmin_lat_label < 0): # When we're in the southern hemisphere
            
            wmin_lat_label = str(abs(wmin_lat_label)) + 'S'
            
        else: # When we're in the northern hemisphere
            
            wmin_lat_label = str(wmin_lat_label) + 'N'
    
    # Decide whether you want u, v or w wind plots
    if w:
        which_wind = raw_input('u, v or w wind plots? (u, v or w)    ')
    else:
        which_wind = raw_input('u or v wind plots? (u or v)    ')
    
    if which_wind == 'u':
        
        min_lev = umin_lev
        min_lat = umin_lat
        min_long = umin_long
        max_lev = umax_lev
        max_lat = umax_lat
        max_long = umax_long
        max_height_label = umax_height_label
        min_height_label = umin_height_label
        max_lat_label = umax_lat_label
        min_lat_label = umin_lat_label
        max_slicetype = 'maxu'
        min_slicetype = 'minu'
        
    elif which_wind == 'v':
        
        min_lev = vmin_lev
        min_lat = vmin_lat
        min_long = vmin_long
        max_lev = vmax_lev
        max_lat = vmax_lat
        max_long = vmax_long
        max_height_label = vmax_height_label
        min_height_label = vmin_height_label
        max_lat_label = vmax_lat_label
        min_lat_label = vmin_lat_label
        max_slicetype = 'maxv'
        min_slicetype = 'minv'

    elif which_wind == 'w':
        
        min_lev = wmin_lev
        min_lat = wmin_lat
        min_long = wmin_long
        max_lev = wmax_lev
        max_lat = wmax_lat
        max_long = wmax_long
        max_height_label = wmax_height_label
        min_height_label = wmin_height_label
        max_lat_label = wmax_lat_label
        min_lat_label = wmin_lat_label
        max_slicetype = 'maxw'
        min_slicetype = 'minw'
            
    # Take upper atmosphere altitude slices (long v lat) at...
    
    # ...max wind altitude
    
    longlat.append(plot_type('Model_Pressure',
                            pressure[max_lev,:,:],
                            'Air Pressure / hPa',
                            plot_specifier = max_height_label,
                            cbformat = '%1.1e',
                            slicetype = max_slicetype))
    
    longlat.append(plot_type('Model_Temp',
                             temp[max_lev,:,:],
                             'Temperature / K',
                             plot_specifier = max_height_label,
                             slicetype = max_slicetype))
    
    longlat.append(plot_type('Model_LW',
                             LW[max_lev,:,:],
                             'LW Heating / K d-1',
                             plot_specifier = max_height_label,
                             slicetype = max_slicetype))
    
    longlat.append(plot_type('Model_SW',
                             SW[max_lev,:,:],
                             'SW Heating / K d-1',
                             plot_specifier = max_height_label,
                             slicetype = max_slicetype))
    
    longlat.append(plot_type('Model_uwind',
                             u[max_lev,:,:],
                             'Westerly Wind / m s-1',
                             plot_specifier = max_height_label,
                             iswind=True,
                             slicetype = max_slicetype))
    
    longlat.append(plot_type('Model_vwind',
                             v[max_lev,:,:],
                             'Southerly Wind / m s-1',
                             plot_specifier = max_height_label,
                             iswind=True,
                             slicetype = max_slicetype))
    if w:
        longlat.append(plot_type('Model_wwind',
                                 w[max_lev,:,:],
                                 'Vertical Wind / m s-1',
                                 plot_specifier = max_height_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = max_slicetype))
    if northgw:
        longlat.append(plot_type('Model_MeridGWTend',
                                 northgw[max_lev,:,:],
                                 'Meridional Wind Tendency / m s-1 d-1',
                                 plot_specifier = max_height_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = max_slicetype))
    if eastgw:
        longlat.append(plot_type('Model_ZonalGWTend',
                                 eastgw[max_lev,:,:],
                                 'Zonal Wind Tendency / m s-1 d-1',
                                 plot_specifier = max_height_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = max_slicetype))
    
    # ...min wind altitude
    
    longlat.append(plot_type('Model_Pressure',
                            pressure[min_lev,:,:],
                            'Air Pressure / hPa',
                            plot_specifier = min_height_label,
                            cbformat = '%1.1e',
                            slicetype = min_slicetype))
    
    longlat.append(plot_type('Model_Temp',
                             temp[min_lev,:,:],
                             'Temperature / K',
                             plot_specifier = min_height_label,
                             slicetype = min_slicetype))
    
    longlat.append(plot_type('Model_LW',
                             LW[min_lev,:,:],
                             'LW Heating / K d-1',
                             plot_specifier = min_height_label,
                             slicetype = min_slicetype))
    
    longlat.append(plot_type('Model_SW',
                             SW[min_lev,:,:],
                             'SW Heating / K d-1',
                             plot_specifier = min_height_label,
                             slicetype = min_slicetype))
    
    longlat.append(plot_type('Model_uwind',
                             u[min_lev,:,:],
                             'Westerly Wind / m s-1',
                             plot_specifier = min_height_label,
                             iswind=True,
                             slicetype = min_slicetype))
    
    longlat.append(plot_type('Model_vwind',
                             v[min_lev,:,:],
                             'Southerly Wind / m s-1',
                             plot_specifier = min_height_label,
                             iswind=True,
                             slicetype = min_slicetype))
    if w:
        longlat.append(plot_type('Model_wwind',
                                 w[min_lev,:,:],
                                 'Vertical Wind / m s-1',
                                 plot_specifier = min_height_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = min_slicetype))
    if northgw:
        longlat.append(plot_type('Model_MeridGWTend',
                                 northgw[min_lev,:,:],
                                 'Meridional Wind Tendency / m s-1 d-1',
                                 plot_specifier = min_height_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = min_slicetype))
    if eastgw:
        longlat.append(plot_type('Model_ZonalGWTend',
                                 eastgw[min_lev,:,:],
                                 'Zonal Wind Tendency / m s-1 d-1',
                                 plot_specifier = min_height_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = min_slicetype))
        
    # Take upper atmosphere latitude slices (long v height) at...
    
    # ...max wind latitude
    
    longh.append(plot_type('Model_Temp',
                            temp[80:,max_lat,:],
                            'Temperature / K',
                            plot_specifier = max_lat_label,
                            slicetype = max_slicetype))
    
    longh.append(plot_type('Model_LW',
                            LW[80:,max_lat,:],
                            'LW Heating / K d-1',
                            plot_specifier = max_lat_label,
                            slicetype = max_slicetype))    
    
    longh.append(plot_type('Model_SW',
                           SW[80:,max_lat,:],
                           'SW Heating / K d-1',
                           plot_specifier = max_lat_label,
                           slicetype = max_slicetype))
    
    longh.append(plot_type('Model_uwind',
                           u[80:,max_lat,:],
                           'Westerly Wind / m s-1',
                           plot_specifier = max_lat_label,
                           iswind=True,
                           slicetype = max_slicetype))    

    longh.append(plot_type('Model_vwind',
                           v[80:,max_lat,:],
                           'Southerly Wind / m s-1',
                           plot_specifier = max_lat_label,
                           iswind=True,
                           slicetype = max_slicetype))
    
    if w:
        longh.append(plot_type('Model_wwind',
                               w[80:,max_lat,:],
                               'Vertical Wind / m s-1',
                               plot_specifier = max_lat_label,
                               iswind=True,
                               cbformat = '%2.1f',
                               slicetype = max_slicetype))
    if northgw:
        longh.append(plot_type('Model_MeridGWTend',
                                 northgw[80:,max_lat,:],
                                 'Meridional Wind Tendency / m s-1 d-1',
                                 plot_specifier = max_lat_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = max_slicetype))
    if eastgw:
        longh.append(plot_type('Model_ZonalGWTend',
                                 eastgw[80:,max_lat,:],
                                 'Zonal Wind Tendency / m s-1 d-1',
                                 plot_specifier = max_lat_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = max_slicetype))    
    
    # ...min wind latitude
    
    longh.append(plot_type('Model_Temp',
                            temp[80:,min_lat,:],
                            'Temperature / K',
                            plot_specifier = min_lat_label,
                            slicetype = min_slicetype))
    
    longh.append(plot_type('Model_LW',
                            LW[80:,min_lat,:],
                            'LW Heating / K d-1',
                            plot_specifier = min_lat_label,
                            slicetype = min_slicetype))    
    
    longh.append(plot_type('Model_SW',
                           SW[80:,min_lat,:],
                           'SW Heating / K d-1',
                           plot_specifier = min_lat_label,
                           slicetype = min_slicetype))
    
    longh.append(plot_type('Model_uwind',
                           u[80:,min_lat,:],
                           'Westerly Wind / m s-1',
                           plot_specifier = min_lat_label,
                           iswind=True,
                           slicetype = min_slicetype))    

    longh.append(plot_type('Model_vwind',
                           v[80:,min_lat,:],
                           'Southerly Wind / m s-1',
                           plot_specifier = min_lat_label,
                           iswind=True,
                           slicetype = min_slicetype))
    
    if w:
        longh.append(plot_type('Model_wwind',
                               w[80:,min_lat,:],
                               'Vertical Wind / m s-1',
                               plot_specifier = min_lat_label,
                               iswind=True,
                               cbformat = '%2.1f',
                               slicetype = min_slicetype))
    if northgw:
        longh.append(plot_type('Model_MeridGWTend',
                                 northgw[80:,min_lat,:],
                                 'Meridional Wind Tendency / m s-1 d-1',
                                 plot_specifier = min_lat_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = min_slicetype))
    if eastgw:
        longh.append(plot_type('Model_ZonalGWTend',
                                 eastgw[80:,min_lat,:],
                                 'Zonal Wind Tendency / m s-1 d-1',
                                 plot_specifier = min_lat_label,
                                 iswind=True,
                                 cbformat = '%2.1f',
                                 slicetype = min_slicetype))
    
    #Take upper atmosphere altitude slices for polar sterographic plots at...
    
    # ...max wind altitude
    
    stereo.append(plot_type('Model_LW',
                            LW[max_lev,:,:],
                            'LW Heating / K d-1',
                            plot_specifier = max_height_label,
                            slicetype = max_slicetype))
    
    stereo.append(plot_type('Model_SW',
                           SW[max_lev,:,:],
                           'SW Heating / K d-1',
                           plot_specifier = max_height_label,
                           slicetype = max_slicetype))     
                              
    stereo.append(plot_type('Model_uwind',
                               u[max_lev,:,:],
                               'Westerly Wind / m s-1',
                               plot_specifier = max_height_label,
                               iswind=True,
                               slicetype = max_slicetype))
    
    stereo.append(plot_type('Model_vwind',
                               v[max_lev,:,:],
                               'Southerly Wind / m s-1',
                               plot_specifier = max_height_label,
                               iswind=True,
                               slicetype = max_slicetype))
    
    if w:
        stereo.append(plot_type('Model_wwind',
                               w[max_lev,:,:],
                               'Vertical Wind / m s-1',
                               plot_specifier = max_height_label,
                               iswind=True,
                               cbformat = '%2.1f',
                               slicetype = max_slicetype))
    
    # ...min wind altitude
    
    stereo.append(plot_type('Model_LW',
                            LW[min_lev,:,:],
                            'LW Heating / K d-1',
                            plot_specifier = min_height_label,
                            slicetype = min_slicetype))    
    
    stereo.append(plot_type('Model_SW',
                           SW[min_lev,:,:],
                           'SW Heating / K d-1',
                           plot_specifier = min_height_label,
                           slicetype = min_slicetype))     
                              
    stereo.append(plot_type('Model_uwind',
                               u[min_lev,:,:],
                               'Westerly Wind / m s-1',
                               plot_specifier = min_height_label,
                               iswind=True,
                               slicetype = min_slicetype))
    
    stereo.append(plot_type('Model_vwind',
                               v[min_lev,:,:],
                               'Southerly Wind / m s-1',
                               plot_specifier = min_height_label,
                               iswind=True,
                               slicetype = min_slicetype))
    
    if w:
        stereo.append(plot_type('Model_wwind',
                               w[min_lev,:,:],
                               'Vertical Wind / m s-1',
                               plot_specifier = min_height_label,
                               iswind=True,
                               cbformat = '%2.1f',
                               slicetype = min_slicetype))

    #Take zonal means (lat v height)   
    
    lath.append(plot_type('Model_SW',
                          SW.collapsed(['longitude'],iris.analysis.MEAN),
                          'SW Heating / K d-1',
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_LW',
                          LW.collapsed(['longitude'],iris.analysis.MEAN),
                          'LW Heating / K d-1',
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_uwind',
                          u.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Zonal Wind / m s-1',
                          iswind = True,
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_vwind',
                          v.collapsed(['longitude'],iris.analysis.MEAN),
                          'Mean Meridional Wind / m s-1',
                          iswind = True,
                          plot_specifier = 'ZonalMean'))
    
    lath.append(plot_type('Model_Temp',
                          temp.collapsed(['longitude'],iris.analysis.MEAN),
                          'SW Heating / K d-1',
                          plot_specifier = 'ZonalMean'))
    if northgw:                      
        lath.append(plot_type('Model_MeridGWTend',
                              northgw.collapsed(['longitude'],iris.analysis.MEAN),
                              'Meridional Wind Tendency / m s-1 d-1',
                              iswind = True,
                              cbformat = '%2.1f',
                              plot_specifier = 'ZonalMean'))
    if eastgw:
        lath.append(plot_type('Model_ZonalGWTend',
                              eastgw.collapsed(['longitude'],iris.analysis.MEAN),
                              'Zonal Wind Tendency / m s-1 d-1',
                              iswind = True,
                              cbformat = '%2.1f',
                              plot_specifier = 'ZonalMean'))
    
    ########################################    Plot figures    ########################################
    
    ### Upper atmosphere altitude slice plots ###
    
    for job in longlat:
        
        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, vmin=-top, vmax=top, mesh = mesh_on, iswind = job.iswind)
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
            
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube, mesh = mesh_on)
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
            # Remove white lines between sections
            cb.solids.set_edgecolor("face")
        
        # Title and label axes
        title = job.filename_var + '_at_' + job.slicetype + '_wind_speed_at_' + job.plot_specifier + '_at_' + endtime
        
        labelling(myplot, title = title, xlabel='Longitude / degrees', xticks=np.arange(-180,181,30),
                      ylabel='Latitude / degrees' , yticks=np.arange(-90,91,30), mesh = mesh_on)
        
        
        
        # Add coastlines
        iplt.plt.gca().coastlines()
        
        # Save figure
        iplt.plt.savefig(outputpath + run + job.filename_var + '_' + job.slicetype + '_' + job.plot_specifier + '.png', format='png', dpi=900)
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename_var + '_' + job.slicetype + '_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
    
    ### Upper atmosphere latitude slice plots ###
    
    for job in longh:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = badplot(job.cube, 'longitude', 'level_height',
                             vmin=-top, vmax=top, mesh = mesh_on, iswind = job.iswind)
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
            
        else:
        
            # Plot data on axis
            
            myplot = badplot(job.cube, 'longitude', 'level_height', mesh = mesh_on)
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal', format = job.cbformat, label = job.cblabel)
            # Remove white lines between sections
            cb.solids.set_edgecolor("face")
                    
        # Title and label axes
        title = job.filename_var + '_at_' + job.slicetype + '_wind_speed_at_' + job.plot_specifier + '_at_' + endtime
        
        labelling(myplot, title = title, xlabel='Longitude / degrees', xticks=np.arange(-180,181,30),
                  ylabel='Height / m', mesh = mesh_on)
        
        # Save figure
        iplt.plt.savefig(outputpath + run + job.filename_var + '_' + job.slicetype + '_' + job.plot_specifier + '.png', format='png', dpi=900)
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename_var + '_' + job.slicetype + '_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
    
    ### Zonal Mean plots ###
   
    for job in lath:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, coords = ['latitude','level_height'],
                              vmin=-top, vmax=top, mesh = mesh_on, iswind = job.iswind)
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
            
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube, coords = ['latitude','level_height'], mesh = mesh_on)
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
            # Remove white lines between sections
            cb.solids.set_edgecolor("face")
                    
        # Title and label axes
        title = job.filename_var + '_' + job.plot_specifier + '_at_' + endtime
        
        labelling(myplot, title = title, xlabel='Latitude / degrees', xticks=np.arange(-90,91,30),
                  ylabel='Height / m', mesh = mesh_on)
        
        # Save figure
        iplt.plt.savefig(outputpath + run + job.filename_var + '_' + job.plot_specifier + '.png', format='png', dpi=900)
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename_var + '_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
        
    ### Polar stereographic altitude slice plots ###
    
    for job in stereo:
        
        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        # Plot onto stereographic grid
        
        if 'S' in (max_lat_label and min_lat_label):
            ax = iplt.plt.axes(projection=ccrs.SouthPolarStereo())
            ax.set_extent([-180, 180, -90, -85], ccrs.PlateCarree())
            ax.gridlines()
            ax.coastlines()
            #ax.quiver(x,y,u,v,transform = ccrs.PlateCarree())
        
        elif 'N' in (max_lat_label and min_lat_label):
            ax = iplt.plt.axes(projection=ccrs.NorthPolarStereo())
            ax.set_extent([-180, 180, 85, 90], ccrs.PlateCarree())
            ax.gridlines()
            ax.coastlines()
            #ax.quiver(x,y,u,v,transform = ccrs.PlateCarree())
        else:
            raise Exception('Wind extrema in opposite hemispheres.')
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, vmin=-top, vmax=top, mesh = mesh_on, iswind = job.iswind, stereo = True)
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
            
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube, mesh = mesh_on, stereo = True)
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
            # Remove white lines between sections
            cb.solids.set_edgecolor("face")
                    
        # Title and label axes
        title = job.filename_var + '_at_' + job.plot_specifier + '_at_' + endtime
        
        labelling(myplot, title = title, mesh = mesh_on)
        # Save figure
        iplt.plt.savefig(outputpath + run + job.filename_var + '_' + job.slicetype + '_stereo_' + job.plot_specifier + '.png', format='png', dpi=900)
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename_var + '_' + job.slicetype + '_stereo_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
          
    # Blocking:
    #xx = raw_input('Enter to quit\n')


