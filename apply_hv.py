import iris
import numpy as np
import os
import numpy.ma as ma

# Applies the given Heaviside function (m01s30i301) to a cube to give missing data where a cube is underground (i.e. where there is orography)

# Either:
#RMDI = -1073741824.0
# Or,
RMDI = np.nan

HV_THRESH = 0.05

def apply_hv(hv, cube):
    newcube = cube.copy()
    hv.data = ma.masked_where(hv.data < HV_THRESH, hv.data)
    newcube /= hv
    newcube.data = newcube.data.filled(RMDI)
    newcube.data = ma.masked_where(newcube.data == RMDI, newcube.data)
    newcube.rename(cube.name())
    return newcube

