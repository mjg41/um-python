def vertlevplot(vert_lvls, top):
    import numpy as np
    import matplotlib.pyplot as plt

    # Set up figures and axes

    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, gridspec_kw = {'width_ratios':[1, 9]})
    #f.suptitle('Vertical Gridpoints')
    ax1.set_xticklabels([])
    ax1.set_xticks([])
    ax2.set_xticks(range(0,121,10))
    plt.subplots_adjust(wspace=0.05)
    plt.xlabel("Model Level Number")
    ax1.set_ylabel("Height / m")
    
    # Do plots and show
    
    ax1.plot(np.zeros(np.size(vert_lvls)),vert_lvls*top,'ko',fillstyle='none')
    ax2.plot(vert_lvls*top,'k-o',fillstyle='none')
    #ax2.plot(np.arange(0.,89.),np.exp(np.arange(-88.,1.)/(2.*6.8)),'r-')
    plt.show()
