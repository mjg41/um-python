#!/common/scitools/environments/default/2018_05_22/bin/python

import numpy as np
import iris
from scipy.interpolate import CubicSpline
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

# Import cube

cubes = iris.load('/projects/ukca-ex/mgriffit/output_files/19880901T0000Z/L94_102km/az887_dec.nc')

# Extract coordinates and wind data

lats = cubes[12].coord('latitude').points[:]
longs_u = cubes[12].coord('longitude').points[:]
lats_v = cubes[13].coord('latitude').points[:]
longs = cubes[13].coord('longitude').points[:]
u_mean_orig = cubes[12].data
v_mean_orig = cubes[13].data

# Interpolate to center points (u longitude --> v longitude and v latitude --> u latitude)
interp_lat = CubicSpline(lats_v, v_mean_orig, axis=1)
interp_long = CubicSpline(longs_u,u_mean_orig, axis=2, extrapolate='periodic')

v_mean = interp_lat(lats)
u_mean = interp_long(longs)

# Create plots

X,Y = np.meshgrid(longs,lats)
plt.figure(figsize=(10,8))
ax = plt.axes(projection=ccrs.PlateCarree())
ax.coastlines()
ax.set_xticks(longs_u - 180., crs=ccrs.PlateCarree())
ax.set_yticks(lats_v, crs=ccrs.PlateCarree())
plot = plt.quiver(X,Y,u_mean[-1,:,:],v_mean[-1,:,:], angles='xy', scale_units='xy', scale=10)
plt.grid(axis='both')
ax.set_xlim([-20.,10])
ax.set_ylim([45.,60.])
plt.savefig('/home/d03/mgriffit/Documents/Plots/Model/vec_field.png', format='png', dpi=600)
plt.show()
