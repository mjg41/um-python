import iris
import numpy as np
import os
import sys
import matplotlib
matplotlib.use('AGG')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from matplotlib.ticker import IndexFormatter, AutoMinorLocator
import cartopy.crs as ccrs

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y)) 

# Create class to do dynamic printing on one line

class Printer():
    """Print things to stdout on one line dynamically"""
    def __init__(self,data):
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

# Routine to calculate and plot the temporal and spatial decomposition of the atmospheric tidal variations

def fourier_fit_2d(longs, times, tidal_var):
    '''Takes in model_var and fits a sinusoidal wave with 
    multiple temporal and spatial frequencies.
    
    Positional arguments
    
    longs is the M-array of longitude coordinates
    times is the N-array of time coordinates
    tidal_var is the (N,M)-array of tidal variations from chosen model variable
    
    Output
    
    amps is the 26 fitted amplitudes (2 temporal frequencies, 13 spatial frequencies)
    errs is the 26 standard deviations from the fitting procedure
    '''

    import numpy as np
    import iris
    from scipy.optimize import curve_fit
    
    # Decide modes to be fitted

    modes_time = np.array([1.0, 2.0]) # diurnal and semidiurnal
    modes_space = np.arange(-6.0,7.0) # Eastward 6 to Westward 6 spatial frequencies
    N_modes = len(modes_time)*len(modes_space)

    # Initialise amps and errs

    amps = np.zeros(N_modes)
    errs = np.zeros(N_modes)
    
    # Remove background winds - just fitting sinusoidal oscillation
    
    tidal_var -= np.nanmean(tidal_var)

    # 2D domain

    X,Y = np.meshgrid(longs, times)
    X = X.T
    Y = Y.T

    # Stack meshgrids ready for fit

    xdata = np.vstack((X.ravel(), Y.ravel()))

    # Function to fit; longitudinal and temporal fit to model_var

    # def long_time_fit(x, y, a_d_1, a_d_2, a_d_3, a_d_4, a_d_5, a_d_6, a_d_7, a_d_8, a_d_9, a_d_10, a_d_11, a_d_12, a_d_13,
    #                         a_s_1, a_s_2, a_s_3, a_s_4, a_s_5, a_s_6, a_s_7, a_s_8, a_s_9, a_s_10, a_s_11, a_s_12, a_s_13,
    #                         p_d_1, p_d_2, p_d_3, p_d_4, p_d_5, p_d_6, p_d_7, p_d_8, p_d_9, p_d_10, p_d_11, p_d_12, p_d_13,
    #                         p_s_1, p_s_2, p_s_3, p_s_4, p_s_5, p_s_6, p_s_7, p_s_8, p_s_9, p_s_10, p_s_11, p_s_12, p_s_13):
    #     return (a_d_1 * np.cos(np.pi*y/12 + np.pi*(-6)*x/180 - p_d_1) +
    #             a_d_2 * np.cos(np.pi*y/12 + np.pi*(-5)*x/180 - p_d_2) +
    #             a_d_3 * np.cos(np.pi*y/12 + np.pi*(-4)*x/180 - p_d_3) +
    #             a_d_4 * np.cos(np.pi*y/12 + np.pi*(-3)*x/180 - p_d_4) +
    #             a_d_5 * np.cos(np.pi*y/12 + np.pi*(-2)*x/180 - p_d_5) +
    #             a_d_6 * np.cos(np.pi*y/12 + np.pi*(-1)*x/180 - p_d_6) +
    #             a_d_7 * np.cos(np.pi*y/12 - p_d_7) +
    #             a_d_8 * np.cos(np.pi*y/12 + np.pi*(1)*x/180 - p_d_8) +
    #             a_d_9 * np.cos(np.pi*y/12 + np.pi*(2)*x/180 - p_d_9) +
    #             a_d_10 * np.cos(np.pi*y/12 + np.pi*(3)*x/180 - p_d_10) +
    #             a_d_11 * np.cos(np.pi*y/12 + np.pi*(4)*x/180 - p_d_11) +
    #             a_d_12 * np.cos(np.pi*y/12 + np.pi*(5)*x/180 - p_d_12) +
    #             a_d_13 * np.cos(np.pi*y/12 + np.pi*(6)*x/180 - p_d_13) +
    #             a_s_1 * np.cos(np.pi*y/6 + np.pi*(-6)*x/180 - p_s_1) +
    #             a_s_2 * np.cos(np.pi*y/6 + np.pi*(-5)*x/180 - p_s_2) +
    #             a_s_3 * np.cos(np.pi*y/6 + np.pi*(-4)*x/180 - p_s_3) +
    #             a_s_4 * np.cos(np.pi*y/6 + np.pi*(-3)*x/180 - p_s_4) +
    #             a_s_5 * np.cos(np.pi*y/6 + np.pi*(-2)*x/180 - p_s_5) +
    #             a_s_6 * np.cos(np.pi*y/6 + np.pi*(-1)*x/180 - p_s_6) +
    #             a_s_7 * np.cos(np.pi*y/6 - p_s_7) +
    #             a_s_8 * np.cos(np.pi*y/6 + np.pi*(1)*x/180 - p_s_8) +
    #             a_s_9 * np.cos(np.pi*y/6 + np.pi*(2)*x/180 - p_s_9) +
    #             a_s_10 * np.cos(np.pi*y/6 + np.pi*(3)*x/180 - p_s_10) +
    #             a_s_11 * np.cos(np.pi*y/6 + np.pi*(4)*x/180 - p_s_11) +
    #             a_s_12 * np.cos(np.pi*y/6 + np.pi*(5)*x/180 - p_s_12) +
    #             a_s_13 * np.cos(np.pi*y/6 + np.pi*(6)*x/180 - p_s_13))
            
    # Callable for fit 

    # def _long_time_fit(M, a_d_1, a_d_2, a_d_3, a_d_4, a_d_5, a_d_6, a_d_7, a_d_8, a_d_9, a_d_10, a_d_11, a_d_12, a_d_13,
    #                       a_s_1, a_s_2, a_s_3, a_s_4, a_s_5, a_s_6, a_s_7, a_s_8, a_s_9, a_s_10, a_s_11, a_s_12, a_s_13,
    #                       p_d_1, p_d_2, p_d_3, p_d_4, p_d_5, p_d_6, p_d_7, p_d_8, p_d_9, p_d_10, p_d_11, p_d_12, p_d_13,
    #                       p_s_1, p_s_2, p_s_3, p_s_4, p_s_5, p_s_6, p_s_7, p_s_8, p_s_9, p_s_10, p_s_11, p_s_12, p_s_13):
    #     x,y = M
    #     return long_time_fit(x, y, a_d_1, a_d_2, a_d_3, a_d_4, a_d_5, a_d_6, a_d_7, a_d_8, a_d_9, a_d_10, a_d_11, a_d_12, a_d_13,
    #                                a_s_1, a_s_2, a_s_3, a_s_4, a_s_5, a_s_6, a_s_7, a_s_8, a_s_9, a_s_10, a_s_11, a_s_12, a_s_13,
    #                                p_d_1, p_d_2, p_d_3, p_d_4, p_d_5, p_d_6, p_d_7, p_d_8, p_d_9, p_d_10, p_d_11, p_d_12, p_d_13,
    #                                p_s_1, p_s_2, p_s_3, p_s_4, p_s_5, p_s_6, p_s_7, p_s_8, p_s_9, p_s_10, p_s_11, p_s_12, p_s_13)

    # Do fit
    #import pdb; pdb.set_trace()
    #params, covar = curve_fit(_long_time_fit, xdata, tidal_var.ravel())
    #amps = params[:N_modes]
    #errs = np.sqrt(np.diag(covar)[:N_modes])
    
    # Do individual fits instead

    def make_long_time_diurn_fit(mode):
        def _long_time_diurn_fit(M, a, p):
            x,y = M
            def long_time_diurn_fit(x, y, a, p):
                return a * np.cos(np.pi*y/12 + np.pi*(mode)*x/180 - p)
            return long_time_diurn_fit(x,y,a,p)
        return _long_time_diurn_fit

    def make_long_time_semi_fit(mode):
        def _long_time_semi_fit(M, a, p):
            x,y = M
            def long_time_semi_fit(x, y, a, p):
                return a * np.cos(np.pi*y/6 + np.pi*(mode)*x/180 - p)
            return long_time_semi_fit(x,y,a,p)
        return _long_time_semi_fit

    # Do fitting

    for ii in np.arange(-6.,7.):

        params, covar = curve_fit(make_long_time_diurn_fit(ii), xdata, tidal_var.ravel())

        amps[int(ii+6)] = np.abs(params[0])

        errs[int(ii+6)] = np.sqrt(np.diag(covar)[0])

        params, covar = curve_fit(make_long_time_semi_fit(ii), xdata, tidal_var.ravel())

        amps[int(ii+19)] = np.abs(params[0])

        errs[int(ii+19)] = np.sqrt(np.diag(covar)[0])


    return amps, errs

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/tidal_analysis/'
    #run = input('Please enter path to file relative to "tidal_analysis/":    ')

    run = '20001201T0000Z/N96_L100_123km/'

    #variable = input('Please enter model variable (temp, u, v, w): ')

    variable = 'v'

    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/tidal_analysis/'
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath + run)):
        try:
            os.makedirs(os.path.dirname(outputpath + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    # Load in tidal variations for all months

    months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

    tidal_vars = [None for ii in range(12)]

    for ii, month in enumerate(months):

        files = os.listdir(inputpath + run + month)

        # Load in tidal var

        for fname in files:

            if (variable + '_tidal_var_' + month) in fname:

                tidal_var_file = fname
                break
    
        tidal_vars[ii] = iris.load(inputpath + run + month + '/' + tidal_var_file)[0]

    # Extract coordinates

    times = tidal_vars[0].coord('time').points[:]
    times = times - times[0]

    longs = tidal_vars[0].coord('longitude').points[:]

    lats = tidal_vars[0].coord('latitude').points[:]

    heights = tidal_vars[0].coord('level_height').points[:]

    ### Do fourier fit at various latitudes at 95km ###

    lat_indices = np.arange(0,144,11)

    height_indices = [15, 17, 19, 21] # L100 90, 95, 100, 107 km

    height_labels = ['90km', '95km', '100km', '107km']

    # Set up plot parameters

    subtitles = ['January', 'February', 'March', 'April', 'May', 'June',
                 'July', 'August', 'September', 'October', 'November',
                 'December']

    # Initialise

    counter = 1

    #lat_freq = 11
    lat_freq = 4

    lats_coarse = lats[::lat_freq]

    modes_space = np.arange(-4.0,5.0) # Eastward 4 to Westward 4 spatial frequencies

    de3_heights_lats = np.zeros((12, len(height_indices), len(lats_coarse)))

    for ii, tidal_var in enumerate(tidal_vars):

        for jj, height_ind in enumerate(height_indices):

            for kk, lat_ind in enumerate(np.arange(0,144,lat_freq)):

                amps, _ = fourier_fit_2d(longs, times, tidal_var.data[:,height_ind,lat_ind,:].T)

                de3_heights_lats[ii,jj,kk] = amps[3]

                Printer('Progress: {:2d}/{:2d}'.format(counter, 12*len(height_indices)*len(lats_coarse)))

                counter += 1

    print()

    # Plot de3 components

    fig, axes = plt.subplots(3,4, sharex=True, sharey=True, figsize=[12,6])
        
    fig.suptitle('   DE3 component of ' + variable + ' perturbations at various heights as a function of latitude', fontsize=16)
        
    fig.text(0.5, 0.01, 'Latitude / deg', ha='center', fontsize=16)

    if variable == 'temp':

        fig.text(0.04, 0.5, r'Amplitude / K', va='center', rotation='vertical', fontsize=16)

    else:

        fig.text(0.04, 0.5, r'Amplitude / ms$^{-1}$', va='center', rotation='vertical', fontsize=16)

    amp_ylim = np.amax(de3_heights_lats) + 0.5

    for ii, [ax, subtitle] in enumerate(zip(axes.ravel(), subtitles)):

        for jj, height_label in enumerate(height_labels):

            # Do plot

            ax.plot(lats_coarse, de3_heights_lats[ii,jj,:], label = 'DE3 - ' + height_label)
            ax.set_title(subtitle)

            ax.set_xlim(-90.1,90.1)
            ax.set_xticks(np.arange(-90,91,30))

            ax.set_ylim(0,amp_ylim)

    # Just get latest handles and labels since they are all the same

    handles, labels = ax.get_legend_handles_labels()
                
    fig.legend(handles, labels, loc='lower right', fontsize='large', ncol=1, bbox_to_anchor=(1.1,0.5), framealpha=1.0)

    # Save

    fig.savefig(outputpath + run + variable + '_tidal_var_fourier_2d_DE3_all_heights_all_lats.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.75)

