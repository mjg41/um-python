import iris
from iris.coord_categorisation import add_hour
import numpy as np
import os

def comp_day_month(wind):
    
    # Add categorisation by hour of day
    
    add_hour(wind,'time',name='hour')
    
    # Mean according to this categorisation
    
    composite_day = wind.aggregated_by(['hour'],iris.analysis.MEAN)
    
    # Remove background winds
    
    composite_day.data -= np.mean(composite_day.data, axis=0)
    
    return composite_day

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
    run = input('Please enter path to file relative to "radar_comparison/":    ')
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    # List all files in folder
    
    files = os.listdir(inputpath + run)
    
    # All months and different winds
    
    months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
    wind_list = ['u_asc', 'u_rot', 'v_asc', 'v_rot']
    
    # Initialise list of cubes for 4 winds, each with 12 months
                
    wind_data_list = [[None for ii in range(12)] for jj in range(4)]
    
    for fname in files:
        
        # Check if it's a netcdf file otherwise continue to next item in loop
        
        if ('.nc' not in fname):
            continue
        
        # Now proceed to importing each wind

        for ii, wind in enumerate(wind_list):
            
            if (wind in fname):            
                
                for jj, month in enumerate(months):
                    
                    # For the jj'th month import the ii'th wind into the jj'th position in the list
                    
                    if (month in fname):
                        
                        # Take the zeroth element to get actual cube
                        
                        wind_data_list[ii][jj] = iris.load(inputpath + run + fname)[0]
                        
                        # Create and add month number as aux_coord
                        # Use 'jj+1' so months run from 1 to 12
                        
                        month_coord = iris.coords.AuxCoord(jj+1, var_name = 'month')
                        wind_data_list[ii][jj].add_aux_coord(month_coord)
                        
                        break
    
    # Given all wind data, compute composite day for each month
    
    composite_day_list = [[None for ii in range(12)] for jj in range(4)]
    
    for ii, wind in enumerate(wind_data_list):
        for jj, wind_month in enumerate(wind):
            
            # Compute composite day
            
            composite_day_list[ii][jj] = comp_day_month(wind_month)
            
            # Remove superfluous time and forecast_period coords in preparation for merge
            
            composite_day_list[ii][jj].remove_coord('time')
            composite_day_list[ii][jj].remove_coord('forecast_period')
            
            # Promote new hour coord to dim coord in preparation for merge
            
            iris.util.promote_aux_coord_to_dim_coord(composite_day_list[ii][jj],'hour')
        
        # Make the output directory if it doesn't exist
    
        if not os.path.exists(os.path.dirname('data/MODEL/' + run)):
            try:
                os.makedirs(os.path.dirname('data/MODEL/' + run))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        # Combine each composite day into one file for whole year (4 total files) and
        # save

        wind_year = iris.cube.CubeList(composite_day_list[ii]).merge_cube()
        iris.save(wind_year, inputpath + run + wind_list[ii] + '_comp_day.nc')
        iris.save(wind_year, 'data/MODEL/' + run + wind_list[ii] + '_comp_day.nc')
