#!/usr/bin/python2
# Because I want to use print()
from __future__ import print_function
# Because I want to divide properly
from __future__ import division

import sys
import numpy as np
import iris
import iris.plot as iplt
import cartopy.crs as ccrs

class plot_type(object):
    def __init__(self, filename, cube, cblabel, iswind=False, cbformat = '%3.0f'):
        self.filename = filename
        self.cube = cube
        self.cblabel = cblabel
        self.iswind = iswind
        self.cbformat = cbformat

# PLOTTING FUNCTIONS
def cube_bounds(cube):
    
    # Calculate and print cube maximum and minimum
    
    cubemin, cubemax = [np.nanmin(cube.data), np.nanmax(cube.data)]
    #print("Maximum value: " + str(cubemax) + "    " + "Index: " , np.unravel_index(cube.data.argmax(), cube.data.shape))
    #print("Minimum value: " + str(cubemin) + "    " + "Index: " , np.unravel_index(cube.data.argmin(), cube.data.shape))
    
    return cubemin,cubemax


def extract_from_cube(cube, xlabel, ylabel):
    
    # Extract coordinates from cube
    
    x = cube.coord(xlabel).points[:]
    y = cube.coord(ylabel).points[:]
    z = cube.data
    
    return x, y, z

# Main plotting routine
def cubeplot(cube, N=100, iswind=False, **params):

    # Plot cube, red/blue for winds, normal for everything else
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('brewer_RdBu_11')
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    plot = iplt.contourf( cube,
                        N,
                        cmap = cmap,
                        origin = 'lower', 
                        hold = 'on',
                        **params)
    return plot

## Alternative plotting routine for odd cases
def badplot(cube, xextract, yextract, N=100, iswind=False, **params):
    
    x,y,z = extract_from_cube(cube, xextract, yextract)
    # Change the plot so that longitude goes from [-180,180] instead of [0,360]    
    newx = x - 180
    newz = np.hstack([z[:,x>=180], z[:,x<180]])
    
    if iswind:
        cmap = iplt.plt.cm.get_cmap('brewer_RdBu_11')
    else:
        cmap = iplt.plt.cm.get_cmap('jet')
    
    plot = iplt.plt.contourf( newx, y, newz,
                            N, 
                            cmap = cmap, 
                            origin = 'lower', 
                            hold = 'on',
                            **params)
    return plot

## Add labels
def labelling(plot, title='', xlabel='', xticks=None, ylabel='', yticks=None):
    plot.ax.set_title(title)
    
    plot.ax.set_xlabel(xlabel)
    if not xticks is None:
        plot.ax.set_xticks(xticks)
    
    plot.ax.set_ylabel(ylabel)
    if not yticks is None:
        plot.ax.set_yticks(yticks)
    return plot

## Add symmetric colorbar about 0 for wind cases
def add_colorbar(plot, z, cb = None, **params):
    # Make mapable
    mymap = iplt.plt.cm.ScalarMappable(cmap=plot.get_cmap())
    mymap.set_array(z)
    
    # Get limits and set new limits
    top = plot.get_clim()[1]
    mymap.set_clim(-top, top)
    
    # Add colorbar
    cb = iplt.plt.colorbar(mymap, **params)
    
    return cb

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Choose input filepath

    inputpath = '/projects/ukca-ex/mgriffit/output_files/'
    run = raw_input('Please enter path to file relative to "output_files/": ')
    ncfile = raw_input('Please enter NetCDF file name: ')
    
    # Choose output filepath
    
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/'
    
    # Load in cube
      
    cubes = iris.load(inputpath + run + ncfile)
    
    longlat = []
    longh = []
    lath = []
    stereo = []
    
    # Split cube into variables and convert units
    
    pressure = cubes.extract(iris.Constraint('air_pressure'))[0]
    pressure.convert_units('hPa')
    
    temp = cubes.extract(iris.Constraint('air_temperature'))[0]
    
    LW = cubes.extract(iris.Constraint('tendency_of_air_temperature_due_to_longwave_heating'))[0]
    LW.convert_units('K d-1')
    
    SW = cubes.extract(iris.Constraint('tendency_of_air_temperature_due_to_shortwave_heating'))[0]
    SW.convert_units('K d-1')
    
    if len(cubes) == 7:
        
        # Cubes containing vertical wind
        print('Cube with vertical wind')
        
        w = cubes.extract(iris.Constraint('upward_air_velocity'))[0]
        
        u = cubes.extract(iris.Constraint('x_wind'))[0]
        
        v = cubes.extract(iris.Constraint('y_wind'))[0]
        
    elif len(cubes) == 6:
    
        # Cubes not containing vertical wind
        print('Cube without vertical wind')
        
        w=[]
        
        u = cubes.extract(iris.Constraint('x_wind'))[0]
        
        v = cubes.extract(iris.Constraint('y_wind')) [0]
    
    # Take top slices (long v lat)

    longlat.append(plot_type('Model_Top_Pressure',
                            pressure[-1,:,:],
                            'Air Pressure / hPa',
                            cbformat = '%1.1e'))
    
    longlat.append(plot_type('Model_Top_Temp',
                             temp[-1,:,:],
                             'Temperature / K'))
    
    longlat.append(plot_type('Model_Top_LW',
                             LW[-1,:,:],
                             'LW Heating / K d-1'))
    
    longlat.append(plot_type('Model_Top_SW',
                             SW[-1,:,:],
                             'SW Heating / K d-1'))
    
    longlat.append(plot_type('Model_Top_uwind',
                             u[-1,:,:],
                             'Westerly Wind / m s-1',
                             iswind=True))
    
    longlat.append(plot_type('Model_Top_vwind',
                             v[-1,:,:],
                             'Southerly Wind / m s-1',
                             iswind=True))
    
    pole = raw_input('Which Pole? (N or S)')
    
    if pole == 'S':
        
        #Take upper atmosphere south polar slices (long v height)
        print('South Polar slices taken')
        
        longh.append(plot_type('Model_Temp_90S',
                                temp[82:,0,:],
                                'Temperature / K'))
        
        longh.append(plot_type('Model_LW_90S',
                                LW[82:,0,:],
                                'LW Heating / K d-1'))    
        
        longh.append(plot_type('Model_SW_90S',
                               SW[82:,0,:],
                               'SW Heating / K d-1'))
        
        longh.append(plot_type('Model_uwind_90S',
                               u[82:,0,:],
                               'Westerly Wind / m s-1',
                               iswind=True))    

        longh.append(plot_type('Model_vwind_90S',
                               v[82:,0,:],
                               'Southerly Wind / m s-1',
                               iswind=True))
        
        if w:
            longh.append(plot_type('Model_wwind_90S',
                                   w[82:,0,:],
                                   'Vertical Wind / m s-1',
                                   iswind=True))
        
        #Take top slices for south polar sterographic plot
        
        stereo.append(plot_type('Model_Top_LW_SP',
                                LW[-1,:,:],
                                'LW Heating / K d-1'))    
        
        stereo.append(plot_type('Model_Top_SW_SP',
                               SW[-1,:,:],
                               'SW Heating / K d-1'))     
                                  
        stereo.append(plot_type('Model_Top_uwind_SP',
                                   u[-1,:,:],
                                   'Westerly Wind / m s-1',
                                   iswind=True))
        
        stereo.append(plot_type('Model_Top_vwind_SP',
                                   v[-1,:,:],
                                   'Southerly Wind / m s-1',
                                   iswind=True))

    else:
        
        #Take upper atmosphere north polar slices (long v height)
        print('North Polar slices taken')
        
        longh.append(plot_type('Model_Temp_90N',
                                temp[82:,-1,:],
                                'Temperature / K'))
        
        longh.append(plot_type('Model_LW_90N',
                                LW[82:,-1,:],
                                'LW Heating / K d-1'))    
        
        longh.append(plot_type('Model_SW_90N',
                               SW[82:,-1,:],
                               'SW Heating / K d-1'))
        
        longh.append(plot_type('Model_uwind_90N',
                               u[82:,-1,:],
                               'Westerly Wind / m s-1',
                               iswind=True))    

        longh.append(plot_type('Model_vwind_90N',
                               v[82:,-1,:],
                               'Southerly Wind / m s-1',
                               iswind=True))
        if w:
            longh.append(plot_type('Model_wwind_90N',
                                   w[82:,-1,:],
                                   'Vertical Wind / m s-1',
                                   iswind=True))

        #Take top slices for south polar sterographic plot
        
        stereo.append(plot_type('Model_Top_LW_NP',
                                LW[-1,:,:],
                                'LW Heating / K d-1'))    
        
        stereo.append(plot_type('Model_Top_SW_NP',
                               SW[-1,:,:],
                               'SW Heating / K d-1'))     
                                  
        stereo.append(plot_type('Model_Top_uwind_NP',
                                   u[-1,:,:],
                                   'Westerly Wind / m s-1',
                                   iswind=True))
        
        stereo.append(plot_type('Model_Top_vwind_NP',
                                   v[-1,:,:],
                                   'Southerly Wind / m s-1',
                                   iswind=True))
                                          
    #Take SW zonal mean (lat v height)   
    
    lath.append(plot_type('Model_SW_ZonalMean',
                          SW.collapsed(['longitude'],iris.analysis.MEAN),
                          'SW Heating / K d-1'))
    
    ########################################    Plot figures    ########################################
    
    ### Top plots ###
    
    for job in longlat:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, vmin=-top, vmax=top, iswind = job.iswind)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
        
        # Title and label axes
        labelling(myplot, xlabel='Longitude / degrees', xticks=np.arange(-180,181,30),
                      ylabel='Latitude / degrees' , yticks=np.arange(-90,91,30))
        
        
        
        # Add coastlines
        iplt.plt.gca().coastlines()
        
        # Save figure
        
        iplt.plt.savefig(outputpath+run+job.filename + '.eps')
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
    
    ### Polar plots ###
    
    for job in longh:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = badplot(job.cube, 'longitude', 'level_height', vmin=-top, vmax=top, iswind = job.iswind)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
        else:
        
            # Plot data on axis
            
            myplot = badplot(job.cube, 'longitude', 'level_height')
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal', format = job.cbformat, label = job.cblabel)
        
        # Title and label axes
        labelling(myplot, xlabel='Longitude / degrees', xticks=np.arange(-180,181,30), ylabel='Height / m')
        
        # Save figure
        
        iplt.plt.savefig(outputpath+run+job.filename + '.eps')
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
    
    ### Zonal Mean plot ###
   
    for job in lath:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, vmin=-top, vmax=top, iswind = job.iswind)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
        
        # Title and label axes
        labelling(myplot, xlabel='Latitude / degrees', xticks=np.arange(-90,91,30),
                      ylabel='Height / m')
        
        # Save figure
        
        iplt.plt.savefig(outputpath+run+job.filename + '.eps')
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
        
    ### Stereographic Plots ###
    
    for job in stereo:
        
        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        # Plot onto stereographic grid
        
        if pole == 'S':
            ax = iplt.plt.axes(projection=ccrs.SouthPolarStereo())
            ax.set_extent([-180, 180, -90, -75], ccrs.PlateCarree())
            ax.gridlines()
            ax.coastlines()
            #ax.quiver(x,y,u,v,transform = ccrs.PlateCarree())
        
        else:
            ax = iplt.plt.axes(projection=ccrs.NorthPolarStereo())
            ax.set_extent([-180, 180, 75, 90], ccrs.PlateCarree())
            ax.gridlines()
            ax.coastlines()
            #ax.quiver(x,y,u,v,transform = ccrs.PlateCarree())
        
        if job.iswind:
            
            # Plot data on axis
            
            myplot = cubeplot(job.cube, axes=ax, vmin=-top, vmax=top, iswind = job.iswind)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
        else:
        
            # Plot data on axis
            
            myplot = cubeplot(job.cube, axes=ax)
            
            # This is the fix for the white lines between contour levels
            
            for c in myplot.collections:
                c.set_edgecolor("face")
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
        
        # Save figure
        
        iplt.plt.savefig(outputpath+run+job.filename + '.eps')
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
          
    # Blocking:
    xx = raw_input('Enter to quit\n')
    



