# Generic function to compute difference between two datasets if dimensions agree.
# INPUT: variable name - e.g. temp, wind, GW
#    path to netcdf 1 - relative to 'output_files'
#    path to netcdf 2 - relative to 'output_files'
# OUTPUT: plot to screen
#     figure saved to file

import matplotlib
matplotlib.use('AGG')
import os
import numpy as np
import iris
import iris.plot as iplt
import sys
from scipy.interpolate import interp1d

class plot_type(object):
    def __init__(self, filename_var, cube, cblabel, plot_specifier='', redblue=False, cbformat = '%3.0f', slicetype=''):
        self.filename_var = filename_var
        self.cube = cube
        self.cblabel = cblabel
        self.plot_specifier = plot_specifier
        self.redblue = redblue
        self.cbformat = cbformat
        self.slicetype = slicetype


# PLOTTING FUNCTIONS
def cube_bounds(cube):
    
    # Calculate and print cube maximum and minimum
    
    cubemin, cubemax = [np.nanmin(cube.data), np.nanmax(cube.data)]
    
    return cubemin,cubemax 

# Main plotting routine
def dataplot(data, x, y, N=100, redblue=False, mesh=False, **params):
        
    if redblue:
        cmap = iplt.plt.cm.get_cmap('RdBu_r', 29)
    else:
        # Use perceptually uniform version of jet from colorcet
        rainbow = ['#0034f8', '#0037f6', '#003af3', '#003df0', '#003fed', '#0041ea', '#0044e7', '#0046e4', '#0048e1', '#004ade',
                   '#004cdb', '#004fd8', '#0051d5', '#0053d2', '#0054d0', '#0056cd', '#0058ca', '#005ac7', '#005cc4', '#005ec1',
                   '#0060be', '#0061bb', '#0063b8', '#0065b6', '#0066b3', '#0068b0', '#006aad', '#006baa', '#006da7', '#006ea5',
                   '#006fa2', '#00719f', '#00729d', '#00739a', '#007598', '#007695', '#077793', '#0d7890', '#13798e', '#187a8b',
                   '#1c7b89', '#1f7c87', '#237d84', '#267e82', '#287f7f', '#2b807d', '#2d817b', '#2f8278', '#318376', '#328473',
                   '#348571', '#35866f', '#36876c', '#37886a', '#388967', '#398a65', '#3a8b62', '#3b8c60', '#3c8e5d', '#3c8f5b',
                   '#3d9058', '#3d9155', '#3e9253', '#3e9350', '#3e944d', '#3e954a', '#3e9647', '#3f9745', '#3f9842', '#3e993e',
                   '#3e9a3b', '#3e9b38', '#3e9c35', '#3e9d32', '#3e9e2e', '#3e9f2b', '#3fa027', '#3fa124', '#40a221', '#41a31d',
                   '#42a41a', '#44a517', '#45a615', '#47a713', '#4aa711', '#4ca80f', '#4fa90e', '#51a90d', '#54aa0d', '#57ab0d',
                   '#5aab0d', '#5dac0d', '#5fad0d', '#62ad0e', '#65ae0e', '#67ae0e', '#6aaf0f', '#6db00f', '#6fb00f', '#72b110',
                   '#74b110', '#77b211', '#79b211', '#7cb311', '#7eb412', '#80b412', '#83b512', '#85b513', '#88b613', '#8ab613',
                   '#8cb714', '#8fb814', '#91b815', '#93b915', '#95b915', '#98ba16', '#9aba16', '#9cbb16', '#9fbb17', '#a1bc17',
                   '#a3bc18', '#a5bd18', '#a7be18', '#aabe19', '#acbf19', '#aebf19', '#b0c01a', '#b2c01a', '#b5c11b', '#b7c11b',
                   '#b9c21b', '#bbc21c', '#bdc31c', '#c0c31c', '#c2c41d', '#c4c41d', '#c6c51d', '#c8c51e', '#cac61e', '#cdc61f',
                   '#cfc71f', '#d1c71f', '#d3c820', '#d5c820', '#d7c920', '#d9c921', '#dcca21', '#deca22', '#e0ca22', '#e2cb22',
                   '#e4cb23', '#e6cc23', '#e8cc23', '#eacc24', '#eccd24', '#eecd24', '#f0cd24', '#f2cd24', '#f3cd24', '#f5cc24',
                   '#f6cc24', '#f8cb24', '#f9ca24', '#f9c923', '#fac823', '#fbc722', '#fbc622', '#fcc521', '#fcc421', '#fcc220',
                   '#fdc120', '#fdc01f', '#fdbe1f', '#fdbd1e', '#febb1d', '#feba1d', '#feb91c', '#feb71b', '#feb61b', '#feb51a',
                   '#ffb31a', '#ffb219', '#ffb018', '#ffaf18', '#ffae17', '#ffac16', '#ffab16', '#ffa915', '#ffa815', '#ffa714',
                   '#ffa513', '#ffa413', '#ffa212', '#ffa111', '#ff9f10', '#ff9e10', '#ff9c0f', '#ff9b0e', '#ff9a0e', '#ff980d',
                   '#ff970c', '#ff950b', '#ff940b', '#ff920a', '#ff9109', '#ff8f08', '#ff8e08', '#ff8c07', '#ff8b06', '#ff8905',
                   '#ff8805', '#ff8604', '#ff8404', '#ff8303', '#ff8102', '#ff8002', '#ff7e01', '#ff7c01', '#ff7b00', '#ff7900',
                   '#ff7800', '#ff7600', '#ff7400', '#ff7200', '#ff7100', '#ff6f00', '#ff6d00', '#ff6c00', '#ff6a00', '#ff6800',
                   '#ff6600', '#ff6400', '#ff6200', '#ff6100', '#ff5f00', '#ff5d00', '#ff5b00', '#ff5900', '#ff5700', '#ff5500',
                   '#ff5300', '#ff5000', '#ff4e00', '#ff4c00', '#ff4a00', '#ff4700', '#ff4500', '#ff4200', '#ff4000', '#ff3d00',
                   '#ff3a00', '#ff3700', '#ff3400', '#ff3100', '#ff2d00', '#ff2a00']
        cmap = ListedColormap(rainbow)
        #cmap = iplt.plt.cm.get_cmap('jet')
    
    if mesh:
        plot = iplt.plt.pcolormesh( x, y, data,
                                  cmap = cmap,
                                  **params)
        plot.set_edgecolor('face')
        # This is the fix for the white lines between levels
        # Remove whitespace
        
        plot.axes.axis('tight')
    
    else:
        plot = iplt.plt.contourf( x, y, data,
                                N, 
                                cmap = cmap, 
                                origin = 'lower', 
                                hold = 'on',
                                **params)  
        # iplt.plt.ylim(80000.,120000.)
        # This is the fix for the white lines between contour levels
            
        for c in plot.collections:
            c.set_edgecolor("face")
    
    return plot

## Add labels
def labelling(plot, title='', xlabel='', xticks=None, ylabel='', yticks=None, mesh = False):
    if mesh:
        plot.axes.set_title(title, fontsize = 12)
        
        plot.axes.set_xlabel(xlabel)
        if not xticks is None:
            plot.axes.set_xticks(xticks)
    
        plot.axes.set_ylabel(ylabel)
        if not yticks is None:
            plot.axes.set_yticks(yticks)
    else:    
        plot.ax.set_title(title, fontsize = 12)
        
        plot.ax.set_xlabel(xlabel)
        if not xticks is None:
            plot.ax.set_xticks(xticks)
    
        plot.ax.set_ylabel(ylabel)
        if not yticks is None:
            plot.ax.set_yticks(yticks)
    
    return plot
    
## Add symmetric colorbar about 0 for wind cases
def add_colorbar(plot, z, cb = None, **params):
    # Make mapable
    mymap = iplt.plt.cm.ScalarMappable(cmap=plot.get_cmap())
    mymap.set_array(z)
    
    # Get limits and set new limits
    top = plot.get_clim()[1]
    mymap.set_clim(-top, top)
    
    # Add colorbar
    cb = iplt.plt.colorbar(mymap, **params)
    # And remove white lines between
    cb.solids.set_edgecolor("face")
    
    return cb

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Create paths to two chosen files

    inputpath = '/projects/ukca-ex/mgriffit/output_files/'
    

    # Choose whether to use mesh plots or contour plots
    
    method = input('Mesh or Contour? (m or c)')
    if method == 'm': 
        mesh_on = True
    elif method == 'c':
        mesh_on = False

    # Choose output filepath
    
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/diffplots/'

    # Change outputpath based on choice of mesh or contour plot

    if method == 'm':
        outputpath += 'mesh/' # add mesh to outputpath
    elif method == 'c':
        ouputpath += 'contour/' # add contour to outputpath
    
    # Add date subfolder to outputpath
    
    # Get first month from first netcdf name (chop off .nc and then take month to right of underscore)
    outputpath += sys.argv[2].split('.')[0].split('_')[-1]
    outputpath += 'v'

    # Get second month from second netcdf name (chop off .nc and then take month to right of underscore)
    outputpath += sys.argv[3].split('.')[0].split('_')[-1]
    outputpath += '/'
    
    # Make the directory if it doesn't exist
    if not os.path.exists(os.path.dirname(outputpath)):
        try:
            os.makedirs(os.path.dirname(outputpath))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    # Load in cubes
    
    cubes1 = iris.load(inputpath + sys.argv[2])
    cubes2 = iris.load(inputpath + sys.argv[3])
    
    # Initialise plotting lists
    
    lath = []
    
    # Decide what variables to read based on user input and form appropriate plot objects

    if sys.argv[1] == 'temp':
        
        temp1 = cubes1.extract(iris.Constraint('air_temperature'))[0]
        temp2 = cubes2.extract(iris.Constraint('air_temperature'))[0]
        
        # Get coordinate fields
    
        latitude = temp1.coord('latitude').points[:]
        level_height1 = temp1.coord('level_height').points[:].filled() # So array is not masked
        level_height2 = temp2.coord('level_height').points[:].filled() # So array is not masked
        
        # Take zonal means (lat v height)   
        
        temp1_zonal = temp1.collapsed(['longitude'],iris.analysis.MEAN)
        temp2_zonal = temp2.collapsed(['longitude'],iris.analysis.MEAN)
        
        # Interpolate lower height field up to higher height field (with nans in gap)
        
        if level_height1[-1] >= level_height2[-1]:
            
            level_height = level_height1
            temp1_zonal_data = temp1_zonal.data
            f_interp_height = interp1d(level_height2, temp2_zonal.data, axis=0, bounds_error=False, kind='cubic')
            temp2_zonal_data = f_interp_height(level_height)
            
        else:
            level_height = level_height2
            temp2_zonal_data = temp2_zonal.data
            f_interp_height = interp1d(level_height1, temp1_zonal.data, axis=0, bounds_error=False, kind='cubic')
            temp1_zonal_data = f_interp_height(level_height)
            
        # Take diff
        
        temp_diff = temp1_zonal_data - temp2_zonal_data
    
        # Just above 85 km

        temp_diff = temp_diff[-12:,:]

        level_height = level_height[-12:]

         # Add diff to plotting list
    
        lath.append(plot_type('Model_tempmean',
                              temp_diff,
                              'Air Temperature / K',
                              redblue = True,
                              plot_specifier = 'ZonalMeanDiff'))
    elif sys.argv[1] == 'u':
    
        u_mean_constraint = iris.Constraint('x_wind',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
        u_mean1 = cubes1.extract(u_mean_constraint)[0]
        u_mean2 = cubes2.extract(u_mean_constraint)[0]
        
        # Get coordinate fields
        
        latitude = u_mean1.coord('latitude').points[:]
        level_height1 = u_mean1.coord('level_height').points[:].filled() # So array is not masked
        level_height2 = u_mean2.coord('level_height').points[:].filled() # So array is not masked
            
        # Take zonal means (lat v height) 
        
        u_mean1_zonal = u_mean1.collapsed(['longitude'],iris.analysis.MEAN)
        u_mean2_zonal = u_mean2.collapsed(['longitude'],iris.analysis.MEAN)
        
        # Interpolate lower height field up to higher height field (with nans in gap)
        
        if level_height1[-1] >= level_height2[-1]:
            
            level_height = level_height1
            u_mean1_zonal_data = u_mean1_zonal.data
            f_interp_height = interp1d(level_height2, u_mean2_zonal.data, axis=0, bounds_error=False, kind='cubic')
            u_mean2_zonal_data = f_interp_height(level_height)
            
        else:
            level_height = level_height2
            u_mean2_zonal_data = u_mean2_zonal.data
            f_interp_height = interp1d(level_height1, u_mean1_zonal.data, axis=0, bounds_error=False, kind='cubic')
            u_mean1_zonal_data = f_interp_height(level_height)
        
        # Take diff
        
        u_mean_diff = u_mean1_zonal_data - u_mean2_zonal_data
        
        # Just above 85 km

        u_mean_diff = u_mean_diff[-12:,:]

        level_height = level_height[-12:]
    
         # Add diff to plotting list
        
        lath.append(plot_type('Model_uwindmean',
                              u_mean_diff,
                              'Mean Zonal Wind / m s-1',
                              redblue = True,
                              plot_specifier = 'ZonalMeanDiff'))
    elif sys.argv[1] == 'v':

        v_mean_constraint = iris.Constraint('y_wind',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
        v_mean1 = cubes1.extract(v_mean_constraint)[0]
        v_mean2 = cubes2.extract(v_mean_constraint)[0]
        
        # Get coordinate fields
        
        latitude = v_mean1.coord('latitude').points[:]
        level_height1 = v_mean1.coord('level_height').points[:].filled() # So array is not masked
        level_height2 = v_mean2.coord('level_height').points[:].filled() # So array is not masked
        
        # Take zonal means (lat v height) 
        
        v_mean1_zonal = v_mean1.collapsed(['longitude'],iris.analysis.MEAN)
        v_mean2_zonal = v_mean2.collapsed(['longitude'],iris.analysis.MEAN)
        
        # Interpolate lower height field up to higher height field (with nans in gap)
        
        if level_height1[-1] >= level_height2[-1]:
            
            level_height = level_height1
            v_mean1_zonal_data = v_mean1_zonal.data
            f_interp_height = interp1d(level_height2, v_mean2_zonal.data, axis=0, bounds_error=False, kind='cubic')
            v_mean2_zonal_data = f_interp_height(level_height)
            
        else:
            level_height = level_height2
            v_mean2_zonal_data = v_mean2_zonal.data
            f_interp_height = interp1d(level_height1, v_mean1_zonal.data, axis=0, bounds_error=False, kind='cubic')
            v_mean1_zonal_data = f_interp_height(level_height)
        
        # Take diff
        
        v_mean_diff = v_mean1_zonal_data - v_mean2_zonal_data
        
        # Just above 85 km

        v_mean_diff = v_mean_diff[-12:,:]

        level_height = level_height[-12:]
        
         # Add diff to plotting list
        
        lath.append(plot_type('Model_vwindmean',
                              v_mean_diff,
                              'Mean Meridional Wind / m s-1',
                              redblue = True,
                              plot_specifier = 'ZonalMeanDiff'))
    elif sys.argv[1] == 'w':
    
        w_mean_constraint = iris.Constraint('upward_air_velocity',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
        w_mean1 = cubes1.extract(w_mean_constraint)[0]
        w_mean2 = cubes2.extract(w_mean_constraint)[0]
        
        # Get coordinate fields
        
        latitude = w_mean1.coord('latitude').points[:]
        level_height1 = w_mean1.coord('level_height').points[:].filled() # So array is not masked
        level_height2 = w_mean2.coord('level_height').points[:].filled() # So array is not masked

        # Take zonal means (lat v height) 
        
        w_mean1_zonal = w_mean1.collapsed(['longitude'],iris.analysis.MEAN)
        w_mean2_zonal = w_mean2.collapsed(['longitude'],iris.analysis.MEAN)
        
        # Interpolate lower height field up to higher height field (with nans in gap)
        
        if level_height1[-1] >= level_height2[-1]:
            
            level_height = level_height1
            w_mean1_zonal_data = w_mean1_zonal.data
            f_interp_height = interp1d(level_height2, w_mean2_zonal.data, axis=0, bounds_error=False, kind='cubic')
            w_mean2_zonal_data = f_interp_height(level_height)
            
        else:
            level_height = level_height2
            w_mean2_zonal_data = w_mean2_zonal.data
            f_interp_height = interp1d(level_height1, w_mean1_zonal.data, axis=0, bounds_error=False, kind='cubic')
            w_mean1_zonal_data = f_interp_height(level_height)
        
        # Take diff
        
        w_mean_diff = w_mean1_zonal_data - w_mean2_zonal_data 
                        
        # Just above 85 km

        w_mean_diff = w_mean_diff[-12:,:]

        level_height = level_height[-12:]

         # Add diff to plotting list
        
        lath.append(plot_type('Model_wwindmean',
                              w_mean_diff,
                              'Mean Vertical Wind / m s-1',
                              redblue = True,
                              cbformat = '%.2f',
                              plot_specifier = 'ZonalMeanDiff'))
    elif sys.argv[1] == 'northgw':
    
        northgw_mean_constraint = iris.Constraint('tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
        northgw_mean1 = cubes1.extract(northgw_mean_constraint)[0]
        northgw_mean1.convert_units('m s-1 d-1')
        northgw_mean2 = cubes2.extract(northgw_mean_constraint)[0]
        northgw_mean2.convert_units('m s-1 d-1')
        
        # Get coordinate fields
        
        latitude = northgw_mean1.coord('latitude').points[:]
        level_height1 = northgw_mean1.coord('level_height').points[:].filled() # So array is not masked
        level_height2 = northgw_mean2.coord('level_height').points[:].filled() # So array is not masked
        
        # Take zonal means (lat v height)
        
        northgw_mean1_zonal = northgw_mean1.collapsed(['longitude'],iris.analysis.MEAN)
        northgw_mean2_zonal = northgw_mean2.collapsed(['longitude'],iris.analysis.MEAN)
        
        # Interpolate lower height field up to higher height field (with nans in gap)
        
        if level_height1[-1] >= level_height2[-1]:
            
            level_height = level_height1
            northgw_mean1_zonal_data = northgw_mean1_zonal.data
            f_interp_height = interp1d(level_height2, northgw_mean2_zonal.data, axis=0, bounds_error=False, kind='cubic')
            northgw_mean2_zonal_data = f_interp_height(level_height)
            
        else:
            level_height = level_height2
            northgw_mean2_zonal_data = northgw_mean2_zonal.data
            f_interp_height = interp1d(level_height1, northgw_mean1_zonal.data, axis=0, bounds_error=False, kind='cubic')
            northgw_mean1_zonal_data = f_interp_height(level_height)
        
        # Take diff
        
        northgw_mean_diff = northgw_mean1_zonal_data - northgw_mean2_zonal_data
        
        # Add diff to plotting list
        
        lath.append(plot_type('Model_MeridGWTendmean',
                              northgw_mean_diff,
                              'Meridional Wind Tendency / m s-1 d-1',
                              redblue = True,
                              cbformat = '%2.1f',
                              plot_specifier = 'ZonalMeanDiff'))
    elif sys.argv[1] == 'eastgw':
    
        eastgw_mean_constraint = iris.Constraint('tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag',cube_func = lambda cube: cube.cell_methods[0].method == 'mean')
        eastgw_mean1 = cubes1.extract(eastgw_mean_constraint)[0]
        eastgw_mean1.convert_units('m s-1 d-1')
        eastgw_mean2 = cubes2.extract(eastgw_mean_constraint)[0]
        eastgw_mean2.convert_units('m s-1 d-1')
        
        # Get coordinate fields
        
        latitude = eastgw_mean1.coord('latitude').points[:]
        level_height1 = eastgw_mean1.coord('level_height').points[:].filled() # So array is not masked
        level_height2 = eastgw_mean2.coord('level_height').points[:].filled() # So array is not masked
        
        # Take zonal means (lat v height)
        
        eastgw_mean1_zonal = eastgw_mean1.collapsed(['longitude'],iris.analysis.MEAN)
        eastgw_mean2_zonal = eastgw_mean2.collapsed(['longitude'],iris.analysis.MEAN)
        
        # Interpolate lower height field up to higher height field (with nans in gap)
        
        if level_height1[-1] >= level_height2[-1]:
            
            level_height = level_height1
            eastgw_mean1_zonal_data = eastgw_mean1_zonal.data
            f_interp_height = interp1d(level_height2, eastgw_mean2_zonal.data, axis=0, bounds_error=False, kind='cubic')
            eastgw_mean2_zonal_data = f_interp_height(level_height)
            
        else:
            level_height = level_height2
            eastgw_mean2_zonal_data = eastgw_mean2_zonal.data
            f_interp_height = interp1d(level_height1, eastgw_mean1_zonal.data, axis=0, bounds_error=False, kind='cubic')
            eastgw_mean1_zonal_data = f_interp_height(level_height)
        
        # Take diff
        
        eastgw_mean_diff = eastgw_mean1_zonal_data - eastgw_mean2_zonal_data 
        
        # Add diff to plotting list
        
        lath.append(plot_type('Model_ZonalGWTendmean',
                              eastgw_mean_diff,
                              'Zonal Wind Tendency / m s-1 d-1',
                              redblue = True,
                              cbformat = '%2.1f',
                              plot_specifier = 'ZonalMeanDiff'))
    
    ########################################    Plot figures    ########################################
    
    for job in lath:

        # Find symmetric limits
        cubemin , cubemax = cube_bounds(job.cube)
        top = np.max((np.abs(cubemax), np.abs(cubemin)))
        
        if job.redblue:
            
            # Plot data on axis
            
            myplot = dataplot(job.cube, latitude, level_height,
                              vmin=-top, vmax=top, mesh = mesh_on, redblue = job.redblue)
            
            # Add funky colorbar
            cb = add_colorbar(myplot, job.cube,
                              orientation='horizontal',format = job.cbformat,label = job.cblabel)
            
        else:
        
            # Plot data on axis
            
            myplot = dataplot(job.cube, latitude, level_height, mesh = mesh_on)
        
            # Add funky colorbar
            cb = iplt.plt.colorbar(myplot, orientation='horizontal',format = job.cbformat,label = job.cblabel)
            # Remove white lines between sections
            cb.solids.set_edgecolor("face")
                    
        # Title and label axes
        title = job.filename_var + '_' + job.plot_specifier
        
        labelling(myplot, title = title, xlabel='Latitude / degrees', xticks=np.arange(-90,91,30),
                  ylabel='Height / m', mesh = mesh_on)
        
        # Save figure
    
        path1_parts = str.split(sys.argv[2],'/')
        path2_parts = str.split(sys.argv[3],'/')
        dates_path = path1_parts[0] + 'v' + path2_parts[0]
        levels_path = path1_parts[1] + 'v' + path2_parts[1]
        iplt.plt.savefig(outputpath + job.filename_var + '_' + job.plot_specifier + '_' + dates_path + '_' + levels_path + '.png', format='png', dpi=500, bbox_inches = 'tight', pad_inches = 0.2)
        #iplt.plt.show()
        #xx = raw_input('Enter to close figure\n')
        print(job.filename_var + '_' + job.plot_specifier + ' plot successfully saved!')
        iplt.plt.clf()
        iplt.plt.close('all')
              
    # Blocking:
    # xx = raw_input('Enter to quit\n')


