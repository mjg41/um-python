import os

locale = os.getuid()
xcsc = 11432
mac = 501

# Setup depending on locale

if locale == xcsc:
    
    # Fix for crashing matplotlib on xcsc
    
    import matplotlib
    matplotlib.use('agg')
    
    # Setup directories
    
    indir = 'data/'
    outdir = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'

elif locale == mac:
    
    # Setup directories
    
    indir = 'data/'
    outdir = '/Users/mjg41/Documents/PhD/Plots/Model/radar_comparison/'

import iris
import matplotlib.pyplot as plt
from matplotlib.ticker import IndexFormatter, FuncFormatter
import numpy as np
from scipy.ndimage.filters import gaussian_filter

## Add symmetric colorbar about 0 for wind cases
def add_colorbar(plot, z, cb = None, **params):
    # Make mapable
    mymap = plt.cm.ScalarMappable(cmap=plot.get_cmap())
    mymap.set_array(z)
    
    # Get limits and set new limits
    top = plot.get_clim()[1]
    
    # top = 41.0 # Hard code top if fixing the colourbar limits
    
    mymap.set_clim(-top, top)
    
    # Add colorbar
    cb = plt.colorbar(mymap, **params)
    # And remove white lines between
    cb.solids.set_edgecolor("face")
    
    return cb

# MAIN
if __name__ == '__main__':
    print('Main!')
    
    # Ask for run
    
    run = input('Please enter path to file relative to "data/":    ')
    
    # Make the output directory if it doesn't exist
    
    if not os.path.exists(os.path.dirname(outdir + run)):
        try:
            os.makedirs(os.path.dirname(outdir + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    # List all files in folder
    
    files = os.listdir(indir + run)
    
    # All months and different winds
    
    wind_list = ['u_asc', 'u_rot', 'v_asc', 'v_rot']
    #wind_list = ['eastgw_asc', 'eastgw_rot', 'northgw_asc', 'northgw_rot']
    
    # Initialise list of cubes for 4 winds, each with 12 months
                
    wind_cube_list = [None for ii in range(4)]
    
    for fname in files:
        
        # Check if it's a netcdf year file otherwise continue to next item in loop
        # Year is for model data since mean is in this data and is removed
        # from composite day. Mean not removed from composite day in radar
        # data so we use the composite day instead
        
        if ((('MODEL' in run) and ('year.nc' not in fname)) or
            (('RADAR' in run) and ('day.nc' not in fname))):
            continue
        
        # Now proceed to importing each wind

        for ii, wind in enumerate(wind_list):
            
            if (wind in fname):            
                
                wind_cube_list[ii] = iris.load(indir + run + fname)[0]
                
                break
    
    # Given 4 cubes, take means and plot
    
    if 'MODEL' in run:
        titles = [r'$\bf{ExUM}$ monthly mean zonal wind at Ascension Island',
                  r'$\bf{ExUM}$ monthly mean zonal wind at Rothera',
                  r'$\bf{ExUM}$ monthly mean meridional wind at Ascension Island',
                  r'$\bf{ExUM}$ monthly mean meridional wind at Rothera']
        #titles = [r'$\bf{ExUM}$ monthly mean zonal GW tendency at Ascension Island',
        #          r'$\bf{ExUM}$ monthly mean zonal GW tendency at Rothera',
        #          r'$\bf{ExUM}$ monthly mean meridional GW tendency at Ascension Island',
        #          r'$\bf{ExUM}$ monthly mean meridional GW tendency at Rothera']
    
    if 'RADAR' in run:
        titles = [r'$\bf{Observed}$ monthly mean zonal wind at Ascension Island',
                  r'$\bf{Observed}$ monthly mean zonal wind at Rothera',
                  r'$\bf{Observed}$ monthly mean meridional wind at Ascension Island',
                  r'$\bf{Observed}$ monthly mean meridional wind at Rothera']
    
    for wind, wind_name, title in zip(wind_cube_list, wind_list, titles):
        
        # Compute monthly mean - taking into account nans in data
        
        monthly_mean = np.nanmean(wind.data, axis = 1)
        
        # Repeat December at front and January at end to give full first and last month (need to transpose for it to work)

        monthly_mean = np.column_stack((monthly_mean[-1,:],monthly_mean.T,monthly_mean[0,:]))
        monthly_mean = monthly_mean.T
        monthly_mean_filt = gaussian_filter(monthly_mean, 0.0)
        
        # Get coords from cube
        
        months = wind.coord('month').points[:]
        months = np.append(0, months) # append fictitious month for looping
        months = np.append(months, 13) # append fictitious month for looping
        heights = wind.coord('level_height').points[:]
        
        # Make into meshgrid
        
        X, Y = np.meshgrid(months,heights)
        X = X.T
        Y = Y.T
        
        # Calculate top value on colorbar
        
        top = np.amax((np.abs(np.nanmin(monthly_mean)), np.abs(np.nanmax(monthly_mean))))
        
        # print(wind_name, '   ', top)
        
        # top = 41.0 # Hard code top if fixing the colourbar limits
        
        # Do contour plot
        
        fig, ax = plt.subplots(figsize=[12,6])
        cs = ax.contourf(X,Y,monthly_mean, 31, cmap = plt.cm.get_cmap('RdBu_r', 31),vmax=top,vmin=-top)
        ax.contour(X,Y,monthly_mean_filt,[0.],colors='k',linestyles='dashed')
        ax.set_title(title + ' ' + str(np.int(run[6:10])+1), fontsize=16)
        ax.set_xlabel('Month', fontsize=26)
        ax.set_ylabel('Height / km', fontsize=26)

        ax.set_xticks(np.arange(0.5,13.5))
        ax.xaxis.set_major_formatter(plt.NullFormatter())
        ax.set_xlim(0.5,12.5)
        ax.set_ylim(79000.,101000.)
        ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        ax.tick_params(axis='y', labelsize=24)
        ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
        ax.xaxis.set_minor_formatter(IndexFormatter(['','J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']))
        ax.tick_params(which='minor', color='white', labelsize=24)

        cb = add_colorbar(cs,monthly_mean)
        
        cb.set_label(r'Mean wind speed / ms$^{-1}$', size=24)
        #cb.set_label(r'Wind tendency / ms$^{-1}$d$^{-1}$', size=24)
        cb.ax.tick_params(labelsize=20)
        
        # Save figure
        
        plt.savefig(outdir + run + wind_name + '_monthly_mean.png', bbox_inches = 'tight', pad_inches = 0.2)
