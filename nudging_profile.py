import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import iris
from matplotlib.ticker import FuncFormatter, AutoMinorLocator, MultipleLocator, IndexFormatter

# Import CIRA temerature cube: shape [12,33,71], [month x lat x height]

t_cira_cube = iris.load('data/t_cira.nc')[0]

# Extract temperature and coordinates, and convert heights to km

t_cira = t_cira_cube.data

months = t_cira_cube.coord('time').points[:]

z = t_cira_cube.coord('height').points[:]

lats = t_cira_cube.coord('latitude').points[:]

# z = z_m/1000.

# Plot directory

plot_dir = '/Users/mjg41/Documents/PhD/Plots/Model/nudging_3d_profile/'

###############    Create nudging temperature profile    ###############

# Initialise variables

low_lr = np.zeros((12,33)) # lapse rate from 70km to mesopause from CIRA
high_lr = np.zeros((12,33)) # lapse rate from mesopause to 120km from CIRA
t_min = np.zeros((12,33)) # temp at mesopause from CIRA
z_min = np.zeros((12,33)) # height of mesopause from CIRA
t_min_nudge = np.zeros((12,33)) # temp at mesopause analytic nudging profile
z_min_nudge = np.zeros((12,33)) # height at mesopause analytic nudging profile

# Open text file to print diagnostics to

out_diag = open("nudge_analysis.txt", "w")

# Switch for diagnostic printing for each month

tests = False

# For each month

for ii in np.arange(12):
    
    # For each lat
    
    for jj, lat in enumerate(lats):
        
        # Find min above 70 km and corresponding index (argmin takes
        # first occurence where multiple minimal values exist)
        
        t_min_cira = min(t_cira[ii,jj,-31:])
        t_min_cira_ind = -31 + np.argmin(t_cira[ii,jj,-31:])
        
        # Find lapse rate from 70 km to minimum (low_lr) and lapse rate
        # from cira top (119.7 km) to minimum (high_lr)
        
        low_lr[ii,jj]= ((t_cira[ii,jj,t_min_cira_ind]- t_cira[ii,jj,-31])/
                                           (z[t_min_cira_ind] - z[-31]))
        
        high_lr[ii,jj]= ((t_cira[ii,jj,-1] - t_cira[ii,jj,t_min_cira_ind])/
                                            (z[-1] - z[t_min_cira_ind]))
        
        # Create t_min field for month ii, lat jj based on minimal
        # temperature value. Record height at which minimum occurs in
        # z_min
        
        t_min[ii,jj] = t_min_cira
        
        z_min[ii,jj] = z[t_min_cira_ind]
        
        # Create analytical temperature nudging field at minimal 
        # temperature value. Similarly, create analytical height field
        # at minimal temperature value
        
        t_min_nudge[ii,jj] = 190. + (25. * np.cos(abs((ii+1) - 6) * np.pi/6.) * 
                                       np.cos((90. + lat) * np.pi/180.))
                                    
        z_min_nudge[ii,jj] = 90000. + (5000. * np.cos(abs((ii+1) - 6) * np.pi/6) * 
                                       np.cos((90. + lat) * np.pi/180))
        
        # Check nudging profile against CIRA
        
        if tests:
            
            print('Month', ii, 'cos term = ', np.cos(abs((ii+1) - 6) * np.pi/6), file=out_diag)
            print('Month', ii, 'lower lapse rate mean and stdev = ', np.mean(low_lr[ii,:]), np.std(low_lr[ii,:]), file=out_diag)
            print('Month', ii, 'upper lapse rate mean and stdev = ', np.mean(high_lr[ii,:]), np.std(high_lr[ii,:]), '\n', file=out_diag)
            
            print('Month', ii, 't_min mean and stdev = ', np.mean(t_min[ii,:]), np.std(t_min[ii,:]), file=out_diag)
            print('Month', ii, 't_min_nudge mean and stdev = ', np.mean(t_min_nudge[ii,:]), np.std(t_min_nudge[ii,:]), file=out_diag)
            print('Month', ii, 'temp diff mean and stdev = ', np.mean(t_min_nudge[ii,:] - t_min[ii,:]), np.std(t_min_nudge[ii,:] - t_min[ii,:]), '\n', file=out_diag)
            
            print('Month', ii, 'z_min mean and stdev = ', np.mean(z_min[ii,:]), np.std(z_min[ii,:]), file=out_diag)
            print('Month', ii, 'z_min_nudge mean and stdev = ', np.mean(z_min_nudge[ii,:]), np.std(z_min_nudge[ii,:]), file=out_diag)
            print('Month', ii, 'z diff mean and stdev = ', np.mean(z_min_nudge[ii,:] - z_min[ii,:]), np.std(z_min_nudge[ii,:] - z_min[ii,:]), file=out_diag)

# Try fitting parameters

# 2D domain

X,Y = np.meshgrid(np.arange(1, 13), lats)
X = X.T
Y = Y.T

# Stack meshgrids ready for fit

xdata = np.vstack((X.ravel(), Y.ravel()))

# Function to fit; seasonal and latitudinal fit to temperature

def time_lat_temp_fit(x, y, a, b):
    return a + (b * np.cos(abs(x - 6) * np.pi/6.) * 
                                       np.cos(np.pi/2. + (y*np.pi/180)))
            
# Callable for fit 

def _time_lat_temp_fit(M, a, b):
    x,y = M
    return time_lat_temp_fit(x, y, a, b)
    
# Function to fit; seasonal and latitudinal fit to height

def time_lat_z_fit(x, y, a, b):
    return a + (b * np.cos(abs(x - 6) * np.pi/6) * 
                                       np.cos(np.pi/2. + (y*np.pi/180)))
                                       
# Callable for fit 

def _time_lat_z_fit(M, a, b):
    x,y = M
    return time_lat_z_fit(x, y, a, b)

# Do fit
#import pdb; pdb.set_trace()
params_temp, _ = curve_fit(_time_lat_temp_fit, xdata, t_min.ravel())

params_z, _ = curve_fit(_time_lat_z_fit, xdata, z_min.ravel())

# Create temp and height profile based on fit

t_min_nudge_fitted = time_lat_temp_fit(X,Y,params_temp[0], params_temp[1])

z_min_nudge_fitted = time_lat_z_fit(X,Y,params_z[0], params_z[1])

# Check overall mesopause nudging profile against CIRA - use 3dp
# formatting throughout

print('Lapse rates\n', file=out_diag)

print(f'Year lower lapse rate mean and stdev = {np.mean(low_lr):.3f}, {np.std(low_lr):.3f}', file=out_diag)
print(f'Year upper lapse rate mean and stdev = {np.mean(high_lr):.3f}, {np.std(high_lr):.3f}\n', file=out_diag)

print('Temperature at mesopause\n', file=out_diag)

print(f'Year t_min mean and stdev = {np.mean(t_min):.3f}, {np.std(t_min):.3f}', file=out_diag)
print(f'Year t_min_nudge mean and stdev = {np.mean(t_min_nudge):.3f}, {np.std(t_min_nudge):.3f}', file=out_diag)
print(f'Year t_min_nudge_fitted mean and stdev = {np.mean(t_min_nudge_fitted):.3f}, {np.std(t_min_nudge_fitted):.3f}', file=out_diag)
print(f'Year temp diff with t_min_nudge mean and stdev = {np.mean(t_min_nudge - t_min):.3f}, {np.std(t_min_nudge - t_min):.3f}', file=out_diag)
print(f'Year temp diff with t_min_nudge_fitted mean and stdev = {np.mean(t_min_nudge_fitted - t_min):.3f}, {np.std(t_min_nudge_fitted - t_min):.3f}\n', file=out_diag)

print('Height at mesopause\n', file=out_diag)

print(f'Year z_min mean and stdev = {np.mean(z_min):.3f}, {np.std(z_min):.3f}', file=out_diag)
print(f'Year z_min_nudge mean and stdev = {np.mean(z_min_nudge):.3f}, {np.std(z_min_nudge):.3f}', file=out_diag)
print(f'Year z_min_nudge_fitted mean and stdev = {np.mean(z_min_nudge_fitted):.3f}, {np.std(z_min_nudge_fitted):.3f}', file=out_diag)
print(f'Year z diff with t_min_nudge mean and stdev = {np.mean(z_min_nudge - z_min):.3f}, {np.std(z_min_nudge - z_min):.3f}', file=out_diag)
print(f'Year z diff with t_min_nudge_fitted mean and stdev = {np.mean(z_min_nudge_fitted - z_min):.3f}, {np.std(z_min_nudge_fitted - z_min):.3f}\n\n', file=out_diag)

# Check fit with plots

month_labels = ['January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November',
                'December']
          
lat_labels = ['80S','60S','40S','20S','EQ','20N','40N','60N','80N']

# Check latitudinal variation of t_min

fig, ax = plt.subplots(3, 4, sharex=True, sharey=True, figsize=[12,8])

fig.suptitle('Latitudinal variation of t_min for each month.', fontsize=16)

fig.text(0.5, 0.03, 'Latitude / deg', ha='center', fontsize=16)
fig.text(0.05, 0.5, 'Temperature / K', va='center', rotation='vertical', fontsize=16)

for ii, [axis, month_label] in enumerate(zip(ax.ravel(), month_labels)):
    
    axis.plot(lats,t_min_nudge_fitted[ii,:], label = 'NUDGE')
    
    axis.plot(lats,t_min[ii,:], label = 'CIRA')
    
    axis.set_ylim(135,215)
    
    axis.set_title(month_label, fontsize=16)
    
    axis.set_xlim(-90.,90.)
    axis.set_xticks(np.arange(-90.,91.,45.))
    axis.xaxis.set_minor_locator(AutoMinorLocator(3))
    
    axis.tick_params(axis='x', labelsize=14)
    axis.tick_params(axis='y', labelsize=14)

handles, labels = axis.get_legend_handles_labels()
fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49,0.0), framealpha=1.0)

fig.savefig(plot_dir + 'lat_var_t_min.png', bbox_inches = 'tight', pad_inches = 0.35)

# Check latitudinal variation of z_min

fig, ax = plt.subplots(3, 4, sharex=True, sharey=True, figsize=[12,8])

fig.suptitle('Latitudinal variation of z_min for each month.', fontsize=16)

fig.text(0.5, 0.03, 'Latitude / deg', ha='center', fontsize=16)
fig.text(0.05, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)

for ii, [axis, month_label] in enumerate(zip(ax.ravel(), month_labels)):
    
    axis.plot(lats,z_min_nudge_fitted[ii,:], label = 'NUDGE')
    
    axis.plot(lats,z_min[ii,:], label = 'CIRA')
    
    axis.set_ylim(88000,100000)
    axis.set_yticks(np.arange(88000., 100001., 4000.))
    axis.yaxis.set_minor_locator(AutoMinorLocator(2))

    axis.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
    
    axis.set_title(month_label, fontsize=16)
    
    axis.set_xlim(-90.,90.)
    axis.set_xticks(np.arange(-90.,91.,45.))
    axis.xaxis.set_minor_locator(AutoMinorLocator(3))
    
    axis.tick_params(axis='x', labelsize=14)
    axis.tick_params(axis='y', labelsize=14)    

handles, labels = axis.get_legend_handles_labels()
fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49,0.0), framealpha=1.0)

fig.savefig(plot_dir + 'lat_var_z_min.png', bbox_inches = 'tight', pad_inches = 0.35)

# Check monthly variation of t_min

fig, ax = plt.subplots(3, 3, sharex=True, sharey=True, figsize=[12,8])

fig.suptitle('Monthly variation of t_min for several latitudes.', fontsize=16)

fig.text(0.5, 0.03, 'Month', ha='center', fontsize=16)
fig.text(0.05, 0.5, 'Temperature / K', va='center', rotation='vertical', fontsize=16)

for ii, [axis, lat_label] in enumerate(zip(ax.ravel(), lat_labels)):
    
    axis.plot(months, t_min_nudge_fitted[:,4*ii], label = 'NUDGE')
    
    axis.plot(months, t_min[:,4*ii], label = 'CIRA')
    
    #axis.set_xlim(1,12)
    #axis.set_xticks([1,3,6,9,12]) 
    
    axis.set_xticks(np.arange(0.5,13.5))
    axis.xaxis.set_major_formatter(plt.NullFormatter())
    axis.set_xlim(0.5,12.5)
    axis.xaxis.set_minor_locator(plt.MultipleLocator(1))
    axis.xaxis.set_minor_formatter(IndexFormatter(['','J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']))
    axis.tick_params(which='minor', color='white', labelsize=14)
    
    axis.set_ylim(135,215)
    
    axis.set_title(lat_label, fontsize=16)
    
    axis.tick_params(axis='x', labelsize=14)
    axis.tick_params(axis='y', labelsize=14) 

handles, labels = axis.get_legend_handles_labels()
fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49, 0.0), framealpha=1.0)
    
fig.savefig(plot_dir + 'mon_var_t_min.png', bbox_inches = 'tight', pad_inches = 0.35)


# Check monthly variation of z_min

fig, ax = plt.subplots(3, 3, sharex=True, sharey=True, figsize=[12,8])

fig.suptitle('Monthly variation of z_min for several latitudes.', fontsize=16)

fig.text(0.5, 0.03, 'Month', ha='center', fontsize=16)
fig.text(0.05, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)

for ii, [axis, lat_label] in enumerate(zip(ax.ravel(), lat_labels)):
    
    axis.plot(months, z_min_nudge_fitted[:,4*ii], label = 'NUDGE')
    
    axis.plot(months, z_min[:,4*ii], label = 'CIRA')

    axis.set_xticks(np.arange(0.5,13.5))
    axis.xaxis.set_major_formatter(plt.NullFormatter())
    axis.set_xlim(0.5,12.5)
    axis.xaxis.set_minor_locator(plt.MultipleLocator(1))
    axis.xaxis.set_minor_formatter(IndexFormatter(['','J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']))
    axis.tick_params(which='minor', color='white', labelsize=14)
    
    axis.set_ylim(88000,100000)
    axis.set_yticks(np.arange(88000., 100001., 4000.))
    axis.yaxis.set_minor_locator(AutoMinorLocator(2))
    
    axis.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))

    axis.set_title(lat_label, fontsize=16)
    
    axis.tick_params(axis='x', labelsize=14)
    axis.tick_params(axis='y', labelsize=14) 

handles, labels = axis.get_legend_handles_labels()
fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49, 0.0), framealpha=1.0)

fig.savefig(plot_dir + 'mon_var_z_min.png', bbox_inches = 'tight', pad_inches = 0.35)

# Take time and lat mean of lapse rates and use to calculate temperature
# nudging profiles at 70 and 120 km

low_lr_mean = np.mean(low_lr)

high_lr_mean = np.mean(high_lr)

# Initialise variables

t_70km_nudge = np.zeros((12,33)) # temp at 70 km analytic nudging profile

t_120km_nudge = np.zeros((12,33)) # temp at 120 km analytic nudging profile

t_70km_nudge_fitted = np.zeros((12,33)) # temp at 70 km analytic nudging profile

t_120km_nudge_fitted = np.zeros((12,33)) # temp at 120 km analytic nudging profile

# For each month

for ii in np.arange(12):
    
    # For each lat
    
    for jj, lat in enumerate(lats):
        
        t_70km_nudge[ii,jj] = t_min_nudge[ii,jj] + (-low_lr_mean * 
                                      (z_min_nudge[ii,jj] - z[-31] - 5000.0))
        
        t_120km_nudge[ii,jj] = t_min_nudge[ii,jj] + (high_lr_mean *
                                       (z[-1] - z_min_nudge[ii,jj] - 5000.0))

        t_70km_nudge_fitted[ii,jj] = t_min_nudge_fitted[ii,jj] - (low_lr_mean * 
                                      (z_min_nudge_fitted[ii,jj] - z[-31]))
        
        t_120km_nudge_fitted[ii,jj] = t_min_nudge_fitted[ii,jj] + (high_lr_mean *
                                       (z[-1] - z_min_nudge_fitted[ii,jj]))

# Check overall 70 and 120 km nudging profiles against CIRA

print('Temperature at 70 km\n', file=out_diag)

print(f'Year t_70km mean and stdev = {np.mean(t_cira[:,:,-31]):.3f}, {np.std(t_cira[:,:,-31]):.3f}', file=out_diag)
print(f'Year t_70km_nudge mean and stdev = {np.mean(t_70km_nudge):.3f}, {np.std(t_70km_nudge):.3f}', file=out_diag)
print(f'Year t_70km_nudge_fitted mean and stdev = {np.mean(t_70km_nudge_fitted):.3f}, {np.std(t_70km_nudge_fitted):.3f}', file=out_diag)
print(f'Year temp diff with t_70km_nudge mean and stdev = {np.mean(t_70km_nudge - t_cira[:,:,-31]):.3f}, {np.std(t_70km_nudge - t_cira[:,:,-31]):.3f}', file=out_diag)
print(f'Year temp diff with t_70km_nudge_fitted mean and stdev = {np.mean(t_70km_nudge_fitted - t_cira[:,:,-31]):.3f}, {np.std(t_70km_nudge_fitted - t_cira[:,:,-31]):.3f}\n', file=out_diag)

print('Temperature at 120 km\n', file=out_diag)

print(f'Year t_120km mean and stdev = {np.mean(t_cira[:,:,-1]):.3f}, {np.std(t_cira[:,:,-1]):.3f}', file=out_diag)
print(f'Year t_120km_nudge mean and stdev = {np.mean(t_120km_nudge):.3f}, {np.std(t_120km_nudge):.3f}', file=out_diag)
print(f'Year t_120km_nudge_fitted mean and stdev = {np.mean(t_120km_nudge_fitted):.3f}, {np.std(t_120km_nudge_fitted):.3f}', file=out_diag)
print(f'Year temp diff with t_120km_nudge mean and stdev = {np.mean(t_120km_nudge - t_cira[:,:,-1]):.3f}, {np.std(t_120km_nudge - t_cira[:,:,-1]):.3f}', file=out_diag)
print(f'Year temp diff with t_120km_nudge_fitted mean and stdev = {np.mean(t_120km_nudge_fitted - t_cira[:,:,-1]):.3f}, {np.std(t_120km_nudge_fitted - t_cira[:,:,-1]):.3f}', file=out_diag)

# Now create full 3D profile from 70 to 120 km

# Just want 70 to 120 km

z_nudge = z[-31:]

#~ z_nudge = np.array([70764.32,  73573.60,
                   #~ 76382.89,  79192.17,  82001.45,  84822.13,  87686.62,
                   #~ 90586.83,  93474.60,  96326.35,  99175.77, 102112.84,
                  #~ 105276.25, 108838.27, 112970.75, 117804.39])

t_nudge = np.zeros((12,33,31)) # 31 CIRA heights from ~70 km to ~120 km
#~ t_nudge = np.zeros((12,33,16))


#~ # Above mesopause, fit to monthly mean zonal mean is power law a*(z-z_min)**b, a=0.0706, b=2.4146

#~ # Interested in data above 70 km so extract that

#~ z_top = z[-31:]
#~ t_cira_top = t_cira[:,:,-31:]

#~ # Calculate monthly mean zonal mean of cira to get value for each height

#~ cira_mean_heights = np.mean(np.mean(t_cira_top,axis=0),axis=0)

#~ # Calculate index of mesopause minimum 

#~ min_ind = np.argmin(cira_mean_heights)

#~ # Power law function to fit and perform fit

#~ def height_temp_power_fit(x,a,b):
    #~ return a*(x-z_top[min_ind])**b + cira_mean_heights[min_ind]

#~ params,_ = curve_fit(height_temp_power_fit,z_top[min_ind:],cira_mean_heights[min_ind:])

#~ # Check with plot

#~ temp_fit = height_temp_power_fit(z_top[15:],params[0],params[1])

#~ plt.plot(temp_fit, z_top[min_ind:], label='NUDGE')
#~ plt.plot(cira_mean_heights[min_ind:], z_top[min_ind:], label='CIRA')
#~ plt.legend()
#~ plt.show()

# For each month

for ii in np.arange(12):
    
    # For each lat
    
    for jj, lat in enumerate(lats):
        
        # For each height
        
        for kk in np.arange(31):
        #~ for kk in np.arange(16):
            
            # If below (or at) mesopause use lower lapse rate
            
            if z_nudge[kk] <= z_min_nudge_fitted[ii,jj]:
                
                t_nudge[ii,jj,kk] = t_min_nudge_fitted[ii,jj] - (low_lr_mean * 
                                      (z_min_nudge_fitted[ii,jj] - z_nudge[kk]))
            
            # If above mesopause use upper lapse rate
                
            elif z_nudge[kk] >= z_min_nudge_fitted[ii,jj]:
                
                #t_nudge[ii,jj,kk] = t_min_nudge_fitted[ii,jj] + (high_lr_mean *
                                       #(z_nudge[kk] - z_min_nudge_fitted[ii,jj]))
                
                t_nudge[ii,jj,kk] = t_min_nudge_fitted[ii,jj] + (4.02564306e-09 *
                                       (z_nudge[kk] - z_min_nudge_fitted[ii,jj])**2.4146)

# Check 3D profile with some plots

# Check variation with some means

nudge_mean_months = np.mean(np.mean(t_nudge[:,:,-16:], axis = -1), axis = -1)
nudge_mean_lats = np.mean(np.mean(t_nudge[:,:,-16:], axis = 0), axis = -1)
nudge_mean_heights = np.mean(np.mean(t_nudge[:,:,-16:], axis = 0), axis = 0)

cira_mean_months = np.mean(np.mean(t_cira[:,:,-16:], axis = -1), axis = -1)
cira_mean_lats = np.mean(np.mean(t_cira[:,:,-16:], axis = 0), axis = -1)
cira_mean_heights = np.mean(np.mean(t_cira[:,:,-16:], axis = 0), axis = 0)

#~ # Time variation

#~ fig,ax = plt.subplots()
#~ ax.plot(months, nudge_mean_months, label='NUDGE')
#~ ax.plot(months, cira_mean_months, label='CIRA')
#~ ax.set_title('Zonal Mean Height Mean - Time variation')
#~ ax.set_xlabel('Month')
#~ ax.set_ylabel('Temperature / K')
#~ ax.legend()
#~ fig.savefig(plot_dir + 'mon_var_3d.png')

#~ # Lat variation

#~ fig,ax = plt.subplots()
#~ ax.plot(lats, nudge_mean_lats, label='NUDGE')
#~ ax.plot(lats, cira_mean_lats, label='CIRA')
#~ ax.set_title('Monthly Mean Height Mean - Lat variation')
#~ ax.set_xlabel('Latitude / deg')
#~ ax.set_ylabel('Temperature / K')
#~ ax.legend()
#~ fig.savefig(plot_dir + 'lat_var_3d.png')

#~ # Height variation

fig,ax = plt.subplots()
ax.plot(nudge_mean_heights, z_nudge[-16:], label='NUDGE')
ax.plot(cira_mean_heights, z_nudge[-16:], label='CIRA')
ax.set_title('Monthly Mean Zonal Mean - Height variation', fontsize=16)
ax.set_ylabel('Height / km', fontsize=16)
ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
ax.set_xticks(np.arange(175.,376.,25.))
ax.set_xlabel('Temperature / K', fontsize=16)
ax.tick_params(axis='x', labelsize=14)
ax.tick_params(axis='y', labelsize=14)
ax.legend(fontsize='large')
fig.savefig(plot_dir + 'height_var_3d.png', bbox_inches = 'tight', pad_inches = 0.1)

# Output to netcdf

# Construct cube containing temperature profile

time_coord = iris.coords.DimCoord(months, standard_name = 'time', units = 'month')
    
lat_coord = iris.coords.DimCoord(lats, standard_name = 'latitude', units = 'degrees')

height_coord = iris.coords.DimCoord(z_nudge, standard_name = 'height', units = 'm')

t_nudge_cube = iris.cube.Cube(t_nudge, standard_name = 'air_temperature' , units = 'K' , dim_coords_and_dims = [(time_coord, 0), (lat_coord, 1), (height_coord, 2)])

# Save cube

iris.save(t_nudge_cube, 'data/t_nudge_lat_time_dep.nc')

# Close diagnostic text file

out_diag.close()

# Compare temp_min from online calculations for June

lats_online = np.arange(-89.375, 89.376, 1.25)

t_min_online = np.array([204.17697917521042,  204.16473366978903,  204.14024848714359,  204.10353528089496,  204.05461152454092,  203.99350050313978,  203.92023130222799,  203.8348387939769,  203.73736362059563,  203.62785217498745,  203.5063565786694,  203.37293465696507, 203.22764991148313,  203.07057148989384,  202.90177415301849,  202.72133823924747,  202.52934962630329,  202.32589969036769,  202.11108526259136,  201.88500858300759,  201.64777725187162,  201.39950417844861,  201.14030752727513,  200.87031066191909, 200.58964208626543,  200.29843538335516,  199.99682915180719,  199.68496693985281,  199.36299717701451,  199.03107310346152,  198.68935269707598,  198.33799859826377,  197.97717803254668,  197.60706273097199,  197.22782884837787,  196.83965687955319, 196.44273157333183,  196.03724184466211,  195.62338068469364,  195.20134506892393,  194.77133586344871,  194.33355772936054,  193.8882190253413,  193.43553170849458,  192.97571123346583,  192.50897644989729,  192.0355494982677,  191.5556557041653, 191.06952347104513,  190.57738417152135,  190.07947203724632,  189.57602404742914,  189.06727981604632,  188.5534814777985,  188.03487357286744,  187.5117029305282,  186.98421855167172,  186.45267149029382,  185.91731473400728,  185.37840308363323, 184.83619303192995,  184.29094264151598,  183.74291142204655,  183.19236020670081,  182.63955102803953,  182.08474699329173,  181.52821215912985,  180.97021140599313,  180.41101031201879,  179.85087502664143,  179.29007214391999,  178.72886857565368, 178.16753142434635,  177.60632785608004,  177.0455249733586,  176.48538968798124,  175.92618859400693,  175.36818784087018,  174.8116530067083,  174.25684897196049,  173.70403979329922,  173.15348857795351,  172.60545735848405,  172.0602069680701, 171.5179969163668,  170.97908526599275,  170.4437285097062,  169.91218144832831,  169.38469706947183,  168.86152642713259,  168.34291852220153,  167.82912018395371,  167.32037595257088,  166.81692796275371,  166.31901582847868,  165.8268765289549, 165.34074429583472,  164.86085050173233,  164.38742355010277,  163.92068876653423,  163.46086829150545,  163.00818097465876,  162.56284227063949,  162.12506413655132,  161.6950549310761,  161.27301931530639,  160.85915815533792,  160.4536684266682, 160.05674312044684,  159.66857115162216,  159.28933726902804,  158.91922196745335,  158.55840140173626,  158.20704730292405,  157.86532689653851,  157.53340282298552,  157.21143306014721,  156.89957084819284,  156.59796461664487,  156.3067579137346, 156.02608933808094,  155.7560924727249,  155.49689582155142,  155.24862274812841,  155.01139141699244,  154.78531473740867,  154.57050030963234,  154.36705037369674,  154.17506176075256,  153.99462584698153,  153.82582851010619,  153.6687500885169, 153.52346534303496,  153.39004342133063,  153.26854782501258,  153.1590363794044,  153.06156120602313,  152.97616869777204,  152.90289949686024,  152.84178847545911,  152.79286471910507,  152.75615151285643,  152.731666330211,  152.71942082478961])

z_min_online = np.array([98626.872629383855,  98624.701858193177,  98620.361348981329,  98613.853167595633,  98605.180411577938,  98594.347208690378,  98581.358714950838,  98566.221112178842,  98548.941605053493,  98529.528417684312,  98507.990789697069,  98484.33897183623, 98458.584221086101,  98430.738795313169,  98400.815947432013,  98368.829919097581,  98334.795933927031,  98298.730190254006,  98260.649853419207,  98220.573047600512,  98178.518847186977,  98134.507267700305,  98088.559256268709,  98040.696681657078, 97990.942323858704,  97939.319863253215,  97885.853869335973,  97830.569789024405,  97773.493934546554,  97714.653470917925,  97654.076403012426,  97591.791562233528,  97527.828592792124,  97462.217937597423,  97394.990823767846,  97326.179247768567, 97255.815960182939,  97183.934450124987,  97110.568929300425,  97035.75431572368,  96959.526217098886,  96881.920913872484,  96802.975341965706,  96722.727075195115,  96641.214307389469,  96558.475834211509,  96474.551034693388,  96389.479852494303, 96303.302776889555,  96216.060823499793,  96127.795514769823,  96038.548860206167,  95948.363336382827,  95857.28186672469,  95765.347801078358,  95672.60489507996,  95579.097289329817,  95484.869488383891,  95389.966339572085,  95294.433011653295, 95198.314973317538,  95101.657971545326,  95004.508009834593,  94906.911326305461,  94808.914371693492,  94710.563787241583,  94611.906382501285,  94512.989113053962,  94413.859058162561,  94314.563398364335,  94215.149393015512,  94115.664357798378, 94016.155642201629,  93916.67060698451,  93817.256601635672,  93717.960941837446,  93618.830886946045,  93519.913617498722,  93421.256212758424,  93322.905628306515,  93224.908673694546,  93127.311990165414,  93030.162028454681,  92933.505026682469, 92837.386988346712,  92741.853660427922,  92646.950511616116,  92552.72271067019,  92459.215104920047,  92366.472198921649,  92274.538133275317,  92183.45666361718,  92093.27113979384,  92004.024485230184,  91915.759176500214,  91828.517223110452, 91742.340147505703,  91657.268965306619,  91573.344165788498,  91490.605692610538,  91409.092924804892,  91328.844658034301,  91249.899086127538,  91172.293782901121,  91096.065684276327,  91021.251070699582,  90947.88554987502,  90876.004039817068, 90805.64075223144,  90736.829176232161,  90669.602062402584,  90603.991407207883,  90540.028437766479,  90477.743596987581,  90417.166529082082,  90358.326065453453,  90301.250210975602,  90245.966130664034,  90192.500136746807,  90140.877676141303, 90091.123318342929,  90043.260743731298,  89997.312732299702,  89953.301152813045,  89911.246952399495,  89871.1701465808,  89833.089809746001,  89797.024066072976,  89762.990080902426,  89731.004052567994,  89701.081204686838,  89673.235778913906, 89647.481028163777,  89623.829210302938,  89602.291582315695,  89582.878394946514,  89565.598887821165,  89550.461285049169,  89537.472791309629,  89526.639588422069,  89517.966832404374,  89511.458651018678,  89507.11814180683,  89504.947370616152])

fig, axes = plt.subplots(1,2,figsize=[15,10])

axes[0].plot(lats_online, t_min_online, label='ONLINE')
axes[0].plot(lats, t_min_nudge_fitted[5,:], label='OFFLINE')
axes[0].set_title('T_min comparison online v offline')
axes[0].set_xlabel('Latitude / deg')
axes[0].set_xlim(-90,90)
axes[0].set_xticks(np.arange(-90.,91.,30.))
axes[0].set_ylabel('Temperature / K')
axes[0].legend()

axes[1].plot(lats_online, z_min_online, label='ONLINE')
axes[1].plot(lats, z_min_nudge_fitted[5,:], label='OFFLINE')
axes[1].set_title('z_min comparison online v offline')
axes[1].set_xlabel('Latitude / deg')
axes[1].set_xlim(-90,90)
axes[1].set_xticks(np.arange(-90.,91.,30.))
axes[1].set_ylabel('Height / km')
axes[1].yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
axes[1].legend()

fig.savefig(plot_dir + 'offline_online_june_t_min_and_z_min.png')
