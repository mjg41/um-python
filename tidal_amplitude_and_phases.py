def best_fit_slope_and_intercept(xs,ys):
    
    from numpy import mean
    
    m = (((mean(xs)*mean(ys)) - mean(xs*ys)) /
         ((mean(xs)*mean(xs)) - mean(xs*xs)))
    
    b = mean(ys) - m*mean(xs)
    
    return m, b

def amplitude_and_phase_24h(time, wind):
    '''Takes in winds (1D or 2D) and fits a sinusoidal wave with period
    24h returning the amplitude and standard deviation.
    
    Positional arguments
    
    time is the N-array of time coordinates
    wind is the N-array or (N,M)-array of wind values
    
    Output
    
    amps is the N-array or (N,M)-array of fitted amplitudes
    '''

    import numpy as np
    import iris
    from scipy.optimize import curve_fit
    
    # Get size of wind and initialise amps
    
    if len(wind.shape) == 1:
        N = wind.shape
        M = 1
        amps = 0.0
        errs = 0.0
        phases = 0.0
        
    elif len(wind.shape) == 2:
        N,M = wind.shape
        amps = np.zeros(M)
        errs = np.zeros(M)
        phases = np.zeros(M)
    
    # Remove background winds - just fitting sinusoidal oscillation
    
    wind -= np.nanmean(wind, axis=0)
    
    # Make function to fit diurnal mode
    
    def diurnal_fit(x, a, b):
        return a * np.cos(np.pi*x/12) + b * np.sin(np.pi*x/12)
    
    # Make function to fit diurnal mode and easily extract covar
    
    def diurnal_fit_for_covar(x, a, b):
        return a*np.sin(np.pi*x/12 + b)
    
    # Do fitting
    
    if M == 1:
        valid = ~(np.isnan(time) | np.isnan(wind))
        params,_ = curve_fit(diurnal_fit, time[valid], wind[valid])
        
        # We use a fit that gives us the amplitude as a single value
        # to find the covariance
        
        _,covar = curve_fit(diurnal_fit_for_covar, time[valid], wind[valid])
        amps = np.sqrt(params[0]**2 + params[1]**2)
        phases = np.arctan2(params[1], params[0])
        errs = np.sqrt(np.diag(covar)[0])
        
    else:
        for ii, column in enumerate(wind.T):
            valid = ~(np.isnan(time) | np.isnan(column))
            params,_ = curve_fit(diurnal_fit, time[valid], column[valid])
            
            # We use a fit that gives us the amplitude as a single value
            # to find the covariance
            
            _,covar = curve_fit(diurnal_fit_for_covar, time[valid], column[valid])
            amps[ii] = np.sqrt(params[0]**2 + params[1]**2)
            phases[ii] = np.arctan2(params[1], params[0])
            errs[ii] = np.sqrt(np.diag(covar)[0])
    
    # Convert phases to hours
    
    phases_h = phases * 12 / np.pi
    
    return amps, errs, phases_h

def amplitude_and_phase_12h(time, wind):
    '''Takes in winds (1D or 2D) and fits a sinusoidal wave with period
    12h returning the amplitude and standard deviation.
    
    Positional arguments
    
    time is the N-array of time coordinates
    wind is the N-array or (N,M)-array of wind values
    
    Output
    
    amps is the N-array or (N,M)-array of fitted amplitudes
    '''

    import numpy as np
    import iris
    from scipy.optimize import curve_fit
    
    # Get size of wind and initialise amps
    
    if len(wind.shape) == 1:
        N = wind.shape
        M = 1
        amps = 0.0
        errs = 0.0
        phases = 0.0
        
    elif len(wind.shape) == 2:
        N,M = wind.shape
        amps = np.zeros(M)
        errs = np.zeros(M)
        phases = np.zeros(M)
    
    # Remove background winds - just fitting sinusoidal oscillation
    
    wind -= np.nanmean(wind, axis=0)
    
    # Make function to fit semi-diurnal mode
    
    def semi_diurnal_fit(x, a, b):
        return a * np.cos(np.pi*x/6) + b * np.sin(np.pi*x/6)
    
    # Make function to fit semi-diurnal mode
    
    def semi_diurnal_fit_for_covar(x, a, b):
        return a*np.sin(np.pi*x/6 + b)
    
    # Do fitting
    
    if M == 1:
        valid = ~(np.isnan(time) | np.isnan(wind))
        params,_ = curve_fit(semi_diurnal_fit, time[valid], wind[valid])
        
        # We use a fit that gives us the amplitude as a single value
        # to find the covariance
            
        _,covar = curve_fit(semi_diurnal_fit_for_covar, time[valid], wind[valid])
        amps = np.sqrt(params[0]**2 + params[1]**2)
        phases = np.arctan2(params[1], params[0])
        errs = np.sqrt(np.diag(covar)[0])
        
    else:
        for ii, column in enumerate(wind.T):
            valid = ~(np.isnan(time) | np.isnan(column))
            params,_ = curve_fit(semi_diurnal_fit, time[valid], column[valid])
            
            # We use a fit that gives us the amplitude as a single value
            # to find the covariance
            
            _,covar = curve_fit(semi_diurnal_fit_for_covar, time[valid], column[valid])
            amps[ii] = np.sqrt(params[0]**2 + params[1]**2)
            phases[ii] = np.arctan2(params[1], params[0])
            errs[ii] = np.sqrt(np.diag(covar)[0])
    
    # Convert phases to hours
    
    phases_h = phases * 12 / np.pi
    
    return amps, errs, phases_h

def amp_plotter(run, figures_12h_in = None, figures_24h_in = None, axes_12h_in = None, axes_24h_in = None, saving=True, amps_year_plot = False, label = None):

    import os
    
    locale = os.getuid()
    xcsc = 11432
    mac = 501
    
    # Setup depending on locale
    
    if locale == xcsc:
        
        # Fix for crashing matplotlib on xcsc
        
        import matplotlib
        matplotlib.use('agg')
        
        # Setup directories
        
        indir = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
        outdir = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    elif locale == mac:
        
        # Setup directories
        
        indir = 'data/'
        outdir = '/Users/mjg41/Documents/PhD/Plots/Model/radar_comparison/'
    
    # Make the output directory if it doesn't exist
    
    if not os.path.exists(os.path.dirname(outdir + run)):
        try:
            os.makedirs(os.path.dirname(outdir + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    import iris
    import numpy as np
    import matplotlib.pyplot as plt
    from tidal_amplitude_and_phases import amplitude_and_phase_24h as a24
    from tidal_amplitude_and_phases import amplitude_and_phase_12h as a12
    import warnings
    import scipy.io as sio
    from matplotlib.ticker import IndexFormatter, FuncFormatter
    
    warnings.filterwarnings("ignore")
    
    # Import winds and combine
    
    u_asc = iris.load(indir + run + 'u_asc_comp_day.nc')[0]
    u_rot = iris.load(indir + run + 'u_rot_comp_day.nc')[0]
    v_asc = iris.load(indir + run + 'v_asc_comp_day.nc')[0]
    v_rot = iris.load(indir + run + 'v_rot_comp_day.nc')[0]
    
    winds = [u_asc.data, u_rot.data, v_asc.data, v_rot.data]
    
    # Import standard devs from data acquisition for radar
    
    if 'RADAR' in run:
        if '2005' in run:
            
            # Load in standard deviations for 2006
            
            mat_file_asc = sio.loadmat(indir + run + '2006AscStd.mat')
            
            mat_file_rot = sio.loadmat(indir + run + '2006RotStd.mat')
            
            # Extract specific variables and combine - different naming
            # convention for variable between Asc and Rot files. Rot
            # files also need to be transposed
            
            u_asc_std_12h = mat_file_asc['T12Ampustd']
            u_rot_std_12h = mat_file_rot['T12ustd'].T
            v_asc_std_12h = mat_file_asc['T12Ampvstd']
            v_rot_std_12h = mat_file_rot['T12vstd'].T
            
            stds_12h = [u_asc_std_12h, u_rot_std_12h, v_asc_std_12h, v_rot_std_12h]
                        
            u_asc_std_24h = mat_file_asc['T24Ampustd']
            u_rot_std_24h = mat_file_rot['T24ustd'].T
            v_asc_std_24h = mat_file_asc['T24Ampvstd']
            v_rot_std_24h = mat_file_rot['T24vstd'].T
            
            stds_24h = [u_asc_std_24h, u_rot_std_24h, v_asc_std_24h, v_rot_std_24h]
            
        elif '2009' in run:
            
            # Load in standard deviations for 2010
            
            mat_file_asc = sio.loadmat(indir + run + '2010AscStd.mat')
            
            mat_file_rot = sio.loadmat(indir + run + '2010RotStd.mat')
            
            # Extract specific variables and combine - different naming
            # convention for variable between Asc and Rot files. Rot
            # files also need to be transposed
            
            u_asc_std_12h = mat_file_asc['T12Ampustd']
            u_rot_std_12h = mat_file_rot['T12ustd'].T
            v_asc_std_12h = mat_file_asc['T12Ampvstd']
            v_rot_std_12h = mat_file_rot['T12vstd'].T
            
            stds_12h = [u_asc_std_12h, u_rot_std_12h, v_asc_std_12h, v_rot_std_12h]
                        
            u_asc_std_24h = mat_file_asc['T24Ampustd']
            u_rot_std_24h = mat_file_rot['T24ustd'].T
            v_asc_std_24h = mat_file_asc['T24Ampvstd']
            v_rot_std_24h = mat_file_rot['T24vstd'].T
            
            stds_24h = [u_asc_std_24h, u_rot_std_24h, v_asc_std_24h, v_rot_std_24h]
    
    
    # Get coords
    
    hours = u_asc.coord('hour').points[:]
    heights = u_asc.coord('level_height').points[:]
    
    # Set titles and initialise output figs and axes
    
    year = str(np.int(str(u_asc.coord('forecast_reference_time').cell(0)[0])[:4]) + 1)
    
    titles_12h = ['Zonal Wind 12hr Tide Amplitude from Composite Day at Ascension ' + year,
                  'Zonal Wind 12hr Tide Amplitude from Composite Day at Rothera ' + year,
                  'Meridional Wind 12hr Tide Amplitude from Composite Day at Ascension ' + year,
                  'Meridional Wind 12hr Tide Amplitude from Composite Day at Rothera ' + year]
    titles_24h = ['Zonal Wind 24hr Tide Amplitude from Composite Day at Ascension ' + year,
                  'Zonal Wind 24hr Tide Amplitude from Composite Day at Rothera ' + year,
                  'Meridional Wind 24hr Tide Amplitude from Composite Day at Ascension ' + year,
                  'Meridional Wind 24hr Tide Amplitude from Composite Day at Rothera ' + year]
    
    subtitles = ['January', 'February', 'March', 'April', 'May', 'June',
                 'July', 'August', 'September', 'October', 'November',
                 'December']
    
    figures_12h = [None, None, None, None]

    figures_24h = [None, None, None, None]
    
    axes_12h = [None, None, None, None]
    
    axes_24h = [None, None, None, None]
    
    # Label line in comparison plot with levels and lid height
    # Set up looping tuple
    
    if not 'RADAR' in run:
        
        # label = run.split('/')[1].split('_')[0] + '_' + run.split('/')[1].split('_')[1]
        if not label:
            label = r'$\bf{ExUM}$'
        
        loop_over_24h = zip(winds, titles_24h)
        loop_over_12h = zip(winds, titles_12h)
        
    elif 'RADAR' in run:
        
        if not label:
            label = r'$\bf{Observed}$'
        
        loop_over_24h = zip(winds, titles_24h, stds_24h)
        loop_over_12h = zip(winds, titles_12h, stds_12h)

    # Plot each wind for each month for 24hr tide and save
    
    for ii, loop_vars in enumerate(loop_over_24h):
        
        # Extract looping variables
        
        if not 'RADAR' in run:
            
            wind, title = loop_vars
        
        elif 'RADAR' in run:
            
            wind, title, std_24h = loop_vars
        
        # If pre-existing figure and axes, use them
        
        if figures_24h_in:
            
            fig = figures_24h_in[ii]
            axes = axes_24h_in[ii]
            title = title[:-4] + 'Comparison'
        
        # Else make new ones
            
        else:
            
            fig, axes = plt.subplots(3,4, sharex=True, sharey=True, figsize=[12,6])
        
        fig.suptitle(title, fontsize=16)
        
        fig.text(0.5, 0.01, 'Amplitude / ms-1', ha='center', fontsize=16)
        fig.text(0.04, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)
        
        # Initialise variable to store all amps over the year
        
        if amps_year_plot:
            
            amps_year = np.zeros((12, len(heights)))
        
        for jj, (ax, subtitle) in enumerate(zip(axes.ravel(), subtitles)):
            
            amps, errs, _ = a24(hours, wind[jj,:,:])
            
            ax.plot(amps,heights, label=label)
            
            ax.fill_betweenx(heights,amps-errs,amps+errs, alpha = 0.25)
            
            if 'RADAR' in run:
                
                ax.errorbar(amps[5:], heights[5:], xerr = std_24h[jj,5:], fmt='none', elinewidth = 1.0, capsize=5, ecolor='black', errorevery = 3, alpha = 0.4)
            
            ax.set_title(subtitle)
            
            ax.set_xlim(0,50); ax.set_xticks(np.arange(0,51,10))
            ax.tick_params(axis='x', labelsize=13)
            
            ax.set_ylim(80000,100000)
            ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
            ax.tick_params(axis='y', labelsize=13)
            
            # Save monthly amplitude to do yearly plot at the end
            
            if amps_year_plot:
                
                amps_year[jj,:] = amps
            
        # If doing comparison add legend
            
        if figures_24h_in:
            
            # Just get latest handles and labels since they are all the same
            
            handles, labels = ax.get_legend_handles_labels()
                
            fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49,-0.01), framealpha=1.0)
        
        # Save completed figure
        
        if saving:
            
            fig.savefig(outdir + run + title.replace(' ','_') + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)
        
        figures_24h[ii] = fig
        axes_24h[ii] = axes
        
        # Plot amps for year
        
        if amps_year_plot:
        
            # Repeat December at front and January at end to give full first and last month (need to transpose for it to work)

            amps_year = np.column_stack((amps_year[-1,:],amps_year.T,amps_year[0,:]))
            amps_year = amps_year.T
            
            # Set up month coord
            
            months = np.arange(1,13)
            months = np.append(0, months) # append fictitious month for looping
            months = np.append(months, 13) # append fictitious month for looping
            
            # Make into meshgrid
            
            X, Y = np.meshgrid(months,heights)
            X = X.T
            Y = Y.T
            
            # Do contour plot
            
            top = np.nanmax(amps_year)
            
            #top = 60.0 # Hard code top if fixing the colourbar limits
            
            fig, ax = plt.subplots(figsize=[12,6])
            cs = ax.contourf(X,Y,amps_year, 25, vmin = 0.0, vmax=top)
            
            if 'RADAR' in run:
                ax.set_title(r'$\bf{Observed}$ ' + title + ' ' + 'Monthly Mean', fontsize=13.5)
            
            if 'MODEL' in run:
                ax.set_title(r'$\bf{ExUM}$ ' + title + ' ' + 'Monthly Mean', fontsize=13.5)
            
            ax.set_xlabel('Month', fontsize=16)
            ax.set_ylabel('Height / km', fontsize=16)

            ax.set_xticks(np.arange(0.5,13.5))
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.set_xlim(0.5,12.5)
            ax.set_ylim(79000.,101000.)
            ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
            ax.tick_params(axis='y', labelsize=14)
            ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
            ax.xaxis.set_minor_formatter(IndexFormatter(['','J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']))
            ax.tick_params(which='minor', color='white', labelsize=14)

            # Sort out colorbar
            mymap = plt.cm.ScalarMappable(cmap=cs.get_cmap())
            mymap.set_array(amps_year)
            top = cs.get_clim()[1]
            #top = 60.0 # Hard code top if fixing the colourbar limits
            #print(title, '    ', np.nanmax(amps_year[:,:-5]))
            #print(title, '    ', np.nanmax(amps_year))
            mymap.set_clim(0.0, top)

            cb = plt.colorbar(mymap)
            
            cb.set_label('Amplitude / ms-1', size=16)
            cb.ax.tick_params(labelsize=14)
            
            # Save figure
            
            plt.savefig(outdir + run + title.replace(' ','_') + '_Monthly_Mean'+ '.png', bbox_inches = 'tight', pad_inches = 0.2)
            
    # Plot each wind for each month for 12hr tide
    
    for ii, loop_vars in enumerate(loop_over_12h):
        
        # Extract looping variables
        
        if not 'RADAR' in run:
            
            wind, title = loop_vars
        
        elif 'RADAR' in run:
            
            wind, title, std_12h = loop_vars
        
        # If pre-existing figure and axes, use them
        
        if figures_12h_in:
            
            fig = figures_12h_in[ii]
            axes = axes_12h_in[ii]
            title = title[:-4] + 'Comparison'
        
        # Else make new ones
            
        else:
            
            fig, axes = plt.subplots(3,4, sharex=True, sharey=True, figsize=[12,6])
        
        fig.suptitle(title, fontsize=16)
        
        fig.text(0.5, 0.01, 'Amplitude / ms-1', ha='center', fontsize=16)
        fig.text(0.04, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)
        
        # Reinitialise variable to store all amps over the year
        
        if amps_year_plot:
            
            amps_year = np.zeros((12, len(heights)))
        
        for jj, (ax, subtitle) in enumerate(zip(axes.ravel(), subtitles)):
            
            amps, errs, _ = a12(hours, wind[jj,:,:])
            
            ax.plot(amps,heights, label=label)
            
            ax.fill_betweenx(heights,amps-errs,amps+errs, alpha = 0.25)
            
            if 'RADAR' in run:
                
                ax.errorbar(amps[5:], heights[5:], xerr = std_12h[jj,5:], fmt='none', elinewidth = 1.0, capsize=5, ecolor='black', errorevery = 3, alpha = 0.4)
            
            ax.set_title(subtitle)
            
            #ax.set_xlim(0,40)
            ax.set_xlim(0,50); ax.set_xticks(np.arange(0,51,10))
            ax.tick_params(axis='x', labelsize=13)
            
            ax.set_ylim(80000,100000)
            ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
            ax.tick_params(axis='y', labelsize=13)
            
            # Save monthly amplitude to do yearly plot at the end
            
            if amps_year_plot:
                
                amps_year[jj,:] = amps
            
        # If doing comparison add legend
            
        if figures_12h_in:
            
            # Just get latest handles and labels since they are all the same
            
            handles, labels = ax.get_legend_handles_labels()
                
            fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49,-0.01), framealpha=1.0)
            
        # Save completed figure
        
        if saving:
            fig.savefig(outdir + run + title.replace(' ','_') + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)
        
        figures_12h[ii] = fig
        axes_12h[ii] = axes
        
        # Plot amps for year
        
        if amps_year_plot:
        
            # Repeat December at front and January at end to give full first and last month (need to transpose for it to work)

            amps_year = np.column_stack((amps_year[-1,:],amps_year.T,amps_year[0,:]))
            amps_year = amps_year.T
            
            # Set up month coord
            
            months = np.arange(1,13)
            months = np.append(0, months) # append fictitious month for looping
            months = np.append(months, 13) # append fictitious month for looping
            
            # Make into meshgrid
            
            X, Y = np.meshgrid(months,heights)
            X = X.T
            Y = Y.T
            
            # Do contour plot
            
            top = np.nanmax(amps_year)
            
            #top = 41.0 # Hard code top if fixing the colourbar limits
            
            fig, ax = plt.subplots(figsize=[12,6])
            cs = ax.contourf(X,Y,amps_year, 25, vmin = 0.0, vmax=top)
            
            if 'RADAR' in run:
                ax.set_title(r'$\bf{Observed}$ ' + title + ' ' + 'Monthly Mean', fontsize=13.5)
            
            if 'MODEL' in run:
                ax.set_title(r'$\bf{ExUM}$ ' + title + ' ' + 'Monthly Mean', fontsize=13.5)
                
            ax.set_xlabel('Month', fontsize=16)
            ax.set_ylabel('Height / km', fontsize=16)

            ax.set_xticks(np.arange(0.5,13.5))
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.set_xlim(0.5,12.5)
            ax.set_ylim(79000.,101000.)
            ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
            ax.tick_params(axis='y', labelsize=14)
            ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
            ax.xaxis.set_minor_formatter(IndexFormatter(['','J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']))
            ax.tick_params(which='minor', color='white', labelsize=14)

            # Sort out colorbar
            mymap = plt.cm.ScalarMappable(cmap=cs.get_cmap())
            mymap.set_array(amps_year)
            top = cs.get_clim()[1]
            #top = 41.0 # Hard code top if fixing the colourbar limits
            #print(title, '    ', np.nanmax(amps_year[:,:-5]))
            #print(title, '    ', np.nanmax(amps_year))
            mymap.set_clim(0.0, top)

            cb = plt.colorbar(mymap, label= 'Mean wind speed / ms-1')
            
            cb.set_label('Amplitude / ms-1', size=16)
            cb.ax.tick_params(labelsize=14)
            
            # Save figure
            
            plt.savefig(outdir + run + title.replace(' ','_') + '_Monthly_Mean'+ '.png', bbox_inches = 'tight', pad_inches = 0.2)
        
    return figures_12h, figures_24h, axes_12h, axes_24h

def phase_plotter(run, figures_12h_in = None, figures_24h_in = None, axes_12h_in = None, axes_24h_in = None, saving=True, vert_wl = False, label=None):

    import os
    
    locale = os.getuid()
    xcsc = 11432
    mac = 501
    
    # Setup depending on locale
    
    if locale == xcsc:
        
        # Fix for crashing matplotlib on xcsc
        
        import matplotlib
        matplotlib.use('agg')
        
        # Setup directories
        
        indir = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
        outdir = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    elif locale == mac:
        
        # Setup directories
        
        indir = 'data/'
        outdir = '/Users/mjg41/Documents/PhD/Plots/Model/radar_comparison/'
    
    # Make the output directory if it doesn't exist
    
    if not os.path.exists(os.path.dirname(outdir + run)):
        try:
            os.makedirs(os.path.dirname(outdir + run))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    
    import iris
    import numpy as np
    import matplotlib.pyplot as plt
    from tidal_amplitude_and_phases import amplitude_and_phase_24h as a24
    from tidal_amplitude_and_phases import amplitude_and_phase_12h as a12
    import warnings
    from matplotlib.ticker import FuncFormatter
    
    warnings.filterwarnings("ignore")
    
    # Import winds and combine
    
    u_asc = iris.load(indir + run + 'u_asc_comp_day.nc')[0]
    u_rot = iris.load(indir + run + 'u_rot_comp_day.nc')[0]
    v_asc = iris.load(indir + run + 'v_asc_comp_day.nc')[0]
    v_rot = iris.load(indir + run + 'v_rot_comp_day.nc')[0]
    
    winds = [u_asc, u_rot, v_asc, v_rot]
    
    # Get coords
    
    hours = u_asc.coord('hour').points[:]
    heights = u_asc.coord('level_height').points[:]
    
    # Set titles and initialise output figs and axes
    
    year = str(np.int(str(u_asc.coord('forecast_reference_time').cell(0)[0])[:4]) + 1)
    
    titles_12h = ['Zonal Wind 12hr Tidal Phase from Composite Day at Ascension ' + year,
                  'Zonal Wind 12hr Tidal Phase from Composite Day at Rothera ' + year,
                  'Meridional Wind 12hr Tidal Phase from Composite Day at Ascension ' + year,
                  'Meridional Wind 12hr Tidal Phase from Composite Day at Rothera ' + year]
    titles_24h = ['Zonal Wind 24hr Tidal Phase from Composite Day at Ascension ' + year,
                  'Zonal Wind 24hr Tidal Phase from Composite Day at Rothera ' + year,
                  'Meridional Wind 24hr Tidal Phase from Composite Day at Ascension ' + year,
                  'Meridional Wind 24hr Tidal Phase from Composite Day at Rothera ' + year]
    
    subtitles = ['January', 'February', 'March', 'April', 'May', 'June',
                 'July', 'August', 'September', 'October', 'November',
                 'December']
    
    figures_12h = [None, None, None, None]

    figures_24h = [None, None, None, None]
    
    axes_12h = [None, None, None, None]
    
    axes_24h = [None, None, None, None]
    
    # Label line in comparison plot with levels and lid height
    
    if not label:
        if not 'RADAR' in run:
            # label = run.split('/')[1].split('_')[0] + '_' + run.split('/')[1].split('_')[1]
            label = r'$\bf{ExUM}$'
        elif 'RADAR' in run:
            label = r'$\bf{Observed}$'
    
    # Plot each wind for each month for 24hr tide and save
    
    for ii, (wind, title) in enumerate(zip(winds, titles_24h)):
        
        # If pre-existing figure and axes, use them
        
        if figures_24h_in:
            
            fig = figures_24h_in[ii]
            axes = axes_24h_in[ii]
            title = title[:-4] + 'Comparison'
        
        # Else make new ones
            
        else:
            
            fig, axes = plt.subplots(3,4, sharex=True, sharey=True, figsize=[12,6])
        
        fig.suptitle(title, fontsize=16)
        
        fig.text(0.5, 0.01, 'Phase / LT', ha='center', fontsize=16)
        fig.text(0.04, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)
        
        for jj, (ax, subtitle) in enumerate(zip(axes.ravel(), subtitles)):
            
            _, _, phases = a24(hours, wind.data[jj,:,:])
            
            # Convert phase to local time
            
            longitude = wind.coord('longitude').points[0] # long in degrees E
            
            phases = (phases + longitude * 24 / 360)%24
            
            # Do plot
            
            ax.scatter(phases, heights, label=label)
            
            if vert_wl:
            
                fit_x = np.unwrap(phases, discont = 2*np.pi)
                
                ax.scatter(fit_x,heights,s=7.5)
                
                m,b = best_fit_slope_and_intercept(heights,fit_x)
                
                ax.plot(m*heights + b, heights,'r')
                
                ax.text(0.01,0.01,'%d m' %(np.abs(24/m)), transform = ax.transAxes)
            
            ax.set_title(subtitle)
            
            ax.set_xlim(0,24); ax.set_xticks(np.arange(0, 25, 4))
            ax.tick_params(axis='x', labelsize=13)
            
            ax.set_ylim(79000,101000)
            ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
            ax.tick_params(axis='y', labelsize=13)
            
        # If doing comparison add legend
            
        if figures_24h_in:
                
            # Just get latest handles and labels since they are all the same
            
            handles, labels = ax.get_legend_handles_labels()
                
            fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49,-0.01))
        
        # Save completed figure
        
        if saving:
            fig.savefig(outdir + run + title.replace(' ','_') + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)
        
        figures_24h[ii] = fig
        axes_24h[ii] = axes
            
    # Plot each wind for each month for 12hr tide
    
    for ii, (wind, title) in enumerate(zip(winds, titles_12h)):
        
        # If pre-existing figure and axes, use them
        
        if figures_12h_in:
            
            fig = figures_12h_in[ii]
            axes = axes_12h_in[ii]
            title = title[:-4] + 'Comparison'
        
        # Else make new ones
            
        else:
            
            fig, axes = plt.subplots(3,4, sharex=True, sharey=True, figsize=[12,6])
        
        fig.suptitle(title, fontsize=16)
        
        fig.text(0.5, 0.01, 'Phase / LT', ha='center', fontsize=16)
        fig.text(0.04, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)
        
        for jj, (ax, subtitle) in enumerate(zip(axes.ravel(), subtitles)):
            
            _, _, phases = a12(hours, wind.data[jj,:,:])
            
            # Convert phase to local time
            
            longitude = wind.coord('longitude').points[0] # long in degrees E
            
            phases = (phases + longitude * 24 / 360)%12
            
            # Do plot
            
            ax.scatter(phases, heights, label=label)
            
            if vert_wl:
            
                fit_x = np.unwrap(phases, discont = 2*np.pi)
                
                ax.scatter(fit_x,heights,s=7.5)
                
                m,b = best_fit_slope_and_intercept(heights,fit_x)
                
                ax.plot(m*heights + b, heights,'r')
                
                ax.text(0.01,0.01,'%d m' %(np.abs(12/m)), transform = ax.transAxes)
            
            ax.set_title(subtitle)
            
            ax.set_xlim(0,12); ax.set_xticks(np.arange(0, 13, 2))
            ax.tick_params(axis='x', labelsize=13)
            
            ax.set_ylim(79000,101000)
            ax.set_yticks([80000., 85000., 90000., 95000., 100000.])
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
            ax.tick_params(axis='y', labelsize=13)
            
        # If doing comparison add legend
            
        if figures_12h_in:
            
            # Just get latest handles and labels since they are all the same
            
            handles, labels = ax.get_legend_handles_labels()
                
            fig.legend(handles, labels, loc='lower center', fontsize='large', ncol=len(labels), bbox_to_anchor=(0.49,-0.01))
            
        # Save completed figure
        
        if saving:
            fig.savefig(outdir + run + title.replace(' ','_') + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)
        
        figures_12h[ii] = fig
        axes_12h[ii] = axes
        
    return figures_12h, figures_24h, axes_12h, axes_24h

if __name__ == '__main__':
    
    import os
    
    locale = os.getuid()
    xcsc = 11432
    mac = 501
    
    # Setup depending on locale
    
    if locale == xcsc:
        
        # Fix for crashing matplotlib on xcsc
        
        import matplotlib
        matplotlib.use('agg')
        
        # Setup directories
        
        indir = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
        outdir = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    elif locale == mac:
        
        # Setup directories
        
        indir = 'data/'
        outdir = '/Users/mjg41/Documents/PhD/Plots/Model/radar_comparison/'
    
    import iris
    import numpy as np
    import matplotlib.pyplot as plt
    import warnings
    
    warnings.filterwarnings("ignore")
    
    # Get user input

    run = input('Please enter path to file relative to "radar_comparison/":    ')
    
    # Do figures

    #figures_12h, figures_24h, axes_12h, axes_24h = amp_plotter(run, saving=False)
    
    figures_12h_p, figures_24h_p, axes_12h_p, axes_24h_p = phase_plotter(run, saving=False, vert_wl=True)
    
    # If on mac, show figures
    
    if locale == mac:
        plt.show()
