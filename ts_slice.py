#!/usr/bin/python3

import iris
import cf_units
import copy

# Initialise slice_more variable and input path

slice_more = 'yes'
inputpath = '/projects/ukca-ex/mgriffit/output_files/'

# Get input pp file
run = input('Please enter path to file relative to "output_files/": ')
ppfile = input('Please enter pp file to be sliced: ')

print('Loading cube')

cubes = iris.load(inputpath + run + ppfile)

while slice_more == 'yes':
    
    # Initialise new_cube to be filled
    
    new_cube = copy.deepcopy(cubes)
    
    # Choose output nc file
    
    ncfile = input('Please enter filename of new nc file: ')
    
    # Slice according to chosen timestep

    ts = input('Which timestep (index)?    ')

    print('Chopping at timestep "' + str(ts) + '"')

    for i in range(0,len(cubes)):
        new_cube[i] = cubes[i][ts,:,:,:]
        
    # Change some units

    print('Converting units')

    new_cube.extract(iris.Constraint('air_pressure'))[0].convert_units('hPa')
    new_cube.extract(iris.Constraint('tendency_of_air_temperature_due_to_longwave_heating'))[0].convert_units('K d-1')
    new_cube.extract(iris.Constraint('tendency_of_air_temperature_due_to_shortwave_heating'))[0].convert_units('K d-1')

    # Add names and units for GW drag variables if they are present

    eastGWcube = new_cube.extract(iris.AttributeConstraint(STASH='m01s06i105'))
    northGWcube = new_cube.extract(iris.AttributeConstraint(STASH='m01s06i106'))

    if eastGWcube:
        new_cube.extract(iris.AttributeConstraint(STASH='m01s06i105'))[0].long_name = 'tendency_of_eastward_wind_due_to_nonorographic_gravity_wave_drag'
        new_cube.extract(iris.AttributeConstraint(STASH='m01s06i105'))[0].units = cf_units.Unit('m s-2')
    if northGWcube:
        new_cube.extract(iris.AttributeConstraint(STASH='m01s06i106'))[0].long_name = 'tendency_of_northward_wind_due_to_nonorographic_gravity_wave_drag'
        new_cube.extract(iris.AttributeConstraint(STASH='m01s06i106'))[0].units = cf_units.Unit('m s-2')

    # Print timestep where slice performed

    ts_string = str(new_cube.extract(iris.Constraint('air_pressure'))[0].coord('time').cell(0)[0])
    print('Successfully sliced at ' + ts_string)

    # Save the new file

    print('Saving file')

    iris.save(new_cube, inputpath + run + ncfile)
    
    slice_more = input('Would you like to take another slice?')
