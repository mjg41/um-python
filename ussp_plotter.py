import numpy as np
import matplotlib.pyplot as plt
import iris.plot as iplt
from matplotlib import colors

# Plotting function which takes the output from offline USSP code and
# plots it

# Open gwd file, read in data and close file

gwd_file = '/Users/mjg41/Documents/PhD/USSP/JT_USSP_in_MLT/gwd.dat'

f = open(gwd_file,'r')
data = f.read()
f.close()

# Split data according to empty lines

data = data.split('\n\n')

# First set of data is number of lats and number of levels as well as
# listing lats and levels. Permanent so define them manually here

lats = np.linspace(-80.0, 80.0, 33)
levels = np.array([100.0, 2200.0, 4200.0, 6100.0, 8000.0, 9800.0, 
                   11400.0, 13100.0, 14600.0, 16100.0, 17600.0, 19200.0,
                   20700.0, 22300.0, 23900.0, 25500.0, 27100.0, 28800.0,
                   30500.0, 32200.0, 33900.0, 35700.0, 37600.0, 39400.0,
                   41300.0, 43300.0, 45200.0, 47200.0, 49200.0, 51200.0,
                   53100.0, 55000.0, 56900.0, 58700.0, 60500.0, 62300.0,
                   64000.0, 65800.0, 67400.0, 69100.0, 70700.0, 72300.0,
                   73900.0, 75500.0, 77100.0, 78700.0, 80200.0, 81800.0,
                   83300.0, 84800.0, 86300.0, 87700.0, 89200.0, 90600.0,
                   92000.0, 93300.0, 94700.0, 96100.0, 97500.0, 98900.0,
                   100400.0, 101800.0, 103300.0, 104900.0, 106500.0,
                   108200.0, 110000.0, 112000.0, 114200.0, 116700.0,
                   119700.0])

# Second set is CIRA temperature

t_cira = data[1]
t_cira = t_cira.replace('\n','')
t_cira = np.fromstring(t_cira,sep = '  ')
t_cira = t_cira.reshape(71,33)
# Mask missing data
t_cira = np.ma.array(t_cira, mask=(t_cira == 999.9))

# Third set is CIRA wind

u_cira = data[2]
u_cira = u_cira.replace('\n','')
u_cira = np.fromstring(u_cira,sep = '  ')
u_cira = u_cira.reshape(71,33)
# Mask missing data
u_cira = np.ma.array(u_cira, mask=(u_cira == 999.9))

# Fourth set is Eastward GW forcing

f_x = data[3]
f_x = f_x.replace('\n','')
f_x = np.fromstring(f_x,sep = '  ')
f_x = f_x.reshape(71,33)

# Fifth set is Northward GW forcing

f_y = data[4]
f_y = f_y.replace('\n','')
f_y = np.fromstring(f_y,sep = '  ')
f_y = f_y.reshape(71,33)

## Plot everything ##
# Set up X,Y coords

X,Y = np.meshgrid(lats,levels)

# Plot CIRA temp data

plt.figure(figsize=(14,6))
plt.suptitle('CIRA Data')
plt.subplot(121)
plt.title('CIRA Zonal Mean Temperature')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
#plt.contourf(X,Y,t_cira,25,cmap='jet')
plt.pcolormesh(X,Y,t_cira,cmap='jet')
cb = plt.colorbar(orientation = 'horizontal')
cb.set_label('Temperature / K')

# Plot CIRA wind data

plt.subplot(122)
# Set colorbar with symmetric limits
top = np.max(np.abs(u_cira))
cmap_wind = iplt.plt.cm.get_cmap('brewer_RdBu_11')
plt.title('CIRA Mean Zonal Mean Wind')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
#plt.contourf(X,Y,u_cira,25,cmap = cmap_wind, vmin=-top, vmax=top)
plt.pcolormesh(X,Y,u_cira,cmap = cmap_wind, vmin=-top, vmax=top)
cb = plt.colorbar(orientation = 'horizontal')
cb.set_label('Zonal Wind / m s-1')


# Plot Zonal GW forcing

plt.figure(figsize=(14,6))
plt.suptitle('GW Forcing')
plt.subplot(121)
# Set colorbar with symmetric limits
top = np.max(np.abs(f_x))
plt.title('Zonal GW Forcing')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
#plt.contourf(X,Y,f_x,25,cmap = cmap_wind, vmin=-top, vmax=top)
plt.pcolormesh(X,Y,f_x,cmap = cmap_wind, vmin=-top, vmax=top)
cb = plt.colorbar(orientation = 'horizontal')
cb.set_label('Zonal Wind Tendency / m s-2')

# Plot Meridional GW forcing

plt.subplot(122)
# Set colorbar with symmetric limits
top = np.max(np.abs(f_x))
plt.title('Meridional GW Forcing')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
#plt.contourf(X,Y,f_y,25,cmap = cmap_wind, vmin=-top, vmax=top)
plt.pcolormesh(X,Y,f_y,cmap = cmap_wind, vmin=-top, vmax=top)
cb = plt.colorbar(orientation = 'horizontal')
cb.set_label('Meridional Wind Tendency / m s-2')

##----- This did the first set of data gwd.dat, now do chop.dat ------##

# Open chop file, read in data and close file

chop_file = '/Users/mjg41/Documents/PhD/USSP/JT_USSP_in_MLT/chop.dat'

g = open(chop_file,'r')
data_chop = g.read()
g.close()

# Split data according to empty lines

data_chop = data_chop.split('\n\n')

# First set of data is number of lats and number of levels as well as
# listing lats and half levels. Permanent so define them manually here.
# Lats as above

levels_half = np.zeros(72)
levels_half[1:-1] = 0.5*(levels[:-1] + levels[1:])
levels_half[-1] = 1.5*levels[-1] - 0.5*levels[-2]

# Second set is first chop direction
chop_north = data_chop[1]
chop_north = chop_north.replace('\n','')
chop_north = np.fromstring(chop_north,sep = '   ')
chop_north = chop_north.reshape(72,33)

# Second set is second chop direction
chop_west = data_chop[2]
chop_west = chop_west.replace('\n','')
chop_west = np.fromstring(chop_west,sep = '   ')
chop_west = chop_west.reshape(72,33)

# Second set is third chop direction
chop_south = data_chop[3]
chop_south = chop_south.replace('\n','')
chop_south = np.fromstring(chop_south,sep = '   ')
chop_south = chop_south.reshape(72,33)

# Second set is fourth chop direction
chop_east = data_chop[4]
chop_east = chop_east.replace('\n','')
chop_east = np.fromstring(chop_east,sep = '   ')
chop_east = chop_east.reshape(72,33)

## Plot everything from chop.dat ##
#Set up X,Y coords and define custom colouring

X,Y = np.meshgrid(lats,levels_half)
cmap_chop = colors.ListedColormap(['gold','midnightblue','firebrick','m','g','r','b','dimgray','k'])
bounds = [-9, -3.5, -2.5, -0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 9]
norm = colors.BoundaryNorm(bounds, cmap_chop.N)

# Plot chop_north

plt.figure(figsize=(14,11))
plt.subplot(221)
plt.suptitle('Chop Types for Various Azimuths')
plt.title('Northward Chop Type')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
plt.pcolormesh(X,Y,chop_north,cmap=cmap_chop,norm=norm,vmax=9,vmin=-9)
cb = plt.colorbar(orientation = 'horizontal', norm=norm, 
                  ticks=[-9, -3, -2, 0, 1, 2, 3, 4, 9])
cb.set_label('Chop Type')

# Plot chop_west

plt.subplot(222)
plt.title('Westward Chop Type')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
plt.pcolormesh(X,Y,chop_west,cmap=cmap_chop,norm=norm,vmax=9,vmin=-9)
cb = plt.colorbar(orientation = 'horizontal', norm=norm, 
                  ticks=[-9, -3, -2, 0, 1, 2, 3, 4, 9])
cb.set_label('Chop Type')

# Plot chop_south

plt.subplot(223)
plt.title('Southward Chop Type')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
plt.pcolormesh(X,Y,chop_south,cmap=cmap_chop,norm=norm,vmax=9,vmin=-9)
cb = plt.colorbar(orientation = 'horizontal', norm=norm, 
                  ticks=[-9, -3, -2, 0, 1, 2, 3, 4, 9])
cb.set_label('Chop Type')

# Plot chop_east

plt.subplot(224)
plt.title('Eastward Chop Type')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
plt.pcolormesh(X,Y,chop_east,cmap=cmap_chop,norm=norm,vmax=9,vmin=-9)
cb = plt.colorbar(orientation = 'horizontal', norm=norm, 
                  ticks=[-9, -3, -2, 0, 1, 2, 3, 4, 9])
cb.set_label('Chop Type')
plt.show()
