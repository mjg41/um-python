import numpy as np
import iris
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

# Latitudes from model

lats_model = np.array([-80.625, -60.625, -40.625, -20.625, -0.625, 20.625, 40.625, 60.625, 80.625])
                 
# Model heights

heights_model = np.array([20.00,     53.33,    100.00,    160.00,    233.33,
                    320.00,    420.00,    533.33,    660.00,    800.00,
                    953.33,   1120.00,   1300.00,   1493.33,   1700.00,
                    1920.00,   2153.33,   2400.00,   2660.00,   2933.33,
                    3220.00,   3520.00,   3833.33,   4160.00,   4500.00,
                    4853.33,   5220.00,   5600.00,   5993.33,   6400.00,
                    6820.00,   7253.33,   7700.00,   8160.00,   8633.34,
                    9120.01,   9620.02,  10133.37,  10660.08,  11200.16,
                   11753.64,  12320.55,  12900.93,  13494.88,  14102.48,
                   14723.88,  15359.24,  16008.82,  16672.90,  17351.90,
                   18046.29,  18756.70,  19483.89,  20228.78,  20992.53,
                   21776.51,  22582.39,  23412.16,  24268.18,  25153.23,
                   26070.59,  27024.11,  28018.26,  29058.23,  30150.02,
                   31300.54,  32517.71,  33810.59,  35189.52,  36666.24,
                   38254.03,  39967.93,  41824.85,  43843.83,  46046.21,
                   48455.83,  51099.35,  53908.63,  56717.91,  59527.20,
                   62336.48,  65145.76,  67955.04,  70764.32,  73573.60,
                   76382.89,  79192.17,  82001.45,  84822.13,  87686.62,
                   90586.83,  93474.60,  96326.35,  99175.77, 102112.84,
                  105276.25, 108838.27, 112970.75, 117804.39, 123400.11])

model_nudge_field = np.zeros((12,9,100))

#~ # Temps for Jan for first, middle(-1.45) and last lat above

#~ model_nudge_field[0,0,-7:] = []

#~ model_nudge_field[0,1,-8:] = []

#~ model_nudge_field[0,2,-8:] = []

#~ # Temps for Feb for first, middle(-1.45) and last lat above

#~ model_nudge_field[1,0,-7:] = []

#~ model_nudge_field[1,1,-8:] = []

#~ model_nudge_field[1,2,-8:] = []

#~ # Temps for Mar for first, middle(-1.45) and last lat above

#~ model_nudge_field[2,0,-7:] = []

#~ model_nudge_field[2,1,-8:] = []

#~ model_nudge_field[2,2,-8:] = []

#~ # Temps for Apr for first, middle(-1.45) and last lat above

#~ model_nudge_field[3,0,-7:] = []

#~ model_nudge_field[3,1,-8:] = []

#~ model_nudge_field[3,2,-8:] = []

#~ # Temps for May for first, middle(-1.45) and last lat above

#~ model_nudge_field[4,0,-7:] = []

#~ model_nudge_field[4,1,-8:] = []

#~ model_nudge_field[4,2,-8:] = []

# Temps for Jun for various latitudes as above

#-80.625
model_nudge_field[5,0,-7:] = [203.94032878112529,  205.40524856281769,  210.87808832009298,  223.41130221511546,  248.0614842383504,  292.74850484632998,  344.4803190215224]

#-60.625
model_nudge_field[5,1,-7:] = [201.22666821173385,  203.17601377442017,  209.39984835575785,  222.96990325398627,  249.02060432533358,  295.56714195905374,  347.11014042814804]

#-40.625
model_nudge_field[5,2,-7:] = [196.02995702522094,  199.06564388117795,  206.86249455525822,  222.56083117955774,  251.45747113669,  301.75966513936169,  352.88783701316981]

#-20.625
model_nudge_field[5,3,-8:] = [188.05235115409508,  189.39993774061764,  194.15271461440173,  204.27724188528896,  223.04781014794034,  255.99774086860779,  311.60346690323865,  362.0722192397302]

#-0.625
model_nudge_field[5,4,-8:] = [179.71886070774912,  182.66353556359766,  189.65911838271566,  202.67794290913659,  225.18247798363959,  262.99238135865028,  324.89970834601672,  374.4777680831894]

#20.625
model_nudge_field[5,5,-9:] = [169.97016843697872,  171.65509958660039,  176.69111185206,  186.35822978650779,  202.70815636459395,  229.43083289011321,  272.665213894114,  341.54667848509985,  390.00958600668025]

#40.625
model_nudge_field[5,6,-9:] = [162.65364627192793,  165.82496586617009,  172.8326506373129,  184.90217709785759,  204.1816369433547,  234.56426817290983,  282.46177957308953,  357.29763392628615,  404.70541173289848]

#60.625
model_nudge_field[5,7,-10:] = [156.03905330517773, 157.36525532494284,  161.88592120220298,  170.56612851510289,  184.62320238625907,  206.29368774241985,  239.63727998587251,  291.28340311430998,  370.88273922157668,  417.38047393070485]

#80.625
model_nudge_field[5,8,-10:] = [153.23028514016454,  155.03658209683198,  160.2394254717683,  169.74154940082607,  184.7632186257778,  207.58569847765853,  242.34884859537488,  295.78556315142004,  377.65376986747935,  423.69792475015652]

indices = [-7, -7, -7, -8, -8, -9, -9, -10, -10]

#~ # Temps for Jul for first, middle(-1.45) and last lat above

#~ model_nudge_field[6,0,-7:] = []

#~ model_nudge_field[6,1,-8:] = []

#~ model_nudge_field[6,2,-8:] = []

#~ # Temps for Aug for first, middle(-1.45) and last lat above

#~ model_nudge_field[7,0,-7:] = []

#~ model_nudge_field[7,1,-8:] = []

#~ model_nudge_field[7,2,-8:] = []

#~ # Temps for Sep for first, middle(-1.45) and last lat above

#~ model_nudge_field[8,0,-7:] = []

#~ model_nudge_field[8,1,-8:] = []

#~ model_nudge_field[8,2,-8:] = []

#~ # Temps for Oct for first, middle(-1.45) and last lat above

#~ model_nudge_field[9,0,-7:] = []

#~ model_nudge_field[9,1,-8:] = []

#~ model_nudge_field[9,2,-8:] = []

#~ # Temps for Nov for first, middle(-1.45) and last lat above

#~ model_nudge_field[10,0,-7:] = []

#~ model_nudge_field[10,1,-8:] = []

#~ model_nudge_field[10,2,-8:] = []

#~ # Temps for Dec for first, middle(-1.45) and last lat above

#~ model_nudge_field[11,0,-7:] = []

#~ model_nudge_field[11,1,-8:] = []

#~ model_nudge_field[11,2,-8:] = []

# Import offline nudging field

offline_nudge_cube = iris.load('data/t_nudge_lat_time_dep.nc')[0]

# Extract temperature and coordinates

offline_nudge_field = offline_nudge_cube.data

months = offline_nudge_cube.coord('time').points[:]

heights_offline = offline_nudge_cube.coord('height').points[:]

lats_offline = offline_nudge_cube.coord('latitude').points[:]

# Extend offline_nudge_field up to model height

heights_offline = np.append(heights_offline, 123400.11)

sigma = (123400.11 - 119700.)*(6371229.0 + 119700.)/(6371229.0 + 123400.11)

append_to_offline = np.zeros((12,33))

for i in np.arange(12):
    for j in np.arange(33):
        append_to_offline[i,j] = 1065.0 - (1065.0 - offline_nudge_field[i,j,-1])*np.exp(-0.00001875*sigma)

offline_nudge_field = np.dstack((offline_nudge_field, append_to_offline))

indices_lats = [0, 4, 8, 12, 16, 20, 24, 28, 32]

# Check similarity with plots - only interested in field above heights_model[-11]=87686.62, heights_offline[12] = 87700
# For lats use value at lats[15]=-5 and lats[16]=0 as comparison, 

#~ fig, axes = plt.subplots(3, 4, sharex=True, sharey=True, figsize=[12,8])

#~ fig.suptitle('Nudging online v offline check for each month.')

#~ fig.text(0.5, 0.01, 'Temperature / K', ha='center', fontsize=16)
#~ fig.text(0.04, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)

#~ subtitles = ['January', 'February', 'March', 'April', 'May', 'June',
             #~ 'July', 'August', 'September', 'October', 'November',
             #~ 'December']

#~ for ii, (ax, subtitle) in enumerate(zip(axes.ravel(), subtitles)):
    
    #~ ax.plot(model_nudge_field[ii,0,-6:], heights_model[-6:], label='ONLINE - 89.375S')
    #~ ax.plot(model_nudge_field[ii,1,-7:], heights_model[-7:], label='ONLINE - 40S')
    #~ ax.plot(model_nudge_field[ii,2,-9:], heights_model[-9:], label='ONLINE - 31N')
    #~ ax.plot(model_nudge_field[ii,3,-9:], heights_model[-9:], label='ONLINE - 54.45N')  
    
    #~ ax.plot(offline_nudge_field[ii,0,20:], heights_offline[20:], label='OFFLINE - 80S')
    #~ ax.plot(offline_nudge_field[ii,8,20:], heights_offline[20:], label='OFFLINE - 40S')
    #~ ax.plot(offline_nudge_field[ii,22,16:], heights_offline[16:], label='OFFLINE - 30N')
    #~ ax.plot(offline_nudge_field[ii,27,16:], heights_offline[16:], label='OFFLINE - 55N')
    
    #~ ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
    #~ ax.set_title(subtitle)

#~ handles, labels = ax.get_legend_handles_labels()
#~ fig.legend(handles, labels, loc='lower center', bbox_to_anchor=(0.75,-0.02), framealpha=1.0)
#~ fig.savefig('/Users/mjg41/Documents/PhD/Plots/Model/nudging_3d_profile/offline_online_all_months_equator.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)

fig, axes = plt.subplots(3, 3, sharex=True, sharey=True, figsize=[12,8])

fig.suptitle('Nudging online v offline check for June.')

fig.text(0.5, 0.01, 'Temperature / K', ha='center', fontsize=16)
fig.text(0.04, 0.5, 'Height / km', va='center', rotation='vertical', fontsize=16)

for ii, ax in enumerate(axes.ravel()):
    
    #~ ax.plot(np.flip(model_nudge_field, axis=1)[5,ii,np.flip(indices)[ii]:], heights_model[np.flip(indices)[ii]:], label='ONLINE: ' + str(np.flip(lats_model)[ii]))
    ax.plot(model_nudge_field[5,ii,indices[ii]:], heights_model[indices[ii]:], label='ONLINE: ' + str(lats_model[ii]))
    
    ax.plot(offline_nudge_field[5,indices_lats[ii],8:], heights_offline[8:], label='OFFLINE: ' + str(lats_offline[indices_lats[ii]]))
    
    ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
    
    ax.legend()
    
fig.savefig('/Users/mjg41/Documents/PhD/Plots/Model/nudging_3d_profile/offline_online_june_height.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.35)
plt.show()
