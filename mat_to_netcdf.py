import numpy as np
import iris
import scipy.io as sio

# Set up directories

indir = 'data/RADAR/'
outdir = 'data/RADAR/'

#run = input('Please enter path to file relative to "RADAR/":    ')
#file_name = input('Please enter mat filename:    ')

run = '20090901T0000Z/'

file_name = '2010Rotvcomp'

output_name = 'v_rot_comp_day'

# Get template cube using pre-existing cube as template

template = iris.load('data/MODEL/20050901T0000Z/L94_102km_nud90_nlte/' + output_name + '.nc')[0]

# Load in .mat file

mat_file = sio.loadmat(indir + run + file_name + '.mat')

# Get key from dict - last one is one referencing data

wind_key = list(mat_file.keys())[-1]

# Extract radar data

radar_data = mat_file[wind_key]

# Swap axes on radar data to get [month,hour,height]

radar_data = np.swapaxes(radar_data,1,2)

heights = np.arange(76000.,106000.,1000.)
height = iris.coords.DimCoord(heights, standard_name = 'height', units = 'm')

# Extract coordinates and other info using new heights

month = template.coord('month')

hour = template.coord('hour')

heights = np.arange(76000.,106000.,1000.)
height = iris.coords.DimCoord(heights, long_name = 'level_height', units = 'm', attributes={'positive': 'up'})

forecast_reference_time = template.coord('forecast_reference_time')
latitude = template.coord('latitude')
longitude = template.coord('longitude')
sigma = template.coord('sigma')

Conventions = template.attributes.get('Conventions')
STASH = template.attributes.get('STASH')
source = 'Data from Meteor Radar'
cell_method = iris.coords.CellMethod(method='mean', coords=('hour',), intervals=(), comments=())

# Construct cube which will contain aircraft data

radar_cube = iris.cube.Cube(radar_data,
                            standard_name = template.standard_name,
                            var_name = template.var_name,
                            units = 'm s-1',
                            dim_coords_and_dims = [(month, 0), (hour, 1), (height, 2)],
                            aux_coords_and_dims = [(forecast_reference_time, None), (latitude, None), (longitude, None), (sigma, None)],
                            attributes = dict(Conventions = Conventions, STASH = STASH, source = source),
                            cell_methods = (cell_method, ))

# Save cube

iris.save(radar_cube, outdir + run + output_name + '.nc')
