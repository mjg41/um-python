#! /usr/local/sci/bin/python2.7
# -*- coding: iso-8859-1 -*-

import pdb
import numpy as np
import scipy.interpolate as spint
import matplotlib as mpl
import matplotlib.pyplot as plt
#import mpl_toolkits.basemap as bm


def bilinear_interpolation(x, y, data, x_want, y_want):
    """Interpolate 2D data, from coordinates x and y, onto x_want & y_want
    Perform the interpolation using scipy's bilinear interpolation routine
    Note this assumes Euclidean geometry (a flat 2d surface) - if using Earth
    data, be aware this assumption worsens towards the poles
    Make sure that
    0) Your input coordinates are monotonically increasing, as this is needed
       by RectBivariateSpline. If you correct for this, remember to flip the 
       data accordingly too!
    1) If the data behaves logarithmically wrt to one of the coordinates, that
       you've corrected for that (e.g. converting pressures to log pressures)
    2) If one of the coordinates is cyclic, that you've corrected for that (e.g.
       made an addcyclic extension to longitudes and data)
    """
    # Remove NaNs from data (interpolator otherwise unhappy) and adjust corresponding coordinates
    
    # Make a unique list of pressure levels (columns) containing a NaN
    index = np.unique(np.argwhere(np.isnan(data))[:,1])
    # Remove the corresponding pressure levels from coordinate
    y = np.delete(y,index) 
    # Remove the pressure levels (columns) containing NaNs from data
    data = data[:,~np.isnan(data).any(axis=0)] 
    
    #Check the data coordinates match x by y
    if data.shape != (len(x), len(y)):
        msg = "Data shape {} does not match coordinates ({}x{})".format(\
                          data.shape, len(x), len(y))
        raise ValueError, msg
    
    #Get interpfn corresponding to a linear bivariate spline (so kx = ky = 1)
    interpfn = spint.RectBivariateSpline(x, y, data, kx=1, ky=1)
    
    #Interpolate onto each location
    X_WANT,Y_WANT = np.meshgrid(x_want,y_want)
    data_out = interpfn.ev(X_WANT.T, Y_WANT.T)
    
    return data_out

def test(log = None, realistic_log = None):
    """Test the bilinear_interpolation function (optionally with log-like data)"""
    
    #Make some dummy data
    if log:
        descr = "logarithmically dependent"
        delta_x, num_y = 1.5, 100
        delta_xwant, num_ywant = delta_x * 15, int(num_y / 10.0)
        x = np.arange(-180, 180, delta_x)
        x_want = np.arange(-180, 180, delta_xwant)
        if realistic_log:
            y = np.logspace(np.log10(1000), np.log10(0.1), num_y)
            y_want = np.logspace(np.log10(1000), np.log10(0.1), num_ywant)
        else:
            y = np.logspace(np.log10(0.1), np.log10(1000), num_y)
            y_want = np.logspace(np.log10(0.1), np.log10(1000), num_ywant)
        
        X, Y = np.meshgrid(x, np.log10(y))
        #API: (X, Y, sigmax=1.0, sigmay=1.0, mux=0.0, muy=0.0, sigmaxy=0.0)
        Z1 = plt.mlab.bivariate_normal(X, Y, 90, 1.5, 0.0, 1.0)
        Z2 = plt.mlab.bivariate_normal(X, Y, 120, 0.5, -60, 0.0)
        Z = 10 * (Z1.T - Z2.T)
    else:
        descr = "normal"
        delta_x, delta_y = 1.5, 2
        delta_xwant, delta_ywant = delta_x * 15, delta_y * 10
        x = np.arange(-180, 180, delta_x)
        y = np.arange(-90, 90, delta_y)
        x_want = np.arange(-180, 180, delta_xwant)
        y_want = np.arange(-90, 90, delta_ywant)
        X, Y = np.meshgrid(x, y)
        Z1 = plt.mlab.bivariate_normal(X, Y, 90, 90, 0.0, 0.0)
        Z2 = plt.mlab.bivariate_normal(X, Y, 120, 60, -60, -30)
        Z = 10 * (Z1.T - Z2.T)
    
    print("Testing some {} data".format(descr))
    
    #Add some high frequency random noise to Z - what you'll lose when coarsening
    Z_random = np.random.uniform(Z.min(), Z.max(), Z.shape)
    Z = Z + (0.1 * Z_random) #Factor influences how much random noise you have

    
    #Coarsen Z
    if log:

        Z_coarse = bilinear_interpolation(x, np.log10(y), Z, x_want, np.log10(y_want))
    else:
        Z_coarse = bilinear_interpolation(x, y, Z, x_want, y_want)
    
    #Interpolate the coarsened stuff back up to the original
    if log:
        Z_coarse_sharp = bilinear_interpolation(x_want, np.log10(y_want), Z_coarse, x, np.log10(y))
    else:
        Z_coarse_sharp = bilinear_interpolation(x_want, y_want, Z_coarse, x, y)
    Z_diff = Z - Z_coarse_sharp
    
    #Illustrate the results
    fig, axArr = plt.subplots(2,2, sharex=True, sharey=True)
    datas = [Z, Z_coarse, Z_coarse_sharp, Z_diff]
    tits = ["Original", "Coarsened", "Coarsened, interpolated back up", 
            "Difference in latter cf Original"]
    exs = [x, x_want, x, x]
    wys = [y, y_want, y, y]
    cmap = mpl.cm.seismic
    colrange = np.min(np.abs([Z.min(), Z.max()]))
    norm = mpl.colors.Normalize(vmin = -colrange, vmax = colrange)
    cmap.set_bad("green")
    cmap.set_over("purple")
    cmap.set_under("magenta")
    for ax, data, tit, ex, wy in zip(axArr.flatten(), datas, tits, exs, wys):
        plt.sca(ax)
        plt.pcolormesh(ex, wy, data.T, cmap = cmap, norm = norm)
        plt.colorbar(extend="both")
        plt.title(tit)
        if log:
            plt.yscale('log')
            
    plt.show()

def main():
    test()
    test(log=True)
    test(log=True, realistic_log=True)

if __name__ == "__main__":
    main()
