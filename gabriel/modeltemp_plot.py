#! /usr/local/sci/bin/python2.7
# -*- coding: iso-8859-1 -*-

# Plots Model Run Temp Data for a given month (specified below)

import os
import pdb
import scipy
import numpy as np
import datetime as dt
import calendar as cal
import matplotlib as mpl
import matplotlib.pyplot as plt
import netCDF4 as ncdf
import iris
from apply_hv import apply_hv

# Load in model run dataset
path = '/projects/ukca-ex/mgriffit/output_files/19880901T0000Z'
filename = '85km/ao114a.pg19880901'
pathfile = os.path.join(path, filename)

# Extract air temp cube
cubes=iris.load(pathfile,'air_temperature')
modeltemp=cubes[0]

# Correct data for terrain-following grid using heaviside function
hv = iris.load(pathfile)[0]
modeltemp = apply_hv(hv,modeltemp)

# Select month and edit the date
#############################################################
date = 'sep88'
modeltemp_month=modeltemp[0,:,:,:]
#############################################################

# Zonal mean already taken, so just choose it
modeltemp_month = modeltemp_month[:,:,0]

# If not already taken, need to take it
# modeltemp_month = modeltemp_month.collapsed(['longitude'],iris.analysis.MEAN)

# Extract model run data and coords
temp=modeltemp_month.data
x= modeltemp_month.coord('latitude').points[:] # According to modeltemp resolution
y= modeltemp_month.coord('pressure').points[:] # According to modeltemp pressure levels

# Define data limits for colorbar
tempmin, tempmax = [np.nanmin(temp),np.nanmax(temp)]

# Plot the data
cs = plt.contourf(x,y,temp, 100, origin = 'lower', hold = 'on',vmin=tempmin,vmax=tempmax)  

# Plot the contour lines with labels
#cs2 = plt.contour(cs, levels = cs.levels[::10], colors = 'k', origin = 'lower')    
#plt.clabel(cs2, colors = 'k', fmt = '%3.0f')

# Give the plot a title based on the filename (e.g. sep05)
title = 'Model Run Temperature ' + date[0].upper() + date[1:3] + ' ' + date[3:5]
plt.title(title)

# Sort out the x axis
plt.xlabel('Latitude / degrees')
xticks = range(-90,91,15)
cs.ax.set_xticks(xticks)

# Sort out the y axis
plt.ylabel('Pressure / hPa')
plt.yscale('log')
plt.gca().invert_yaxis()

# Plot colorbar
cb = plt.colorbar(cs, orientation='horizontal')
cb.set_label('Temperature / K')

plt.savefig('/home/d03/mgriffit/Documents/Plots/Model/' + title + '.eps')

