#! /usr/local/sci/bin/python2.7
# -*- coding: iso-8859-1 -*-

# Plot the difference between MLS Data and Model Run Data for a given month (specified below)

import os
import pdb
import scipy
import numpy as np
import datetime as dt
import calendar as cal
import matplotlib as mpl
import matplotlib.pyplot as plt
import netCDF4 as ncdf
import iris
from GabrielInterpolator import bilinear_interpolation

# NetCDF default loading behaviour currently does not expose variables which define reference surfaces for dimensionless vertical coordinates as independent Cubes. This behaviour is deprecated in favour of automatic promotion to Cubes. To switch to the new behaviour:
iris.FUTURE.netcdf_promote = True

# Load in model run dataset
path = "/data/users/mgriffit/output_files"
filename = "85km/ao114a.pg19880901"
pathfile = os.path.join(path, filename)

# Extract u wind cube
cubes=iris.load(pathfile,'x_wind')
modelwind=cubes[0] # Choose cube on pressure levels


# Select month
#############################################################
modelwind_month=modelwind[0,:,:,:]
#############################################################

# Zonal mean already taken, so just choose it
modelwind_month = modelwind_month[:,:,0]

# If not already taken, need to take it
# modelwind_month = modelwind_month.collapsed(['longitude'],iris.analysis.MEAN)

# Extract model run data and coords
dataflipped=modelwind_month.data
data = dataflipped.T
x= modelwind_month.coord('latitude').points[:] # According to modelrun resolution
y= modelwind_month.coord('pressure').points[:] # According to modelrun pressure levels

# Load in URAP dataset
path2 = "/data/users/mgriffit/urap_data"
filename2 = "uars_199111-199912.nc"
pathfile2 = os.path.join(path2, filename2)
cubes2=iris.load(pathfile2)

# Extract wind cube
urapwind=cubes2[0]

# Take only latitude coordinate (corresponds to zonal mean)
urapwind=urapwind[:,:,:,0]

# Select month according to urap_date_calc.py and edit the date appropriately
date = 'sep93'
urapwind=urapwind[22,:,:]

# Extract data and coords
urapdata=urapwind.data
x_want = urapwind.coord('latitude').points[:]
y_want = urapwind.coord('pressure').points[:]

# Missing data filled by climatology and has 10000 added to it to illustrate this. Climatological values okay, so we substract 10000 from filled data.
urapdata[urapdata>8000] -= 10000

#Interpolate using GabrielInterpolator
interparray=bilinear_interpolation(x,np.log10(y),data,x_want,np.log10(y_want))

#Calculate difference array and get min and max values for colorbar
difference = urapdata - interparray.T
diffmin , diffmax = [np.nanmin(difference),np.nanmax(difference)]

# Plot the difference
cs = plt.contourf(x_want,y_want,difference, 100, origin = 'lower', hold = 'on',vmin=diffmin,vmax=diffmax)  # Gabriel has vmin=-220 vmax=90.....does this fit better with model output?


# Plot the contour lines with labels
cs2 = plt.contour(cs, levels = cs.levels[::10], colors = 'k', origin = 'lower')    
plt.clabel(cs2, colors = 'k', fmt = '%3.0f')

# Give the plot a title based on the filename (e.g. sep05)
title = 'URAP & Model Difference ' + date[0].upper() + date[1:3] + ' ' + date[3:5]
plt.title(title)

# Sort out the x axis
plt.xlabel('Latitude / degrees')
xticks = range(-90,91,15)
cs.ax.set_xticks(xticks)

# Sort out the y axis
#plt.ylim(ymin=10.**(-2.5)) #have to work out how to remove contour labels
plt.ylabel('Pressure / 100 Pa')
plt.yscale('log')
plt.gca().invert_yaxis()

# Plot colorbar
cb = plt.colorbar(cs, orientation='horizontal')
cb.set_label('Westerly Wind Speed / m/s')

plt.savefig('/home/h04/mgriffit/Documents/Plots/URAP/' + title + '.eps')
