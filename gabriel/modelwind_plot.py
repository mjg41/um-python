#! /usr/local/sci/bin/python2.7
# -*- coding: iso-8859-1 -*-

# Plots Model Run Wind Data for a given month (specified below)

import os
import pdb
import scipy
import numpy as np
import datetime as dt
import calendar as cal
import matplotlib as mpl
import matplotlib.pyplot as plt
import netCDF4 as ncdf
import iris

# Load in model run dataset
path = '/projects/ukca-ex/mgriffit/output_files/19880901T0000Z'
filename = '85km/ao114a.pg19880901'
pathfile = os.path.join(path, filename)

# Extract u wind cube
cubes=iris.load(pathfile,'x_wind')
modelwind=cubes[0] # Choose cube on pressure levels

# Select month and edit the date
#############################################################
date = 'sep88'
modelwind_month=modelwind[0,:,:,:]
#############################################################

# Zonal mean already taken, so just choose it
modelwind_month = modelwind_month[:,:,0]

# If not already taken, need to take it
# modelwind_month = modelwind_month.collapsed(['longitude'],iris.analysis.MEAN)

# Extract model run data and coords
winddata=modelwind_month.data
x= modelwind_month.coord('latitude').points[:] # According to modelrun resolution
y= modelwind_month.coord('pressure').points[:] # According to modelrun pressure levels

# Define data limits for colorbar
windmin, windmax = [np.nanmin(winddata),np.nanmax(winddata)]

# Plot the data
cs = plt.contourf(x,y,winddata, 100, origin = 'lower', hold = 'on',vmin=windmin,vmax=windmax)  # Gabriel had vmin=-58 vmax=170.....does this fit better with model output?

# Plot the contour lines with labels
#cs2 = plt.contour(cs, levels = cs.levels[::10], colors = 'k', origin = 'lower')    
#plt.clabel(cs2, colors = 'k', fmt = '%3.0f')

# Give the plot a title based on the chosen date
title = 'Model Run Winds ' + date[0].upper() + date[1:3] + ' ' + date[3:5]
plt.title(title)

# Sort out the x axis
plt.xlabel('Latitude / degrees')
xticks = range(-90,91,15)
cs.ax.set_xticks(xticks)

# Sort out the y axis
plt.ylabel('Pressure / hPa')
plt.yscale('log')
plt.gca().invert_yaxis()

# Plot colorbar
cb = plt.colorbar(cs, orientation='horizontal')
cb.set_label('Westerly Wind Speed / m s-1')

plt.savefig('/home/d03/mgriffit/Documents/Plots/Model/' + title + '.eps')

