#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

# Plots the urap data for a given month (specified below). A date can be converted to the required month index by using the program urap_date_calc.py

import numpy as np
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt
import iris
import iris.plot as iplt

def urap_index_calc(month, year):
    # Calculates appropraite time index of URAP data based on input year and month.
    # Uses startdate and length of URAP data (98 months) to form dates array.
    
    import numpy as np
    import datetime as dt
    from dateutil.relativedelta import relativedelta
    from iris.time import PartialDateTime
    
    # Convert the times to datetimes
    startdate = dt.datetime(year = 1991, month = 11, day = 1)
    dates = np.zeros(98, dtype = np.object) #98 months of data from Nov 1991 to Dec 1999
    for date in range(98):
        dates[date] = startdate + relativedelta(months = date)
    
    # Create date constraint based on chosen month & year
    monthyear = PartialDateTime(year=year, month=month)
    
    # Find corresponding index
    index = np.where(dates == monthyear)[0][0] # Need [0][0] to pull index out of array result of np.where()
    
    return index

################### MAIN ######################################
# File path
path = '/projects/ukca-ex/mgriffit/urap_data/'
filename = 'uars_199111-199912.nc'
pathfile = path + filename

# Read file
cubes=iris.load(pathfile)
wind=cubes[0]

# Select month using urap_index_calc based on user input
month, year = input('Please select month and year (11 1991 to 12 1999) e.g. 12 1994 :    ').split()
index = urap_index_calc(np.int(month), np.int(year))
wind_cut = wind[index, :, :, 0]

# Extract the data values and coordinates
winddata=wind_cut.data
x = wind_cut.coord('latitude').points[:]
pressure = wind_cut.coord('pressure').points[:]

# Convert pressure to height

H = 7200
height = -H*np.log(pressure/1000)

# Missing data filled by climatology and has 10000 added to it to illustrate this. Climatological values okay, so we substract 10000 from filled data.
winddata[winddata>8000] -= 10000

# Define data limits for colorbar
windmin, windmax = [np.nanmin(winddata),np.nanmax(winddata)]
top = np.max((np.abs(windmax), np.abs(windmin)))

# Define colormap
cmap1 = iplt.plt.cm.get_cmap('RdBu_r', 29)

# Plot the data
plot = plt.pcolormesh(x,height,winddata, vmin=-top, vmax=top, cmap=cmap1)

# Remove whitespace
plot.set_edgecolor('face')

# Edit the colormap and add the colorbar
mymap = plt.cm.ScalarMappable(cmap=plot.get_cmap())
mymap.set_array(winddata)
newtop = plot.get_clim()[1]
mymap.set_clim(-newtop, newtop)
#cb = plt.colorbar(mymap, orientation = 'horizontal', label = 'Mean Zonal Wind / m s-1')

# Plot the contour lines with labels
#plot2 = plt.contour(plot, levels = plot.levels[::10], colors = 'k', origin = 'lower')    
#plt.clabel(plot2, colors = 'k', fmt = '%3.0f')

# Give the plot a title based on the chosen date
date = year + '-' + month + '-16'
title = 'URAP_uwind_ZonalMean_at_' + date
#plt.title(title, fontsize = 12)

# Sort out the x axis
plt.xlabel('Latitude / degrees')
xticks = range(-90,91,30)
plot.axes.set_xticks(xticks)

# Sort out the y axis

plt.ylabel('Approximate Height / m')
plt.ylim(ymax=100000.0)
plt.yticks(range(0,100001,20000))

plt.savefig('/home/d03/mgriffit/Documents/Plots/URAP/' + title + '.png', format='png', dpi=900)
print(title + ' plot successfully saved!')

