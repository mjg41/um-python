#! /usr/local/sci/bin/python2.7
# -*- coding: iso-8859-1 -*-

# Plot the difference between MLS Data and Model Run Data for a given month (specified below)

import os
import pdb
import scipy
import numpy as np
import datetime as dt
import calendar as cal
import matplotlib as mpl
import matplotlib.pyplot as plt
import netCDF4 as ncdf
import iris
from GabrielInterpolator import bilinear_interpolation
from apply_hv import apply_hv

# NetCDF default loading behaviour currently does not expose variables which define reference surfaces for dimensionless vertical coordinates as independent Cubes. This behaviour is deprecated in favour of automatic promotion to Cubes. To switch to the new behaviour:
iris.FUTURE.netcdf_promote = True

# Load in model run dataset
path = '/projects/ukca-ex/mgriffit/output_files/19880901T0000Z'
filename = '85km/ao114a.pg19880901'
pathfile = os.path.join(path, filename)

# Extract air temp cube
cubes=iris.load(pathfile,'air_temperature')
modeltemp=cubes[0]

# Correct data for terain-following grid using heaviside function
hv = iris.load(pathfile)[0]
modeltemp = apply_hv(hv,modeltemp)

# Select month
#############################################################
modeltemp_month=modeltemp[2,:,:,:]
#############################################################

# Zonal mean already taken, so just choose it
modeltemp_month = modeltemp_month[:,:,0]

# If not already taken, need to take it
# modeltemp_month = modeltemp_month.collapsed(['longitude'],iris.analysis.MEAN)

# Extract model run data and coords
dataflipped=modeltemp_month.data
data = dataflipped.T
x= modeltemp_month.coord('latitude').points[:] # According to modeltemp resolution
y= modeltemp_month.coord('pressure').points[:] # According to modeltemp pressure levels

# Load in MLS dataset
path2 = "/data/users/mgriffit/mls_data"
filename2 = "mlssep05.nc"
pathfile2 = os.path.join(path2, filename2)
cubes2=iris.load(pathfile2)

# Extract temperature cube
mlstemp=cubes2[0]

# Extract data and define coords
mlsdata=mlstemp.data
mlsdata = mlsdata[::-1,:] # makes the mlsdata and modeldata have the same orientation (top to sea level)
x_want=np.linspace(-89,89,mlstemp.shape[1]) # According to mlsdata latitudes. Bin centres with data defined as +/-1 degree to either side

y_want=np.array([  261.015716552734,  # According to mlsdata pressure levels
       215.443466186523,
       177.827941894531,
       146.779922485352,
       121.152763366699,
                   100.,
       82.5404205322266,
       68.1292037963868,
        56.234130859375,
       46.4158897399902,
       38.3118667602539,
       31.6227760314941,
       26.1015720367432,
       21.5443477630615,
       14.6779928207398,
                    10.,
       6.81292057037354,
       4.64158868789673,
       3.16227769851685,
       2.15443468093872,
       1.46779930591583,
                     1.,
      0.681292057037354,
      0.464158892631531,
       0.31622776389122,
      0.215443462133408,
      0.146779924631119,
      0.100000001490116,
     0.0464158877730369,
     0.0215443465858698,
    0.00999999977648256,
     0.0046415887773037,
    0.00215443479828537,
    0.00100000004749745])
y_want= y_want[::-1] # makes the y coordinates of mlsdata and modeldata have the same orientation (top to sea level)

#Interpolate using GabrielInterpolator
interparray=bilinear_interpolation(x,np.log10(y),data,x_want,np.log10(y_want))

#Calculate difference array and get min and max values for colorbar
difference = mlsdata - interparray.T
diffmin , diffmax = [np.nanmin(difference),np.nanmax(difference)]

# Plot the difference
cs = plt.contourf(x_want,y_want,difference, 100, origin = 'lower', hold = 'on',vmin=diffmin,vmax=diffmax)  # Gabriel has vmin=-220 vmax=90.....does this fit better with model output?

# Plot the contour lines with labels
cs2 = plt.contour(cs, levels = cs.levels[::10], colors = 'k', origin = 'lower')    
plt.clabel(cs2, colors = 'k', fmt = '%3.0f')

# Give the plot a title based on the filename (e.g. sep05)
title = 'MLS & Model Difference ' + filename2[3].upper() + filename2[4:6] + ' ' + filename2[6:8]
plt.title(title)

# Sort out the x axis
plt.xlabel('Latitude / degrees')
xticks = range(-90,91,15)
cs.ax.set_xticks(xticks)

# Sort out the y axis
#plt.ylim(ymin=10.**(-2.5)) #have to work out how to remove contour labels
plt.ylabel('Pressure / 100 Pa')
plt.yscale('log')
plt.gca().invert_yaxis()

# Plot colorbar
cb = plt.colorbar(cs, orientation='horizontal')
cb.set_label('Temperature / K')

plt.savefig('/home/h04/mgriffit/Documents/Plots/MLS/' + title + '.eps')
