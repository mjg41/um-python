#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

# Plots the mls data for a given month (specified below).

import os
import numpy as np
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt
import iris

# File path
path = '/projects/ukca-ex/mgriffit/mls_data/'

# User chooses month

month_in_str, year_in_str = input('Please select month and year (01 2004 to 01 2012) e.g. 12 2004 :    ').split()
month = np.int(month_in_str)
year = np.int(year_in_str)

# Get corresponding file

months_str = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
filename = 'mls' + months_str[month - 1] + year_in_str[2:] + '.nc'
pathfile = path + filename

# Read file
try:
    cubes=iris.load(pathfile)
except IOError:
    print('IOError: You need to create the monthly average for the dates you have chosen using mls_monthaverage.py before a plot can be produced.')
    quit()
temp=cubes[0].data
# Mask missing data
temp = np.ma.array(temp, mask=(np.isnan(temp)))

# Define data coordinates
x = np.linspace(-89,89,temp.shape[1]) # According to mlsdata latitudes. Bin centres with data defined as +/-1 degree to either side
pressure = np.array([  261.015716552734,  # According to mlsdata pressure levels
       215.443466186523,
       177.827941894531,
       146.779922485352,
       121.152763366699,
                   100.,
       82.5404205322266,
       68.1292037963868,
        56.234130859375,
       46.4158897399902,
       38.3118667602539,
       31.6227760314941,
       26.1015720367432,
       21.5443477630615,
       14.6779928207398,
                    10.,
       6.81292057037354,
       4.64158868789673,
       3.16227769851685,
       2.15443468093872,
       1.46779930591583,
                     1.,
      0.681292057037354,
      0.464158892631531,
       0.31622776389122,
      0.215443462133408,
      0.146779924631119,
      0.100000001490116,
     0.0464158877730369,
     0.0215443465858698,
    0.00999999977648256,
     0.0046415887773037,
    0.00215443479828537,
    0.00100000004749745])

# Convert pressure to height

H = 7200
height = -H*np.log(pressure/1000)

# Define data limits for colorbar
tempmin, tempmax = [np.nanmin(temp),np.nanmax(temp)]

# Plot the data with jet colormap
cmap = plt.cm.get_cmap('jet')
plot = plt.pcolormesh(x, height, temp, vmin=tempmin, vmax=tempmax, cmap=cmap)

# Remove whitespace
plot.set_edgecolor('face')

# Give the plot a title based on the chosen date
date = year_in_str + '-' + month_in_str + '-16'
title = 'MLS_Temp_ZonalMean_at_' + date
#plt.title(title, fontsize = 12)

# Sort out the x axis
plt.xlabel('Latitude / degrees')
xticks = range(-90,91,30)
plot.axes.set_xticks(xticks)
plot.axes.set_xbound(lower=-90.0, upper=90.0)

# Sort out the y axis
plt.ylabel('Approximate Height / m')
plt.ylim(ymax=100000.0)
plt.yticks(range(0,100001,20000))

# Plot colorbar
#cb = plt.colorbar(plot, orientation='horizontal')
#cb.set_label('Air Temperature / K')

plt.savefig('/home/d03/mgriffit/Documents/Plots/MLS/' + title + '.png', format='png', dpi=900)
print(title + ' plot successfully saved!')
