#! /usr/local/sci/bin/python2.7

# Calculates dates of data based on startdate and an iris compatible input file, when the time coordinate has format "months since startdate"

import iris
import numpy as np
import datetime as dt
from dateutil.relativedelta import relativedelta

# File path
path = '/projects/ukca-ex/mgriffit/urap_data/'
filename = 'uars_199111-199912.nc'
pathfile = path + filename

# Read the file
cube = iris.load(pathfile)[0]
wind = cube.data[:]
lats = cube.coord('latitude').points[:]
times = cube.coord('time').points[:]
pressures = cube.coord('pressure').points[:]

# Convert the times to datetimes
startdate = dt.datetime(year = 1991, month = 11, day = 1)
dates = np.zeros(times.shape, dtype = np.object)
dates.fill(np.nan)
for date in times:
    dates[date] = startdate + relativedelta(months = date)

print dates[:]
# Can now search dates vector for date to give corresponding element in time coordinate
