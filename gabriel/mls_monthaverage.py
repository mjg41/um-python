#! /usr/local/sci/bin/python2.7

# For a given year and month (specified below) the average temperature over that month is computed from the mls data and outputted to a netcdf file with given name (specified below)

import pdb
import numpy as np
import datetime as dt
import calendar as cal
import netCDF4 as ncdf
import warnings

#File path
path = '/projects/ukca-ex/mgriffit/mls_data/'
filename = 'mls_zm_p.nc'
pathfile = path + filename

#Read the file
with ncdf.Dataset(pathfile, "r") as fo:
    temps = fo.variables["Temperature"][:]
    lats = fo.variables["Lat"][:]
    times = fo.variables["Time"][:]
    pressures = fo.variables["Prs"][:]

# Convert the matlab times to datetimes
epoch = dt.datetime(year = 1970, month = 1, day = 1)
matlab_epoch = 719529
dates = np.zeros(times.shape, dtype = np.object)
dates.fill(np.nan)
for i, time in enumerate(times):
    dayoffset = time - matlab_epoch
    dates[i] = epoch + dt.timedelta(days = float(dayoffset))

# We want to average a month's worth of data. 

# Which month and year?
month_in_str, year_in_str = raw_input('Please select month and year (01 2004 to 01 2012) e.g. 12 2004 :    ').split()
month = np.int(month_in_str)
year = np.int(year_in_str)
start_date = dt.datetime(year, month, 1)
dummy, endday = cal.monthrange(year, month)
end_date = start_date + dt.timedelta(days = endday)

#What indices correspond to our start & end dates? Warn if match isn't perfect.
istarts, dummy = np.where(dates >= start_date)
istart = istarts[0]
iends, dummy = np.where(dates >= end_date)
iend = iends[0]
if dates[istart] != start_date:
    msg = "Start date {} does not correspond to istart date {}".format(start_date, dates[istart])
    raise Warning, msg
if dates[iend] != end_date:
    msg = "End date {} does not correspond to iend date {}".format(end_date, dates[iend])
    raise Warning, msg

print "Maximum and mimimum values of temps:"
print np.nanmax(temps)
print np.nanmin(temps)


#Select the portion of temps within our start & end dates
temp_cut = temps[:, :, istart:iend]

print "Maximum and mimimum values of temp_cut:"
print np.nanmax(temp_cut)
print np.nanmin(temp_cut)

print temp_cut.shape
#Remove NaNs
#temp_cut2 = np.ma.fix_invalid(temp_cut)
#Average over time axis, catching warning when averaging all NaNs
with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=RuntimeWarning)
    av_temp = np.nanmean(temp_cut,axis=2)

print "Maximum and mimimum values of av_temp:"
print np.nanmax(av_temp)
print np.nanmin(av_temp)
print av_temp.shape

# Name output file based on chosen year and month
months_str = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
out_name = 'mls' + months_str[month - 1] + year_in_str[2:] + '.nc'
out = ncdf.Dataset(path + out_name, 'w')

# Fill output file with MLS data

out.createDimension('Latitude', 90)
out.createDimension('Pressure', 34)
t_var = out.createVariable('T', 'f8', ('Pressure', 'Latitude'))
t_var.long_name = "Zonal temperature on pressure grid"
t_var.var_name = "T"
t_var.standard_name = "air_temperature"
t_var.units = "K"   
t_var[:] = av_temp
out.close()


