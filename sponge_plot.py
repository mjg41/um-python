import numpy as np
import matplotlib.pyplot as plt

# Eta height values

eta_theta_levels=np.array([
    0.000000e+00,   1.976949e-04,   5.271534e-04,   9.884744e-04,   1.581559e-03,   
    2.306407e-03,   3.163118e-03,   4.151592e-03,   5.271830e-03,   6.523931e-03,   
    7.907795e-03,   9.423423e-03,   1.107091e-02,   1.285017e-02,   1.476118e-02,   
    1.680406e-02,   1.897871e-02,   2.128512e-02,   2.372339e-02,   2.629342e-02,   
    2.899522e-02,   3.182888e-02,   3.479430e-02,   3.789149e-02,   4.112053e-02,   
    4.448135e-02,   4.797392e-02,   5.159836e-02,   5.535457e-02,   5.924253e-02,   
    6.326236e-02,   6.741395e-02,   7.169731e-02,   7.611253e-02,   8.065951e-02,   
    8.533835e-02,   9.014896e-02,   9.509143e-02,   1.001658e-01,   1.053722e-01,   
    1.107107e-01,   1.161817e-01,   1.217855e-01,   1.275224e-01,   1.333934e-01,   
    1.393994e-01,   1.455418e-01,   1.518222e-01,   1.582431e-01,   1.648073e-01,   
    1.715191e-01,   1.783830e-01,   1.854052e-01,   1.925933e-01,   1.999563e-01,   
    2.075058e-01,   2.152552e-01,   2.232211e-01,   2.314232e-01,   2.398847e-01,   
    2.486332e-01,   2.577011e-01,   2.671264e-01,   2.769533e-01,   2.872332e-01,   
    2.980252e-01,   3.093978e-01,   3.214292e-01,   3.342090e-01,   3.478394e-01,   
    3.624364e-01,   3.772611e-01,   3.920883e-01,   4.069154e-01,   4.217425e-01,   
    4.365696e-01,   4.513967e-01,   4.662238e-01,   4.810509e-01,   4.958781e-01,   
    5.107052e-01,   5.255323e-01,   5.403594e-01,   5.551865e-01,   5.700136e-01,   
    5.848408e-01,   5.996679e-01,   6.144950e-01,   6.293221e-01,   6.441492e-01,   
    6.589763e-01,   6.738035e-01,   6.886306e-01,   7.034577e-01,   7.182848e-01,   
    7.331119e-01,   7.479390e-01,   7.627661e-01,   7.775933e-01,   7.924204e-01,   
    8.072475e-01,   8.220746e-01,   8.369017e-01,   8.517288e-01,   8.665560e-01,   
    8.813831e-01,   8.962102e-01,   9.110373e-01,   9.258644e-01,   9.406915e-01,   
    9.555187e-01,   9.703458e-01,   9.851729e-01,   1.000000e+00])
n_eta_theta = eta_theta_levels.size

# Latitudes

xi2 = np.linspace(-np.pi/2., np.pi/2.,144)
n_xi2 = xi2.size

# Set eg_vert_damp_coeff (damping thickness) and start of damping eta_s
# and compute 1/(1-eta_s)

eta_s = 0.5
eg_vert_damp_coeff = 0.05
recip_one_m_etas = 1.0/(1.0-eta_s)

# Initiliase vertical damping

mu_w = np.zeros((n_xi2,n_eta_theta))

# Compute damping

for k in range(n_eta_theta):
    for j in range(n_xi2):
        
        cos_xi2 = np.cos(xi2[j])
        eta_ijk = cos_xi2*eta_theta_levels[k] + (1.0-cos_xi2)
        
        if eta_ijk > eta_s :
            mu_w[j,k] = eg_vert_damp_coeff * (np.sin( 0.5*np.pi*(eta_ijk-eta_s)
                        *recip_one_m_etas)**2 + np.sin(xi2[j])**40.0)
        else:
            mu_w[j,k] = 0.0

# Plot damping, converting to latitude vs. height

lats = xi2 * 180. / np.pi
top_height = 100000.0
heights = eta_theta_levels * top_height
X,Y = np.meshgrid(lats, heights, indexing = 'ij')
cmap = plt.cm.get_cmap('jet')
plt.pcolormesh(X, Y, mu_w, cmap=cmap)
plt.title('Sponge layer for 100 km model lid')
plt.xlabel('Latitude / degrees')
plt.ylabel('Height / m')
cbar = plt.colorbar(orientation='horizontal')
cbar.set_label(r'Vertical damping coefficient $\bar{\mu}$')
plt.axis('tight')
plt.xticks(range(-90,91,15))
plt.show()

