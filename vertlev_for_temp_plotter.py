import iris
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, AutoMinorLocator
from vertlev_creator import temp_to_vertlevs as ttv

# Load in temps from file

SMIN06 = iris.load('data/WACCM/T_WACCM_SMIN_200506.nc')[0]
SMIN12 = iris.load('data/WACCM/T_WACCM_SMIN_200512.nc')[0]
SMAX06 = iris.load('data/WACCM/T_WACCM_SMAX_200506.nc')[0]
SMAX12 = iris.load('data/WACCM/T_WACCM_SMAX_200512.nc')[0]

T_NUDGE = iris.load('data/t_nudge.nc')[0]

h_NUDGE = T_NUDGE.coord('height').points[:]
# Extract data and coords

temp_data = [SMIN06.data, SMIN12.data, SMAX06.data, SMAX12.data]

#~ temp_data = [SMIN06.data]

heights = [SMIN06.coord('height').points[:],
           SMIN12.coord('height').points[:],
           SMAX06.coord('height').points[:],
           SMAX12.coord('height').points[:]]

#~ heights = [SMIN06.coord('height').points[:]]

labels = ['Solar minimum Jun',
          'Solar minimum Dec',
          'Solar maximum Jun',
          'Solar maximum Dec']

#~ labels = ['Solar minimum Jun']

# Compute min, mean, max

temps_min = []
temps_mean = []
temps_max = []

for temp in temp_data:
    temps_min.append(np.amin(temp, axis=1))
    temps_mean.append(np.mean(temp,axis=1))
    temps_max.append(np.amax(temp,axis=1))

# Plot all mins, means and max together in 3 separate plots

titles_1 = ['Zonal minimum temperature from WACCM-X',
          'Zonal mean temperature from WACCM-X',
          'Zonal maximum temperature from WACCM-X']

temps = [temps_min, temps_mean, temps_max]

for title,temp in zip(titles_1,temps):
    
    # Initialise plot
    
    fig, ax = plt.subplots(figsize = [7,4])
    
    # Plot title
    
    fig.suptitle(title)
    
    for data, height, label in zip(temp,heights,labels):
        
        # Plot each of four temps with label
        
        ax.plot(data,height,label=label)
        ax.set_xlabel('Temperature / K')
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        ax.set_ylabel('Height / km')
    
    ax.plot(T_NUDGE.data[82:],h_NUDGE[82:], label='Nudge Temp') # non-zero terms
    ax.legend()
    fig.savefig('/Users/mjg41/Documents/PhD/Plots/WACCM/' + title + '.png', dpi=200)

plt.close('all')

# Plot scale heights

titles_2 = ['Scale heights using zonal minimum temperature from WACCM-X',
          'Scale heights using zonal mean temperature from WACCM-X',
          'Scale heights using zonal maximum temperature from WACCM-X']

for title,temp in zip(titles_2,temps):
    
    # Initialise plot
    
    fig, ax = plt.subplots(figsize = [7,4])
    
    # Plot title
    
    fig.suptitle(title, fontsize=16)
    
    for data, height, label in zip(temp,heights,labels):
        
        # Calculate scale height
        
        g = 9.80665/((1 + height/6371000)**2)
        
        H = 287.05 * data / g
        
        # Plot each of four scale heights with label
        
        ax.plot(H/1000.,height,label=label)
        ax.set_xlabel('Scale height / km', fontsize=16)
        ax.set_ylabel('Height / km', fontsize=16)
        ax.set_ylim(0,145000)
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        ax.set_xlim(0,18)
        ax.set_xticks(np.arange(0,19,2))
        ax.xaxis.set_minor_locator(AutoMinorLocator(2))
        ax.tick_params(axis='x', labelsize=14)
        ax.tick_params(axis='y', labelsize=14)
        ax.legend(fontsize='large')
    
    fig.savefig('/Users/mjg41/Documents/PhD/Plots/WACCM/' + title + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.1)

plt.close('all')

# Create vertlevs and plot

levs = []

titles_3 = ['Levels from zonal minimum temperature',
            'Levels from zonal mean temperature',
            'Levels from zonal maximum temperature']

for temp,title in zip(temps,titles_3):
    
    # Initialise plot
    
    #fig, ax = plt.subplots(figsize = [7,4])
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True, gridspec_kw = {'width_ratios':[1, 9]}, figsize = [7,4])

    
    # Plot title
    
    fig.suptitle(title + ' - H/2 minimum', fontsize=16)
    
    for data, height, label in zip(temp,heights,labels):
        
        # Compute vert_levs and plot
        
        levs,_ = ttv(data,height)

        ax1.plot(np.zeros(np.size(levs[levs<131000])),levs[levs<131000],'o',fillstyle='none')
        ax2.plot(levs[levs<131000],'-o',fillstyle='none', label=label)
        
        ax1.set_xticklabels([])
        ax1.set_xticks([])
        ax2.set_xticks(range(0,105,10))
        plt.subplots_adjust(wspace=0.05)
        ax2.set_xlabel('Model level number', fontsize=16)
        ax1.set_ylabel('Height / km', fontsize=16)
        ax1.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        ax2.tick_params(axis='x', labelsize=14)
        ax1.tick_params(axis='y', labelsize=14)
        ax2.legend()
    
    fig.savefig('/Users/mjg41/Documents/PhD/Plots/Model/' + title + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.1)

# Plot vertical level depth

titles_4 = ['Level depth from zonal minimum temperature',
            'Level depth from zonal mean temperature',
            'Level depth from zonal maximum temperature']

for temp,title in zip(temps,titles_4):
    
    # Initialise plot
    
    fig, ax = plt.subplots(figsize = [7,4])
    
    # Plot title
    
    fig.suptitle(title + ' - H/2 minimum')
    
    for data, height, label in zip(temp,heights,labels):
        
        # Compute vert_levs and plot
        
        levs,_ = ttv(data,height)
        lev_diff = np.diff(levs)
        levs = levs[1:]
        
        ax.plot(lev_diff[levs<131000], levs[levs<131000], label=label)
        ax.set_xlabel('Level depth / km', fontsize=16)
        ax.xaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        #ax.set_xticks(np.arange(0,np.amax(lev_diff+2000),2000))
        #ax.tick_params(axis='x', which='major', labelsize=6)
        ax.set_ylabel('Height / km', fontsize=16)
        ax.tick_params(axis='x', labelsize=14)
        ax.tick_params(axis='y', labelsize=14)
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: ('%g') % (x / 1000.0)))
        ax.legend()
    
    fig.savefig('/Users/mjg41/Documents/PhD/Plots/Model/' + title + '.png', dpi=200, bbox_inches = 'tight', pad_inches = 0.1)
plt.show()
