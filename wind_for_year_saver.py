import iris
import numpy as np
import os

# MAIN
if __name__ == '__main__':
    print('Main!')

    # Create paths to input and output

    inputpath = '/projects/ukca-ex/mgriffit/output_files/radar_comparison/'
    run = input('Please enter path to file relative to "radar_comparison/":    ')
    outputpath = '/home/d03/mgriffit/Documents/Plots/Model/radar_comparison/'
    
    # List all files in folder
    
    files = os.listdir(inputpath + run)
    
    # All months and different winds
    
    months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
    #wind_list = ['u_asc', 'u_rot', 'v_asc', 'v_rot']
    wind_list = ['eastgw_asc', 'eastgw_rot', 'northgw_asc', 'northgw_rot']
    
    # Initialise list of cubes for 4 winds, each with 12 months
                
    wind_data_list = [[None for ii in range(12)] for jj in range(4)]
    
    for fname in files:
        
        # Check if it's a netcdf file otherwise continue to next item in loop
        
        if ('.nc' not in fname):
            continue
        
        # Now proceed to importing each wind

        for ii, wind in enumerate(wind_list):
            
            if (wind in fname):            
                
                for jj, month in enumerate(months):
                    
                    # For the jj'th month import the ii'th wind into the jj'th position in the list
                    
                    if (month in fname):
                        
                        # Take the zeroth element to get actual cube
                        
                        wind_data_list[ii][jj] = iris.load(inputpath + run + fname)[0]
                        
                        # Create and add month number as aux_coord
                        # Use 'jj+1' so months run from 1 to 12
                        
                        month_coord = iris.coords.AuxCoord(jj+1, var_name = 'month')
                        wind_data_list[ii][jj].add_aux_coord(month_coord)

                        break

    # Given all wind data, save complete wind file
    
    for ii, wind in enumerate(wind_data_list):
        for jj, wind_month in enumerate(wind):

            # Remove superfluous time and forecast_period coords in preparation for merge

            wind_data_list[ii][jj].remove_coord('time')
            wind_data_list[ii][jj].remove_coord('forecast_period')

        # Make the output directory if it doesn't exist
    
        if not os.path.exists(os.path.dirname('data/MODEL/' + run)):
            try:
                os.makedirs(os.path.dirname('data/MODEL/' + run))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

        # Merge and save each wind_data cube

        wind_year = iris.cube.CubeList(wind).merge_cube()
        iris.save(wind_year, inputpath + run + wind_list[ii] + '_wind_year.nc')
        iris.save(wind_year, 'data/MODEL/' + run + wind_list[ii] + '_wind_year.nc')
